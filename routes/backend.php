<?php  
/* Admin */

Route::auth();
Route::get('login-admin',['as'  => 'getlogin', 'uses' =>'backend\PagesController@getLogin']);
Route::post('login-admin',['as'  => 'postlogin', 'uses' =>'backend\PagesController@postLogin']);
Route::get('loout-admin',['as'  => 'getlogout', 'uses' =>'backend\PagesController@getLogoutAdmin']);

Auth::routes();

Route::group(['prefix'=>'admin','namespace'=>'backend','middleware'=>'adminLogin'],function(){
  Route::group(['prefix'=>'pages'],function (){
    Route::get('/',['as'=>'admin.pages.index','uses'=>'PagesController@AdminIndex']);
    Route::get('lich',['as'=>'admin.pages.lich','uses'=>'PagesController@Lich']);
    Route::get('chitietuser',['as'=>'admin.pages.chitietuser','uses'=>'PagesController@ChiTietUser']);
    Route::get('doi-mk',['as'=>'admin.pages.getdoimk','uses'=>'PagesController@getDoiMk']);
    Route::post('doi-mk',['as'=>'admin.pages.postdoimk','uses'=>'PagesController@postDoiMk']);
    Route::get('inbox-mail',['as'=>'admin.pages.inboxmail','uses'=>'PagesController@InboxMail']);
  });
  Route::group(['prefix'=>'danhmuc'],function(){
    Route::get('/',['as'=>'admin.danhmuc.list','uses'=>'DanhMucController@List']);
    Route::get('add',['as'=>'admin.danhmuc.getadd','uses'=>'DanhMucController@getAdd']);
    Route::post('add',['as'=>'admin.danhmuc.postadd','uses'=>'DanhMucController@postAdd']);
    Route::get('edit/{id}',['as'=>'admin.danhmuc.getedit','uses'=>'DanhMucController@getEdit']);
    Route::post('edit/{id}',['as'=>'admin.danhmuc.postedit','uses'=>'DanhMucController@postEdit']);
    Route::get('del/{id}',['as'=>'admin.danhmuc.del','uses'=>'DanhMucController@Del']);
  });
  Route::group(['prefix'=>'sanpham'],function(){
    Route::get('/',['as'=>'admin.sanpham.list','uses'=>'SanPhamController@List']);
    Route::get('add',['as'=>'admin.sanpham.getadd','uses'=>'SanPhamController@getAdd']);
    Route::post('add',['as'=>'admin.sanpham.postadd','uses'=>'SanPhamController@postAdd']);
    Route::get('edit/{id}',['as'=>'admin.sanpham.getedit','uses'=>'SanPhamController@getEdit']);
    Route::post('edit/{id}',['as'=>'admin.sanpham.postedit','uses'=>'SanPhamController@postEdit']);
    Route::get('del/{id}',['as'=>'admin.sanpham.del','uses'=>'SanPhamController@Del']);
    Route::get('detail/{id}',['as'=>'admin.sanpham.detail','uses'=>'SanPhamController@Detail']);
    Route::get('addDetail/{id}',['as'=>'admin.sanpham.getDetail','uses'=>'SanPhamController@GetAddDetail']);
    Route::post('addDetail/{id}',['as'=>'admin.sanpham.postDetail','uses'=>'SanPhamController@PostAddDetail']);
    Route::get('delimg/{id}',['as'=>'admin.sanpham.getdelimg','uses'=>'SanPhamController@DelImg']);
  });
  Route::group(['prefix'=>'cauhinhweb'],function(){
    Route::get('cau-hinh',['as'=>'admin.cauhinhweb.getcauhinh','uses'=>'CauHinhController@getCauHinh']);
    Route::post('cau-hinh',['as'=>'admin.cauhinhweb.postcauhinh','uses'=>'CauHinhController@postCauHinh']);
    Route::get('banner',['as'=>'admin.cauhinhweb.getbanner','uses'=>'CauHinhController@getBanner']);
    Route::get('bannerAjax/{id}',['as'=>'admin.cauhinhweb.getbannerAjax','uses'=>'CauHinhController@getBannerAjax']);
    Route::post('banner',['as'=>'admin.cauhinhweb.postbanner','uses'=>'CauHinhController@postBanner']);
    Route::get('delBanner/{id}',['as'=>'admin.cauhinhweb.delBanner','uses'=>'CauHinhController@delBanner']);
    Route::get('logo',['as'=>'admin.cauhinhweb.getlogo','uses'=>'CauHinhController@getLogo']);
    Route::post('logo',['as'=>'admin.cauhinhweb.postlogo','uses'=>'CauHinhController@postLogo']);
    Route::get('delLogo/{id}',['as'=>'admin.cauhinhweb.delLogo','uses'=>'CauHinhController@delLogo']);
    Route::get('lienhe',['as'=>'admin.cauhinhweb.getlienhe','uses'=>'CauHinhController@getLienHe']);
    Route::post('lienhe',['as'=>'admin.cauhinhweb.postlienhe','uses'=>'CauHinhController@postLienHe']);
    Route::get('video-fb',['as'=>'admin.cauhinhweb.getvideoFb','uses'=>'CauHinhController@getVideoFb']);
    Route::post('video-fb',['as'=>'admin.cauhinhweb.postvideoFb','uses'=>'CauHinhController@postVideoFb']);
  });
  Route::group(['prefix'=>'tintuc'],function(){
    Route::get('/',['as'=>'admin.tintuc.list','uses'=>'TinTucController@List']);
    Route::get('add',['as'=>'admin.tintuc.getadd','uses'=>'TinTucController@getAdd']);
    Route::post('add',['as'=>'admin.tintuc.postadd','uses'=>'TinTucController@postAdd']);
    Route::get('edit/{id}',['as'=>'admin.tintuc.getedit','uses'=>'TinTucController@getEdit']);
    Route::post('edit/{id}',['as'=>'admin.tintuc.postedit','uses'=>'TinTucController@postEdit']);
    Route::get('del/{id}',['as'=>'admin.tintuc.del','uses'=>'TinTucController@Del']);
    Route::get('detail/{id}',['as'=>'admin.tintuc.detail','uses'=>'TinTucController@Detail']);
  });
  Route::group(['prefix'=>'dichvu'],function(){
    Route::get('/',['as'=>'admin.dichvu.list','uses'=>'DichVuController@List']);
    Route::get('add',['as'=>'admin.dichvu.getadd','uses'=>'DichVuController@getAdd']);
    Route::post('add',['as'=>'admin.dichvu.postadd','uses'=>'DichVuController@postAdd']);
    Route::get('edit/{id}',['as'=>'admin.dichvu.getedit','uses'=>'DichVuController@getEdit']);
    Route::post('edit/{id}',['as'=>'admin.dichvu.postedit','uses'=>'DichVuController@postEdit']);
    Route::get('del/{id}',['as'=>'admin.dichvu.del','uses'=>'DichVuController@Del']);
    Route::get('detail/{id}',['as'=>'admin.dichvu.detail','uses'=>'DichVuController@Detail']);
  });
  Route::group(['prefix'=>'user'],function(){
    Route::get('/',['as'=>'admin.user.list','uses'=>'UserController@List']);
    Route::get('add',['as'=>'admin.user.getadd','uses'=>'UserController@getAdd']);
    Route::post('add',['as'=>'admin.user.postadd','uses'=>'UserController@postAdd']);
    Route::get('edit/{id}',['as'=>'admin.user.getedit','uses'=>'UserController@getEdit']);
    Route::post('edit/{id}',['as'=>'admin.user.postedit','uses'=>'UserController@postEdit']);
    Route::get('del/{id}',['as'=>'admin.user.del','uses'=>'UserController@Del']);
    Route::get('detail/{id}',['as'=>'admin.user.detail','uses'=>'UserController@Detail']);
  });
  Route::group(['prefix'=>'donhang'],function(){
    Route::get('/',['as'=>'admin.donhang.list','uses'=>'DonHangController@List']);
    Route::get('detail/{id}',['as'=>'admin.donhang.detail','uses'=>'DonHangController@Detail']);
    Route::get('del/{id}',['as'=>'admin.donhang.del','uses'=>'DonHangController@Del']);
    Route::get('xacnhan/{id}',['as'=>'admin.donhang.xacnhan','uses'=>'DonHangController@XacNhan']);
    Route::get('huybo/{id}',['as'=>'admin.donhang.huybo','uses'=>'DonHangController@HuyBo']);
    Route::get('delsp/{dh}/{sp}',['as'=>'admin.donhang.delsp','uses'=>'DonHangController@DelSp']);
  });
  Route::get(['prefix'=>'quanlycode'],function(){
    Route::get('quanlymenu',['as'=>'admin.quanlycode.quanlymenu','uses'=>'QuanLyCodeController@QuanLyMenu']);
  });
  Route::group(['prefix'=>'phanmorong'],function(){
    Route::get('gioithieu',['as'=>'admin.phanmorong.getgioithieu','uses'=>'PhanMoRongController@getGioiThieu']);
    Route::post('gioithieu',['as'=>'admin.phanmorong.postgioithieu','uses'=>'PhanMoRongController@postGioiThieu']);

    Route::get('chinhsach',['as'=>'admin.phanmorong.chinhsach','uses'=>'PhanMoRongController@ChinhSach']);
    Route::get('add-chinhsach',['as'=>'admin.phanmorong.getaddchinhsach','uses'=>'PhanMoRongController@getAddChinhSach']);
    Route::post('add-chinhsach',['as'=>'admin.phanmorong.postaddchinhsach','uses'=>'PhanMoRongController@postAddChinhSach']);
    Route::get('edit-chinhsach/{id}',['as'=>'admin.phanmorong.geteditchinhsach','uses'=>'PhanMoRongController@getEditChinhSach']);
    Route::post('edit-chinhsach/{id}',['as'=>'admin.phanmorong.posteditchinhsach','uses'=>'PhanMoRongController@postEditChinhSach']);
    Route::get('delchinhsach/{id}',['as'=>'admin.phanmorong.delchinhsach','uses'=>'PhanMoRongController@DelChinhSach']);
  });

});