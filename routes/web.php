<?php
@include('backend.php');

Route::get('/',['as'=>'index','uses'=>'frontend\TrangChuController@TrangChu']);

Route::get('dang-ky.html','frontend\UserController@GetDangKy');
Route::post('dang-ky.html','frontend\UserController@PostDangKy');
Route::get('dang-nhap.html','frontend\UserController@GetDangNhap');
Route::post('dang-nhap.html', 'frontend\UserController@PostDangNhap');
Route::post('post-dang-nhap.html','frontend\UserController@Login');
Route::get('dang-xuat.html','frontend\UserController@DangXuat');

Route::get('tim-kiem.html','frontend\PagesController@TimKiem');
Route::get('yeu-thich.html','frontend\PagesController@YeuThich');
Route::get('yeu-thich/{sp}/{user}','frontend\PagesController@AddYeuThich');
Route::get('xoasp-yeu-thich/{sp}/{user}','frontend\PagesController@XoaSpYeuThich');

Route::get('lien-he.html','frontend\PagesController@LienHe');
Route::get('gioi-thieu.html','frontend\PagesController@GioiThieu');
Route::get('san-pham.html','frontend\PagesController@SanPham');
Route::get('tin-tuc.html','frontend\TinTucController@TinTuc');
Route::get('tin-tuc/{slug}','frontend\TinTucController@ChiTietTin');
Route::get('where-quan-huyen/{id}','frontend\PagesController@WhereQuanHuyen');
Route::get('where-xa-phuong/{id}','frontend\PagesController@WhereXaPhuong');
Route::get('xem-nhanh/{id}','frontend\PagesController@XemNhanh');
Route::get('ajax-size/{id}/{size}','frontend\SanPhamController@AjaxSize');
Route::get('ajax-mau/{id}/{mau}/{size}','frontend\SanPhamController@AjaxMau');
Route::get('ajax-gia/{gia}','frontend\SanPhamController@AjaxGia');

Route::get('gio-hang.html','frontend\GioHangController@GioHang');
Route::get('them-gio-hang/{id}','frontend\GioHangController@AddCart');
Route::get('xoa-gio-hang/{rowId}','frontend\GioHangController@XoaGioHang');
Route::get('xoa-all-gio-hang','frontend\GioHangController@XoaAllGioHang');
Route::get('cap-nhat-sl/{rowId}/{qty}','frontend\GioHangController@CapNhatSl');
Route::get('thanh-toan.html','frontend\GioHangController@GetThanhToan')->name('thanh-toan');
Route::post('thanh-toan.html','frontend\GioHangController@PostThanhToan')->name('thanh-toan');
Route::get('thanh-toan-thanh-cong.html','frontend\GioHangController@ThanhToanThanhCong')->name('thanh-cong');

Route::get('{slug}','frontend\SanPhamController@TheLoaiSanPham')->where('id', '[0-9]+');;
Route::get('san-pham/{slug}','frontend\SanPhamController@ChiTietSanPham');




