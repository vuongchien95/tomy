<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\GioiThieu;
use App\Models\LienHe;
use App\Models\SanPham;
use App\User;
use App\Http\Requests\FronURequest;
use Hash,Auth,Session;
use App\Models\TinhThanh;
use App\Models\QuanHuyen;
use App\Models\XaPhuong;
use App\Models\YeuThich;

class PagesController extends Controller
{
  public function LienHe(){
    $lienhe = LienHe::first();
    return view('frontend.pages.lienhe',compact('lienhe'));
  }
  public function GioiThieu(){
    $gioithieu = GioiThieu::first();
    return view('frontend.pages.gioithieu',compact('gioithieu'));
  }
  public function SanPham(){
    $sanpham = SanPham::where('AnHien',1)->inRandomOrder()->paginate(12);
    return view('frontend.pages.sanpham',compact('sanpham'));
  }
  
  public function WhereQuanHuyen($id){
    $quanHuyen = QuanHuyen::where('idTinhThanh',$id)->get();
    foreach ($quanHuyen as $items) 
    {
      echo "<option value='".$items->id."'>".$items->Name."</option>";
    }
  }

  public function WhereXaPhuong($id){
    $xaPhuong = XaPhuong::where('idQuanHuyen',$id)->get();
    foreach ($xaPhuong as $items) 
    {
      echo "<option value='".$items->id."'>".$items->Name."</option>";
    }
  }

  public function XemNhanh($id){
    $xemNhanh = SanPham::find($id);

    $json = json_encode($xemNhanh);
    echo $json;
  }

  public function TimKiem(Request $req){
    $search = $req->search;
    $sanPham = SanPham::where('Name','like','%'.$req->search.'%')->orWhere('Gia',$req->search)->orWhere('GiaSale',$req->search)->orWhere('MaSp',$req->search)->paginate(8);
    return view('frontend.pages.timkiem',compact('sanPham','search'));
  }

  public function YeuThich(){
    if (Auth::check()) {
      $user = Auth::user()->id;
      $yeuThich = YeuThich::where('idUser',$user)->get();
      foreach ($yeuThich as $key => $items) {
        $sanPham[] = SanPham::where(['id'=>$yeuThich[$key]->idSanPham,'AnHien'=>1])->get();
      }
    }
    return view('frontend.pages.yeuthich',compact('sanPham'));
  }

  public function AddYeuThich($sp,$user){
    $check = YeuThich::where(['idUser'=>$user,'idSanPham'=>$sp])->first();
    if ($check) {
      echo "false";
    }else{
      $addYeuThich            = new YeuThich;
      $addYeuThich->idUser    = $user;
      $addYeuThich->idSanPham = $sp;
      $addYeuThich->save();
    } 
  }
  public function XoaSpYeuThich($sp,$user){
    $delYt = YeuThich::where(['idSanPham'=>$sp,'idUser'=>$user])->first();
    $delYt->delete();
  }

}
