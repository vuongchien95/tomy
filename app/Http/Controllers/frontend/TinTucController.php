<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\TinTuc;

class TinTucController extends Controller
{
  public function TinTuc(){
    $data['tintuc'] = TinTuc::where('AnHien',1)->paginate(6);
    return view('frontend.tintuc.tintuc',$data);
  }
  public function ChiTietTin($slug){
    $data['chitiettin'] = TinTuc::where(['Slug'=>$slug,'AnHien'=>1])->first();
    $data['tintuckhac'] = TinTuc::where('AnHien',1)->inRandomOrder()->limit(5)->get();
    return view('frontend.tintuc.chitiettin',$data);
  }
}
