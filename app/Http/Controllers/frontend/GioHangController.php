<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Cart;
use Session,Mail,Auth;
use App\Models\SanPham;
use App\Models\TinhThanh;
use App\Models\QuanHuyen;
use App\Models\XaPhuong;
use App\Models\DonHang;
use App\Models\CTDonHang;
use App\User;

class GioHangController extends Controller
{
  public function GioHang(){
    $data['cart']    = Cart::content();
    $data['total']   = Cart::subtotal(0);
    return view('frontend.giohang.giohang',$data);
  }

  public function AddCart(Request $req,$id){
    $sanPham = SanPham::find($id);
    if ($sanPham->GiaSale > 0) {
      $price = $sanPham->GiaSale;
    }else{
      $price = $sanPham->Gia;
    }

    if($req->quantity){
      $qty = $req->quantity;
    }
    else{
      $qty =1;
    }
    Cart::add(array(
      'id'=>$id,
      'name'=>$sanPham->Name,
      'qty'=>$qty,
      'price'=>$price,
      'options'=>array(
        'image'    =>$sanPham->Image,
        'slug'     =>$sanPham->Slug,
        'masp'     =>$sanPham->MaSp,
        'kieu'     =>$sanPham->Kieu,
        'kichthuoc'=>$req->kichthuoc,
        'mausac'   =>$req->mausac
      ))
  );
    $cart  = Cart::content();
    $total = Cart::subtotal(0);

    return redirect('/gio-hang.html');
  }

  public function XoaGioHang($rowId){
    Cart::remove($rowId);
  }
  public function XoaAllGioHang(){
    Cart::destroy();
  }

  public function CapNhatSl($rowId,$qty){
    Cart::update($rowId,$qty);
    $cart = Cart::content();
    $data['total'] = Cart::subtotal(0).'₫';
    foreach ($cart as $items) {
      if($items->rowId == $rowId){
        $data['sub_total'] = number_format($items->price * $items->qty).'₫';
      }
    }
    $json = json_encode($data);
    echo $json;
  }

  public function ThanhToanThanhCong(){
    $data['cart']       = Cart::content();
    $data['total']      = Cart::subtotal(0);
    return view('frontend.giohang.thanhcong',$data);
  }

  public function GetThanhToan(){
    $data['cart']       = Cart::content();
    $data['total']      = Cart::subtotal(0);
    $data['tinhThanh']  = TinhThanh::all();
    return view('frontend.giohang.thanhtoan',$data);
  }
  public function PostThanhToan(Request $req){
    $tinhThanh = TinhThanh::where('id',$req->tinhthanh)->first();
    $quanHuyen = QuanHuyen::where('id',$req->quanhuyen)->first();
    $xaPhuong  = XaPhuong::where('id',$req->xaphuong)->first();

    
    if(Auth::check()) {
      $email     = Auth::user()->email;
      $address   = Auth::user()->address;
      $phone     = Auth::user()->phone;
      $name      = Auth::user()->name;
      $cart    = Cart::content();
      $total   = Cart::subtotal(0);

      //lưu bảng đơn hàng
      $donHang           = new DonHang;
      $donHang->idUser   = Auth::user()->id;
      $donHang->TongTien = $total;
      $donHang->ThanhToan= 'Tiền mặt';
      $donHang->NgayMua  = date('Y-m-d');
      $donHang->TrangThai= 2; //chờ xác nhận
      $donHang->TinNhan  = $req->note;
      $donHang->save();

      //lưu bảng chi tiết đơn hàng
      foreach ($cart as $items) {
        $ctDonHang            = new CTDonHang;
        $ctDonHang->idDonHang = $donHang->id;
        $ctDonHang->idSanPham = $items->id;
        $ctDonHang->SoLuong   = $items->qty;
        $ctDonHang->Gia       = $items->price;
        $ctDonHang->SizeMau   = $items->options['kichthuoc'].' - '.$items->options['mausac'];
        $ctDonHang->save();
      }

      $data = [
        'name'   => $name,
        'email'  => $email,
        'phone'  => $phone,
        'address'=> $address
      ];

      Mail::send('frontend.pages.mail',$data, function ($message) use ($data) {
        $message->from('mail.anhson@gmail.com', 'Tony4men');

        $message->to($data['email']);
        $message->subject('Tony4men - Thời trang - Phụ kiện nam');
      });

      // Session::forget('cart');
      return redirect('thanh-toan-thanh-cong.html')->with(['thanh_cong'=>'ok','email'=>$email,'address'=>$address,'phone'=>$phone,'name'=>$name]);

    }else{
      $this->validate($req,
        [
          'email'=>'email|unique:users,email',
          'name'=>'min:5|max:30',
          'phone'=>'min:7|max:15',
        ],
        [
          'email.email'=>'Bạn nhập không đúng định dạng Email',
          'email.unique'=>'Email đã tồn tại. Vui lòng đăng nhập',
          'name.min'=>'Tên chỉ được nhập từ 5 đến 30 kí tự',
          'name.max'=>'Tên chỉ được nhập từ 5 đến 30 kí tự',
          'phone.max'=>'Số điện thoại nhập k đúng',
          'phone.min'=>'Số điện thoại nhập k đúng'
        ]
      );
      $email     = $req->email;
      $address   = $req->address.' - '.$xaPhuong->Name.' - '.$quanHuyen->Name.' - '.$tinhThanh->Name;
      $phone     = $req->email;
      $name      = $req->name;
      $cart    = Cart::content();
      $total   = Cart::subtotal(0);

      $user             = new User;
      $user->name       = $req->name;
      $user->phone      = $req->phone;
      $user->email      = $req->email;
      $user->level      = 2;
      $user->trangthai  = 1;
      $user->ngaydk     = date('Y-m-d H:i:s');
      $user->remember_token = $req->_token;
      $user->address    = $req->address.' - '.$xaPhuong->Name.' - '.$quanHuyen->Name.' - '.$tinhThanh->Name;
      $user->save();

      //lưu bảng đơn hàng
      $donHang           = new DonHang;
      $donHang->idUser   = $user->id;
      $donHang->TongTien = $total;
      $donHang->ThanhToan= 'Tiền mặt';
      $donHang->NgayMua  = date('Y-m-d');
      $donHang->TrangThai= 2; //chờ xác nhận
      $donHang->TinNhan  = $req->note;
      $donHang->save();

      //lưu bảng chi tiết đơn hàng
      foreach ($cart as $items) {
        $ctDonHang            = new CTDonHang;
        $ctDonHang->idDonHang = $donHang->id;
        $ctDonHang->idSanPham = $items->id;
        $ctDonHang->SoLuong   = $items->qty;
        $ctDonHang->Gia       = $items->price;
        $ctDonHang->SizeMau   = $items->options['kichthuoc'].' - '.$items->options['mausac'];
        $ctDonHang->save();
      }

      $data = [
        'name'   => $req->name,
        'email'  => $req->email,
        'phone'  => $req->phone,
        'address'=> $req->address.' - '.$xaPhuong->Name.' - '.$quanHuyen->Name.' - '.$tinhThanh->Name
      ];

      Mail::send('frontend.pages.mail',$data, function ($message) use ($data) {
        $message->from('vuoncghien95@gmail.com', 'Tony4men');

        $message->to($data['email']);
        $message->subject('Tony4men - Thời trang - Phụ kiện nam');
      });

      // Session::forget('cart');
      return redirect('thanh-toan-thanh-cong.html')->with(['thanh_cong'=>'ok','email'=>$email,'address'=>$address,'phone'=>$phone,'name'=>$name]);
    }
    
  }

}
