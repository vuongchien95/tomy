<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CauHinh;
use App\Models\Banner;
use App\Models\ChinhSach;
use App\Models\SanPham;
use App\Models\DanhMuc;
use App\Models\TinTuc;
use Session;

class TrangChuController extends Controller
{
  public function TrangChu(){
    $data['cauhinh']            = CauHinh::first();
    $data['banner']             = Banner::where('AnHien',1)->get();
    $data['chinhsach']          = ChinhSach::all();
    $data['sanphamkhuyenmai']   = SanPham::where([['GiaSale','>',0],['AnHien','=',1]])->get();
    $data['danhmuccha']         = DanhMuc::where('DanhMuc_Cha',0)->get();
    $data['aonam']              = SanPham::where(['Kieu'=> 1, 'AnHien' => 1])->inRandomOrder()->limit(8)->get();
    $data['quannam']            = SanPham::where(['Kieu'=> 2, 'AnHien' => 1])->inRandomOrder()->limit(8)->get();
    $data['giaydep']            = SanPham::where(['Kieu'=> 3, 'AnHien' => 1])->inRandomOrder()->limit(8)->get();
    $data['baloda']             = SanPham::where(['Kieu'=> 4, 'AnHien' => 1])->inRandomOrder()->limit(8)->get();
    $data['phukien']            = SanPham::where(['Kieu'=> 5, 'AnHien' => 1])->inRandomOrder()->limit(8)->get();
    $data['tintuc']             = TinTuc::where('AnHien',1)->limit(5)->orderBy('id','DESC')->get();
    return view('frontend.pages.trangchu',$data);
  }
  
}
