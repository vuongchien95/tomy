<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\SanPham;
use App\Models\SamPhamChiTiet;
use App\Models\SamPhamImage;
use App\Models\DanhMuc;
use App\Models\ChinhSach;
use App\Models\LienHe;
use Auth;

class SanPhamController extends Controller
{
  public function TheLoaiSanPham($slug){
    $data['danhmuc']        = DanhMuc::where('Slug',$slug)->first();
    $data['theloaisanpham'] = SanPham::where(['idDanhMuc'=>$data['danhmuc']->id,'AnHien'=>1])->paginate(12);
    return view('frontend.sanpham.theloaisp',$data);
  }
  public function ChiTietSanPham($slug){
    $data['chinhsach']   = ChinhSach::all();
    $data['lienhe']      = LienHe::select('Sdt')->first();
    $data['sanpham']     = SanPham::where(['Slug'=>$slug,'AnHien'=>1])->first();
    $data['sanphamlq']   = SanPham::where(['Kieu'=>$data['sanpham']->Kieu,'AnHien'=>1])->get();
    $data['sanphamImg']  = SamPhamImage::where(['idSanPham'=>$data['sanpham']->id,'AnHien'=>1])->get();

    $data['sanPhamCT']   = SamPhamChiTiet::where('idSanPham',$data['sanpham']->id)->first();
    if ($data['sanPhamCT']) {
      $data['spChiTiet']   = json_decode($data['sanPhamCT']->SizeMauSL, true);
    }
    return view('frontend.sanpham.chitietsp',$data);
  }

  public function AjaxSize($id,$size){
    $sanPhamCT   = SamPhamChiTiet::where('idSanPham',$id)->first();
    $spChiTiet   = json_decode($sanPhamCT->SizeMauSL, true);
    foreach ($spChiTiet as $key => $items_1) {
      if ($items_1['size'] == $size) {
        foreach ($items_1['mau_sl'] as $key => $items_2) {
          if ($items_2['mau'] != null) {
            echo "<option value='".$items_2['mau']."'>Màu ".$items_2['mau']."</option>";
          }
        }
      }
    }
  }

  public function AjaxMau($id,$mau,$size){
    $sanPhamCT   = SamPhamChiTiet::where('idSanPham',$id)->first();
    $spChiTiet   = json_decode($sanPhamCT->SizeMauSL, true);

    foreach ($spChiTiet as $items_1) {
      if ($items_1['size'] == $size) {
        foreach ($items_1['mau_sl'] as $items_2) {
          if ($items_2['mau'] == $mau) {
            echo $items_2['sl'];
          }
        }
      }
    }
  }

  public function AjaxGia($gia){
    if ($gia == 200) {
      $sanpham = SanPham::where('Gia','<',200000)->orWhere('GiaSale','<',200000)->get();
      if (count($sanpham) > 0) {
        $html = '';
        foreach ($sanpham as $items) {
          $html .= '<div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 product-col">';
            $html .= '<div class="product-box">';
              $html .= '<div class="product-thumbnail">';
                $html .= '<a class="image_link display_flex" href="/san-pham/'.$items->Slug.'">';
                  $html .= '<img src="uploads/sanpham/'.$items->Image.'">';
                $html .= '</a>';
              $html .= '<div class="product-action-grid clearfix">';
                $html .= '<form action="" method="" class="variants form-nut-grid" enctype="multipart/form-data">';
                  $html .= '<div>';
                    $html .= '<a title="xem nhanh" href="javascript:;" id="data-id" data-id="'.$items->id.'" class="button_wh_40 btn_view right-to quick-view"><i class="fa fa-search"></i></a>';
                    $html .= '<a href="javascript:;" id="data-id" data-id="'.$items->id.'" class="button_wh_40 btn-cart left-to muahang" title="Mua hàng"><span>Mua hàng</span></a>';
                    if (Auth::check()) {
                      $html .= '<a title="Yêu thích" id="data-id" data-id="'.$items->id.'" ok="'.Auth::user()->id.'" class="button_wh_40 iWishAdd iwishAddWrapper" href="javascript:;" >
                      <i class="fa fa-heart-o"></i></a>';
                    }else{
                      $html .= '<a title="Yêu thích" class="button_wh_40 iWishAdda iwishAddWrapper" href="javascript:;" ><i class="fa fa-heart-o"></i></a>';
                    }
                  $html .= '</div>';
                $html .= '</form>';
              $html .= '</div>';
            $html .= '</div>';
            $html .= '<div class="product-info a-center">';
              $html .= '<h3 class="product-name">';
                $html .= '<a class="text2line" href="san-pham/'.$items->Slug.'">'.$items->Name.'</a>';
              $html .= '</h3>';
            $html .= '<div class="price-box clearfix">';
              if ($items->GiaSale != '') {
                $html .= '<span class="price product-price">'.number_format($items->GiaSale,0,',','.').'₫</span>';
                $html .= '<span class="price product-price-old">'.number_format($items->Gia,0,',','.').'₫</span>';
              }else{
                $html .= '<span class="price product-price">'.number_format($items->Gia,0,',','.').'₫</span>';
              }
                $html .= '</div>';
              $html .= '</div>';
            $html .= '</div>';
          $html .= '</div>';  
        }
        $html .= '';
        return response()->json(['html'=> $html],200);
      }
    }else if($gia == 300){
      $sanpham = SanPham::whereBetween('Gia', [200000, 300000])->get();
      if (count($sanpham) > 0) {
        $html = '';
        foreach ($sanpham as $items) {
          $html .= '<div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 product-col">';
            $html .= '<div class="product-box">';
              $html .= '<div class="product-thumbnail">';
                $html .= '<a class="image_link display_flex" href="/san-pham/'.$items->Slug.'">';
                  $html .= '<img src="uploads/sanpham/'.$items->Image.'">';
                $html .= '</a>';
              $html .= '<div class="product-action-grid clearfix">';
                $html .= '<form action="" method="" class="variants form-nut-grid" enctype="multipart/form-data">';
                  $html .= '<div>';
                    $html .= '<a title="xem nhanh" href="javascript:;" id="data-id" data-id="'.$items->id.'" class="button_wh_40 btn_view right-to quick-view"><i class="fa fa-search"></i></a>';
                    $html .= '<a href="javascript:;" id="data-id" data-id="'.$items->id.'" class="button_wh_40 btn-cart left-to muahang" title="Mua hàng"><span>Mua hàng</span></a>';
                    if (Auth::check()) {
                      $html .= '<a title="Yêu thích" id="data-id" data-id="'.$items->id.'" ok="'.Auth::user()->id.'" class="button_wh_40 iWishAdd iwishAddWrapper" href="javascript:;" >
                      <i class="fa fa-heart-o"></i></a>';
                    }else{
                      $html .= '<a title="Yêu thích" class="button_wh_40 iWishAdda iwishAddWrapper" href="javascript:;" ><i class="fa fa-heart-o"></i></a>';
                    }
                  $html .= '</div>';
                $html .= '</form>';
              $html .= '</div>';
            $html .= '</div>';
            $html .= '<div class="product-info a-center">';
              $html .= '<h3 class="product-name">';
                $html .= '<a class="text2line" href="san-pham/'.$items->Slug.'">'.$items->Name.'</a>';
              $html .= '</h3>';
            $html .= '<div class="price-box clearfix">';
              if ($items->GiaSale != '') {
                $html .= '<span class="price product-price">'.number_format($items->GiaSale,0,',','.').'₫</span>';
                $html .= '<span class="price product-price-old">'.number_format($items->Gia,0,',','.').'₫</span>';
              }else{
                $html .= '<span class="price product-price">'.number_format($items->Gia,0,',','.').'₫</span>';
              }
                $html .= '</div>';
              $html .= '</div>';
            $html .= '</div>';
          $html .= '</div>';  
        }
        $html .= '';
        return response()->json(['html'=> $html],200);
      }
    }else if($gia == 400){
      $sanpham = SanPham::whereBetween('Gia', [300000, 400000])->get();
      if (count($sanpham) > 0) {
        $html = '';
        foreach ($sanpham as $items) {
          $html .= '<div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 product-col">';
            $html .= '<div class="product-box">';
              $html .= '<div class="product-thumbnail">';
                $html .= '<a class="image_link display_flex" href="/san-pham/'.$items->Slug.'">';
                  $html .= '<img src="uploads/sanpham/'.$items->Image.'">';
                $html .= '</a>';
              $html .= '<div class="product-action-grid clearfix">';
                $html .= '<form action="" method="" class="variants form-nut-grid" enctype="multipart/form-data">';
                  $html .= '<div>';
                    $html .= '<a title="xem nhanh" href="javascript:;" id="data-id" data-id="'.$items->id.'" class="button_wh_40 btn_view right-to quick-view"><i class="fa fa-search"></i></a>';
                    $html .= '<a href="javascript:;" id="data-id" data-id="'.$items->id.'" class="button_wh_40 btn-cart left-to muahang" title="Mua hàng"><span>Mua hàng</span></a>';
                    if (Auth::check()) {
                      $html .= '<a title="Yêu thích" id="data-id" data-id="'.$items->id.'" ok="'.Auth::user()->id.'" class="button_wh_40 iWishAdd iwishAddWrapper" href="javascript:;" >
                      <i class="fa fa-heart-o"></i></a>';
                    }else{
                      $html .= '<a title="Yêu thích" class="button_wh_40 iWishAdda iwishAddWrapper" href="javascript:;" ><i class="fa fa-heart-o"></i></a>';
                    }
                  $html .= '</div>';
                $html .= '</form>';
              $html .= '</div>';
            $html .= '</div>';
            $html .= '<div class="product-info a-center">';
              $html .= '<h3 class="product-name">';
                $html .= '<a class="text2line" href="san-pham/'.$items->Slug.'">'.$items->Name.'</a>';
              $html .= '</h3>';
            $html .= '<div class="price-box clearfix">';
              if ($items->GiaSale != '') {
                $html .= '<span class="price product-price">'.number_format($items->GiaSale,0,',','.').'₫</span>';
                $html .= '<span class="price product-price-old">'.number_format($items->Gia,0,',','.').'₫</span>';
              }else{
                $html .= '<span class="price product-price">'.number_format($items->Gia,0,',','.').'₫</span>';
              }
                $html .= '</div>';
              $html .= '</div>';
            $html .= '</div>';
          $html .= '</div>';  
        }
        $html .= '';
        return response()->json(['html'=> $html],200);
      }
    }else if($gia == 500){
      $sanpham = SanPham::whereBetween('Gia', [400000, 500000])->get();
      if (count($sanpham) > 0) {
        $html = '';
        foreach ($sanpham as $items) {
          $html .= '<div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 product-col">';
            $html .= '<div class="product-box">';
              $html .= '<div class="product-thumbnail">';
                $html .= '<a class="image_link display_flex" href="/san-pham/'.$items->Slug.'">';
                  $html .= '<img src="uploads/sanpham/'.$items->Image.'">';
                $html .= '</a>';
              $html .= '<div class="product-action-grid clearfix">';
                $html .= '<form action="" method="" class="variants form-nut-grid" enctype="multipart/form-data">';
                  $html .= '<div>';
                    $html .= '<a title="xem nhanh" href="javascript:;" id="data-id" data-id="'.$items->id.'" class="button_wh_40 btn_view right-to quick-view"><i class="fa fa-search"></i></a>';
                    $html .= '<a href="javascript:;" id="data-id" data-id="'.$items->id.'" class="button_wh_40 btn-cart left-to muahang" title="Mua hàng"><span>Mua hàng</span></a>';
                    if (Auth::check()) {
                      $html .= '<a title="Yêu thích" id="data-id" data-id="'.$items->id.'" ok="'.Auth::user()->id.'" class="button_wh_40 iWishAdd iwishAddWrapper" href="javascript:;" >
                      <i class="fa fa-heart-o"></i></a>';
                    }else{
                      $html .= '<a title="Yêu thích" class="button_wh_40 iWishAdda iwishAddWrapper" href="javascript:;" ><i class="fa fa-heart-o"></i></a>';
                    }
                  $html .= '</div>';
                $html .= '</form>';
              $html .= '</div>';
            $html .= '</div>';
            $html .= '<div class="product-info a-center">';
              $html .= '<h3 class="product-name">';
                $html .= '<a class="text2line" href="san-pham/'.$items->Slug.'">'.$items->Name.'</a>';
              $html .= '</h3>';
            $html .= '<div class="price-box clearfix">';
              if ($items->GiaSale != '') {
                $html .= '<span class="price product-price">'.number_format($items->GiaSale,0,',','.').'₫</span>';
                $html .= '<span class="price product-price-old">'.number_format($items->Gia,0,',','.').'₫</span>';
              }else{
                $html .= '<span class="price product-price">'.number_format($items->Gia,0,',','.').'₫</span>';
              }
                $html .= '</div>';
              $html .= '</div>';
            $html .= '</div>';
          $html .= '</div>';  
        }
        $html .= '';
        return response()->json(['html'=> $html],200);
      }
    }else if($gia == 1000000){
      $sanpham = SanPham::whereBetween('Gia', [500000, 1000000])->get();
      if (count($sanpham) > 0) {
        $html = '';
        foreach ($sanpham as $items) {
          $html .= '<div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 product-col">';
            $html .= '<div class="product-box">';
              $html .= '<div class="product-thumbnail">';
                $html .= '<a class="image_link display_flex" href="/san-pham/'.$items->Slug.'">';
                  $html .= '<img src="uploads/sanpham/'.$items->Image.'">';
                $html .= '</a>';
              $html .= '<div class="product-action-grid clearfix">';
                $html .= '<form action="" method="" class="variants form-nut-grid" enctype="multipart/form-data">';
                  $html .= '<div>';
                    $html .= '<a title="xem nhanh" href="javascript:;" id="data-id" data-id="'.$items->id.'" class="button_wh_40 btn_view right-to quick-view"><i class="fa fa-search"></i></a>';
                    $html .= '<a href="javascript:;" id="data-id" data-id="'.$items->id.'" class="button_wh_40 btn-cart left-to muahang" title="Mua hàng"><span>Mua hàng</span></a>';
                    if (Auth::check()) {
                      $html .= '<a title="Yêu thích" id="data-id" data-id="'.$items->id.'" ok="'.Auth::user()->id.'" class="button_wh_40 iWishAdd iwishAddWrapper" href="javascript:;" >
                      <i class="fa fa-heart-o"></i></a>';
                    }else{
                      $html .= '<a title="Yêu thích" class="button_wh_40 iWishAdda iwishAddWrapper" href="javascript:;" ><i class="fa fa-heart-o"></i></a>';
                    }
                  $html .= '</div>';
                $html .= '</form>';
              $html .= '</div>';
            $html .= '</div>';
            $html .= '<div class="product-info a-center">';
              $html .= '<h3 class="product-name">';
                $html .= '<a class="text2line" href="san-pham/'.$items->Slug.'">'.$items->Name.'</a>';
              $html .= '</h3>';
            $html .= '<div class="price-box clearfix">';
              if ($items->GiaSale != '') {
                $html .= '<span class="price product-price">'.number_format($items->GiaSale,0,',','.').'₫</span>';
                $html .= '<span class="price product-price-old">'.number_format($items->Gia,0,',','.').'₫</span>';
              }else{
                $html .= '<span class="price product-price">'.number_format($items->Gia,0,',','.').'₫</span>';
              }
                $html .= '</div>';
              $html .= '</div>';
            $html .= '</div>';
          $html .= '</div>';  
        }
        $html .= '';
        return response()->json(['html'=> $html],200);
      }
    }else if($gia == 2000000){
      $sanpham = SanPham::whereBetween('Gia', [1000000, 2000000])->get();
      if (count($sanpham) > 0) {
        $html = '';
        foreach ($sanpham as $items) {
          $html .= '<div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 product-col">';
            $html .= '<div class="product-box">';
              $html .= '<div class="product-thumbnail">';
                $html .= '<a class="image_link display_flex" href="/san-pham/'.$items->Slug.'">';
                  $html .= '<img src="uploads/sanpham/'.$items->Image.'">';
                $html .= '</a>';
              $html .= '<div class="product-action-grid clearfix">';
                $html .= '<form action="" method="" class="variants form-nut-grid" enctype="multipart/form-data">';
                  $html .= '<div>';
                    $html .= '<a title="xem nhanh" href="javascript:;" id="data-id" data-id="'.$items->id.'" class="button_wh_40 btn_view right-to quick-view"><i class="fa fa-search"></i></a>';
                    $html .= '<a href="javascript:;" id="data-id" data-id="'.$items->id.'" class="button_wh_40 btn-cart left-to muahang" title="Mua hàng"><span>Mua hàng</span></a>';
                    if (Auth::check()) {
                      $html .= '<a title="Yêu thích" id="data-id" data-id="'.$items->id.'" ok="'.Auth::user()->id.'" class="button_wh_40 iWishAdd iwishAddWrapper" href="javascript:;" >
                      <i class="fa fa-heart-o"></i></a>';
                    }else{
                      $html .= '<a title="Yêu thích" class="button_wh_40 iWishAdda iwishAddWrapper" href="javascript:;" ><i class="fa fa-heart-o"></i></a>';
                    }
                  $html .= '</div>';
                $html .= '</form>';
              $html .= '</div>';
            $html .= '</div>';
            $html .= '<div class="product-info a-center">';
              $html .= '<h3 class="product-name">';
                $html .= '<a class="text2line" href="san-pham/'.$items->Slug.'">'.$items->Name.'</a>';
              $html .= '</h3>';
            $html .= '<div class="price-box clearfix">';
              if ($items->GiaSale != '') {
                $html .= '<span class="price product-price">'.number_format($items->GiaSale,0,',','.').'₫</span>';
                $html .= '<span class="price product-price-old">'.number_format($items->Gia,0,',','.').'₫</span>';
              }else{
                $html .= '<span class="price product-price">'.number_format($items->Gia,0,',','.').'₫</span>';
              }
                $html .= '</div>';
              $html .= '</div>';
            $html .= '</div>';
          $html .= '</div>';  
        }
        $html .= '';
        return response()->json(['html'=> $html],200);
      }
    }
    
  }




}
