<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CauHinh;
use App\Models\Logo;
use App\Models\Banner;
use App\Models\LienHe;
use App\Models\Video;

use DB;

class CauHinhController extends Controller
{
// .............cấu hình.................................................
	public function getCauHinh(){
		$cauhinh = CauHinh::first();
		return view('backend.cauhinhweb.cauhinh',compact('cauhinh'));
	}
	public function postCauHinh(Request $req){
    
    if ($req->hasFile('favicon')) {
      $file = $req->file('favicon')->getClientOriginalName();
      $filename = rand().'_'.$file; 
      $req->file('favicon')->move('uploads/cauhinh/',$filename);
      $arrInsert = [
        'TieuDe'  => trim($req->tieude),
        'TuKhoa'  => trim($req->tukhoa),
        'MoTa'    => trim($req->mota),
        'Favicon' => $filename
      ];

      DB::table('cauhinh')->truncate();

      $idIn = DB::table('cauhinh')->insertGetId($arrInsert);
      return back();
      
    }else{
      $arrInsert = [
        'TieuDe'  => trim($req->tieude),
        'TuKhoa'  => trim($req->tukhoa),
        'MoTa'    => trim($req->mota)
      ];

      $idIn = DB::table('cauhinh')->update($arrInsert);
      return back();
    }
  }


 // ...............Banner...............................................
  public function getBanner(){
    $data['banner'] = DB::table('banner')->orderby('id','DESC')->get();
		// dd($data['banner']);
    return view('backend.cauhinhweb.banner',$data);
  }
  public function postBanner(Request $req){
    if($files = $req->file('avatar')){
			// DB::table('banner')->delete();
     foreach($files as $file){
      $name = rand().$file->getClientOriginalName();
				// dd($name)
      $banner = DB::table('banner')->insertGetId(['Image' => $name,'AnHien' => 1 ]);
      if ($banner) {
       $file->move('uploads/images/banner',$name);
     }
				// $file->move('uploads/images/banner',$name);
				// DB::table('banner')->insert(['Image' => $name ]);
   }
 }
 return back();
}

public function delBanner($id){
  $del = Banner::find($id);
  $del->delete();
  return redirect()->back();
}

public function getBannerAjax($id){
  $update = Banner::find($id);
  if ($update->AnHien == 1) {
      $update->AnHien = 2; //2 ẩn
    }else{
      $update->AnHien = 1; //1 hiện
    }
    
    $update->save();
  }

	// ...........Logo................................................

  public function getLogo(){
    $data['logo'] = DB::table('logo')->first();
		// dd($data['logo']);
    return view('backend.cauhinhweb.logo',$data);
  }
  public function postLogo(Request $req){

    if($files = $req->file('avatar')){
     DB::table('logo')->delete();
     foreach($files as $file){
      $name = rand().$file->getClientOriginalName();
				// dd($name)
      $banner = DB::table('logo')->insertGetId(['Image' => $name]);
      if ($banner) {
       $file->move('uploads/images/logo',$name);
     }
				// $file->move('uploads/images/banner',$name);
				// DB::table('banner')->insert(['Image' => $name ]);
   }
 }
 return back();
}
public function delLogo($id){
  $del = Logo::find($id);
  $del->delete();
  return redirect()->back();
}

	// ................Liên Hệ Web......................................................
public function getLienHe(){
  $lienhe = LienHe::first();
  return view('backend.cauhinhweb.lienhe',compact('lienhe'));
}
public function postLienHe(Request $req){
  $tieude = trim($req->tencty);
  $box_footer = json_encode($req->box_footer);

  $arrInsert = [
   'TenCty'   => $tieude,
   'NgDaiDien'=> trim($req->ngdaidien),
   'DiaChi'   => trim($req->address),
   'Sdt'      => trim($req->sdt),
   'Hotline'  => trim($req->hotline),
   'Email'    => trim($req->email),
   'Fb'       => trim($req->fb),
   'ZaLo'     => trim($req->zalo),
   'Google'   => trim($req->google),
   'Map'      => trim($req->map)
 ];

 DB::table('lienhe')->truncate();

 $idIn = DB::table('lienhe')->insertGetId($arrInsert);
 return back();

}

	// ......................Video FB...............................................

public function getVideoFb(){
  $video = Video::select()->first();
    // dd($video);
  return view('backend.cauhinhweb.video-fb',compact('video'));
}
public function postVideoFb(Request $req){
  $arrInsert = [
   'LinkVideo'  => trim($req->linkvideo),
   'LinkFb'     => trim($req->linkfb)
 ];

 DB::table('video')->truncate();

 $idIn = DB::table('video')->insertGetId($arrInsert);
 return back();

}

}
