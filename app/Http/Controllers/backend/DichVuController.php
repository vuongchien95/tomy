<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\DichVu;
use App\Models\DanhMuc;
use App\Http\Requests\AdminDVRequest;
use App\Http\Requests\AdminEditDVRequest;
class DichVuController extends Controller
{
	public function List(){
		$list = DichVu::select()->orderBy('id','DESC')->get();
		// dd($list);
		return view('backend.dichvu.list',compact('list'));
	}
	public function getAdd(){
		$data = DanhMuc::select()->get();
		return view('backend.dichvu.add',compact('data'));
	}
	public function postAdd(AdminDVRequest $req){
		$dichvu            = new DichVu;
		// $dichvu->idDanhMuc = $req->danhmuc;
		
		$dichvu->idUser    = Auth::user()->id;
		$dichvu->TieuDe    = title_case($req->name);
		$dichvu->Slug      = str_slug($req->name);
		$dichvu->Gia       = $req->price;
		$dichvu->MoTa      = $req->mota;
		$dichvu->TuKhoaSeo = $req->tukhoaseo;
        $dichvu->TieuDeSeo = $req->tieudeseo;
        $dichvu->MoTaSeo   = $req->motaseo;
		// $dichvu->NgayDang  = date('Y-m-d H:i:s');
		$dichvu->AnHien    = $req->status;


		$file = $req->file('image')->getClientOriginalName();
		$filename = rand().'_'.$file;
		$dichvu->Image = $filename;     
		$req->file('image')->move('uploads/dichvu/',$filename);

		$dichvu->save();
		return redirect()->route('admin.dichvu.list')->with(['thog_bao'=>'Thêm mới Dịch Vụ thành công !!']);
	}

	public function getEdit($id){
		$edit    = DichVu::findOrFail($id);
		// dd($edit);
		$data = DanhMuc::select()->get();
		return view('backend.dichvu.edit',compact('edit','data'));
	}

	public function postEdit(AdminEditDVRequest $req,$id){
		$dichvu            = DichVu::find($id);
		// $dichvu->idDanhMuc = $req->danhmuc;
		$dichvu->idUser    = Auth::user()->id;
		$dichvu->TieuDe    = title_case($req->name);
		$dichvu->Slug      = str_slug($req->name);
		$dichvu->Gia       = $req->price;
		$dichvu->MoTa      = $req->mota;
		$dichvu->TuKhoaSeo = $req->tukhoaseo;
    $dichvu->TieuDeSeo = $req->tieudeseo;
    $dichvu->MoTaSeo   = $req->motaseo;
		// $dichvu->NgayDang  = date('Y-m-d H:i:s');
		$dichvu->AnHien    = $req->status;

		$file_path = public_path('uploads/dichvu/').$dichvu->Image;
    	if ($req->hasFile('image')) {
    		if (file_exists($file_path))
    		{
    			unlink($file_path);
    		}

    		$f = $req->file('image')->getClientOriginalName();
    		$filename = rand().'_'.$f;
    		$dichvu->Image = $filename;       
    		$req->file('image')->move('uploads/dichvu/',$filename);
    	}

		$dichvu->save();
		return redirect()->route('admin.dichvu.list')->with(['thog_bao'=>'Dịch Vụ đã được sửa thành công !!']);
	}
	public function Del($id){
		$del = DichVu::find($id);
		$del->delete();
		return redirect()->back()->with(['thog_bao'=>'Xóa Dịch Vụ thành công!!']);
	}

	public function Detail($id){
		$chitiet = DichVu::find($id);
		return view('backend.dichvu.chitiet',compact('chitiet'));
	}
}
