<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\SanPham;
use App\Models\DanhMuc;
use App\Models\SamPhamImage;
use App\Models\SamPhamChiTiet;
use Auth,DB;
use App\Http\Requests\AdminSpRequest;
use App\Http\Requests\AdminEditSpRequest;

class SanPhamController extends Controller
{
  public function List(){
    $list = SanPham::select()->orderBy('id','DESC')->get();
    return view('backend.sanpham.list',compact('list'));
  }

  public function getAdd(){
    $data = DanhMuc::select()->get();
    return view('backend.sanpham.add',compact('data'));
  }
  public function postAdd(AdminSpRequest $req){
    $sp            = new SanPham;

    $sp->Name      = title_case($req->name);
    $sp->Slug      = str_slug($req->name);
    $sp->idDanhMuc = $req->danhmuc;
    $sp->idUser    = Auth::user()->id;
    $sp->MaSp      = $req->masp;
    $sp->Kieu      = $req->kieu;
    $sp->ThuongHieu= $req->thuonghieu;
    $sp->Gia       = $req->gia;
    $sp->GiaSale   = $req->giasale;
    $sp->SoLanXem  = 1;
    $sp->TuKhoaSeo = $req->tukhoaseo;
    $sp->TieuDeSeo = $req->tieudeseo;
    $sp->MoTaSeo   = $req->motaseo;
    $sp->MoTa      = $req->mota;
    $sp->TrangThai = $req->trangthai;
    $sp->AnHien    = $req->status;
    // $sp->NgayDang  = date('Y-m-d H:i:s');

    $file = $req->file('image')->getClientOriginalName();
    $filename = rand().'_'.$file;
    $sp->Image = $filename;     
    $req->file('image')->move('uploads/sanpham/',$filename);

    $sp->save();
    return redirect()->route('admin.sanpham.list')->with(['thog_bao'=>'Thêm mới Sản Phẩm thành công !!']);
  }

  public function getEdit($id){
    $edit    = SanPham::findOrFail($id);
    $data = DanhMuc::select()->get();
    return view('backend.sanpham.edit',compact('edit','data'));
  }

  public function postEdit(AdminEditSpRequest $req,$id){
    $sp            = SanPham::find($id);
    
    $sp->Name      = title_case($req->name);
    $sp->Slug      = str_slug($req->name);
    $sp->idDanhMuc = $req->danhmuc;
    $sp->idUser    = Auth::user()->id;
    $sp->MaSp      = $req->masp;
    $sp->Kieu      = $req->kieu;
    $sp->ThuongHieu= $req->thuonghieu;
    $sp->Gia       = $req->gia;
    $sp->GiaSale   = $req->giasale;
    $sp->SoLanXem  = 1;
    $sp->TuKhoaSeo = $req->tukhoaseo;
    $sp->TieuDeSeo = $req->tieudeseo;
    $sp->MoTaSeo   = $req->motaseo;
    $sp->MoTa      = $req->mota;
    $sp->TrangThai = $req->trangthai;
    $sp->AnHien    = $req->status;
    // $sp->NgayDang  = date('Y-m-d H:i:s');

    $file_path = public_path('uploads/sanpham/').$sp->Image;
    if ($req->hasFile('image')) {
      if (file_exists($file_path))
      {
        unlink($file_path);
      }

      $f = $req->file('image')->getClientOriginalName();
      $filename = rand().'_'.$f;
      $sp->Image = $filename;       
      $req->file('image')->move('uploads/sanpham/',$filename);
    }
    

    $sp->save();
    return redirect()->route('admin.sanpham.list')->with(['thog_bao'=>'Thông tin Sản Phẩm đã được sửa thành công !!']);
  }
  public function Del($id){
    $del = SanPham::find($id)->delete();
    // $delImg = SamPhamImage::whereIn('idSanPham',[$del->id])->delete();
    // File::delete('resources/upload/'.$product->image);
    return redirect()->back()->with(['thog_bao'=>'Xóa Sản Phẩm thành công!!']);
  }
  public function DelImg($id){
    $del_img = SamPhamImage::find($id)->delete();
    return redirect()->back()->with(['thog_bao'=>'Xóa Ảnh Sản Phẩm thành công!!']);
  }

  public function GetAddDetail($id){
    $sanpham    = SanPham::find($id);
    $sp_img     = SamPhamImage::whereIn('idSanPham',[$sanpham->id])->get();
    $query = SamPhamChiTiet::where('idSanPham',$id)->first();
    if($query){
      $sp_chitiet = json_decode($query->SizeMauSL);
    }

    return view('backend.sanpham.addchitiet',compact('sanpham','sp_img','sp_chitiet','query'));
  }
  
  public function PostAddDetail(Request $req,$id){
    if($files = $req->file('avatar')){
      foreach($files as $file){
        $name = rand().$file->getClientOriginalName();
        $spImg = new SamPhamImage;
        $spImg->idSanPham = $id;
        $spImg->AnHien    = 1;
        $spImg->Image     = $name;
        $spImg->save();
        if ($spImg) {
          $file->move('uploads/images/sanphamimg',$name);
        }
      }
    }

    $sanpham = SanPham::find($id);
    if ($sanpham->Kieu == 1) {
      $jsonao = array(
        [
          'size'=>$req->sizeS,
          'mau_sl'=>array(
            ['mau'=>$req->mau1,'sl'=>$req->soluong1],
            ['mau'=>$req->mau2,'sl'=>$req->soluong2],
            ['mau'=>$req->mau3,'sl'=>$req->soluong3],
            ['mau'=>$req->mau4,'sl'=>$req->soluong4],
            ['mau'=>$req->mau5,'sl'=>$req->soluong5]
          )
        ],
        [
          'size'=>$req->sizeM,
          'mau_sl'=>array(
            ['mau'=>$req->mau6,'sl'=>$req->soluong6],
            ['mau'=>$req->mau7,'sl'=>$req->soluong7],
            ['mau'=>$req->mau8,'sl'=>$req->soluong8],
            ['mau'=>$req->mau9,'sl'=>$req->soluong9],
            ['mau'=>$req->mau10,'sl'=>$req->soluong10]
          )
        ],
        [
          'size'=>$req->sizeL,
          'mau_sl'=>array(
            ['mau'=>$req->mau11,'sl'=>$req->soluong11],
            ['mau'=>$req->mau12,'sl'=>$req->soluong12],
            ['mau'=>$req->mau13,'sl'=>$req->soluong13],
            ['mau'=>$req->mau14,'sl'=>$req->soluong14],
            ['mau'=>$req->mau15,'sl'=>$req->soluong15]
          )
        ],
        [
          'size'=>$req->sizeXL,
          'mau_sl'=>array(
            ['mau'=>$req->mau16,'sl'=>$req->soluong16],
            ['mau'=>$req->mau17,'sl'=>$req->soluong17],
            ['mau'=>$req->mau18,'sl'=>$req->soluong18],
            ['mau'=>$req->mau19,'sl'=>$req->soluong19],
            ['mau'=>$req->mau20,'sl'=>$req->soluong20]
          )
        ],
        [
          'size'=>$req->sizeXXL,
          'mau_sl'=>array(
            ['mau'=>$req->mau21,'sl'=>$req->soluong21],
            ['mau'=>$req->mau22,'sl'=>$req->soluong22],
            ['mau'=>$req->mau23,'sl'=>$req->soluong23],
            ['mau'=>$req->mau24,'sl'=>$req->soluong24],
            ['mau'=>$req->mau25,'sl'=>$req->soluong25]
          )
        ]
      );

      $encodeao   = json_encode($jsonao);

      $spchitiet = new SamPhamChiTiet();
      $spchitiet->idSanPham = $id;
      $spchitiet->SizeMauSl = $encodeao;
      $spchitiet->MaGiamGia = $req->magiamgia;
      $spchitiet->Gia       = $req->gia;

      SamPhamChiTiet::where('idSanPham',$id)->delete();

      $spchitiet->save();


    }elseif($sanpham->Kieu == 2) {
      $jsonquan = array(
        [
          'size'=>$req->size28,
          'mau_sl'=>array(
            ['mau'=>$req->mau26,'sl'=>$req->soluong26],
            ['mau'=>$req->mau27,'sl'=>$req->soluong27],
            ['mau'=>$req->mau28,'sl'=>$req->soluong28],
            ['mau'=>$req->mau29,'sl'=>$req->soluong29],
            ['mau'=>$req->mau30,'sl'=>$req->soluong30]
          )
        ],
        [
          'size'=>$req->size29,
          'mau_sl'=>array(
            ['mau'=>$req->mau31,'sl'=>$req->soluong31],
            ['mau'=>$req->mau32,'sl'=>$req->soluong32],
            ['mau'=>$req->mau33,'sl'=>$req->soluong33],
            ['mau'=>$req->mau34,'sl'=>$req->soluong34],
            ['mau'=>$req->mau35,'sl'=>$req->soluong35]
          )
        ],
        [
          'size'=>$req->size30,
          'mau_sl'=>array(
            ['mau'=>$req->mau36,'sl'=>$req->soluong36],
            ['mau'=>$req->mau37,'sl'=>$req->soluong37],
            ['mau'=>$req->mau38,'sl'=>$req->soluong38],
            ['mau'=>$req->mau39,'sl'=>$req->soluong39],
            ['mau'=>$req->mau40,'sl'=>$req->soluong40]
          )
        ],
        [
          'size'=>$req->size31,
          'mau_sl'=>array(
            ['mau'=>$req->mau41,'sl'=>$req->soluong41],
            ['mau'=>$req->mau42,'sl'=>$req->soluong42],
            ['mau'=>$req->mau43,'sl'=>$req->soluong43],
            ['mau'=>$req->mau44,'sl'=>$req->soluong44],
            ['mau'=>$req->mau45,'sl'=>$req->soluong45]
          )
        ],
        [
          'size'=>$req->size32,
          'mau_sl'=>array(
            ['mau'=>$req->mau46,'sl'=>$req->soluong46],
            ['mau'=>$req->mau47,'sl'=>$req->soluong47],
            ['mau'=>$req->mau48,'sl'=>$req->soluong48],
            ['mau'=>$req->mau49,'sl'=>$req->soluong49],
            ['mau'=>$req->mau50,'sl'=>$req->soluong50]
          )
        ],
        [
          'size'=>$req->size33,
          'mau_sl'=>array(
            ['mau'=>$req->mau51,'sl'=>$req->soluong51],
            ['mau'=>$req->mau52,'sl'=>$req->soluong52],
            ['mau'=>$req->mau53,'sl'=>$req->soluong53],
            ['mau'=>$req->mau54,'sl'=>$req->soluong54],
            ['mau'=>$req->mau55,'sl'=>$req->soluong55]
          )
        ]
      );

      $encodequan = json_encode($jsonquan);
      $spchitiet = new SamPhamChiTiet();
      $spchitiet->idSanPham = $id;
      $spchitiet->SizeMauSl = $encodequan;
      $spchitiet->MaGiamGia = $req->magiamgia;
      $spchitiet->Gia       = $req->gia;

      SamPhamChiTiet::where('idSanPham',$id)->delete();

      $spchitiet->save();
    }elseif($sanpham->Kieu == 3) {
      $jsongiay = array(
        [
          'size'=>$req->sizegiay38,
          'mau_sl'=>array(
            ['mau'=>$req->mau56,'sl'=>$req->soluong56],
            ['mau'=>$req->mau57,'sl'=>$req->soluong57],
            ['mau'=>$req->mau58,'sl'=>$req->soluong58],
            ['mau'=>$req->mau59,'sl'=>$req->soluong59],
            ['mau'=>$req->mau60,'sl'=>$req->soluong60]
          )
        ],
        [
          'size'=>$req->sizegiay39,
          'mau_sl'=>array(
            ['mau'=>$req->mau61,'sl'=>$req->soluong61],
            ['mau'=>$req->mau62,'sl'=>$req->soluong62],
            ['mau'=>$req->mau63,'sl'=>$req->soluong63],
            ['mau'=>$req->mau64,'sl'=>$req->soluong64],
            ['mau'=>$req->mau65,'sl'=>$req->soluong65]
          )
        ],
        [
          'size'=>$req->sizegiay40,
          'mau_sl'=>array(
            ['mau'=>$req->mau66,'sl'=>$req->soluong66],
            ['mau'=>$req->mau67,'sl'=>$req->soluong67],
            ['mau'=>$req->mau68,'sl'=>$req->soluong68],
            ['mau'=>$req->mau69,'sl'=>$req->soluong69],
            ['mau'=>$req->mau70,'sl'=>$req->soluong70]
          )
        ],
        [
          'size'=>$req->sizegiay41,
          'mau_sl'=>array(
            ['mau'=>$req->mau71,'sl'=>$req->soluong71],
            ['mau'=>$req->mau72,'sl'=>$req->soluong72],
            ['mau'=>$req->mau73,'sl'=>$req->soluong73],
            ['mau'=>$req->mau74,'sl'=>$req->soluong74],
            ['mau'=>$req->mau75,'sl'=>$req->soluong75]
          )
        ],
        [
          'size'=>$req->sizegiay42,
          'mau_sl'=>array(
            ['mau'=>$req->mau76,'sl'=>$req->soluong76],
            ['mau'=>$req->mau77,'sl'=>$req->soluong77],
            ['mau'=>$req->mau78,'sl'=>$req->soluong78],
            ['mau'=>$req->mau79,'sl'=>$req->soluong79],
            ['mau'=>$req->mau80,'sl'=>$req->soluong80]
          )
        ],
        [
          'size'=>$req->sizegiay43,
          'mau_sl'=>array(
            ['mau'=>$req->mau81,'sl'=>$req->soluong81],
            ['mau'=>$req->mau82,'sl'=>$req->soluong82],
            ['mau'=>$req->mau83,'sl'=>$req->soluong83],
            ['mau'=>$req->mau84,'sl'=>$req->soluong84],
            ['mau'=>$req->mau85,'sl'=>$req->soluong85]
          )
        ]
      );

      $encodegiay = json_encode($jsongiay);
      $spchitiet = new SamPhamChiTiet();
      $spchitiet->idSanPham = $id;
      $spchitiet->SizeMauSl = $encodegiay;
      $spchitiet->MaGiamGia = $req->magiamgia;
      $spchitiet->Gia       = $req->gia;

      SamPhamChiTiet::where('idSanPham',$id)->delete();

      $spchitiet->save();
    }

    return redirect()->back()->with(['thog_bao'=>'Thêm thông tin thành công!!']);
  }

  public function Detail($id){

    $chitiet = SanPham::find($id);
    return view('backend.sanpham.chitiet',compact('chitiet'));
  }

  
}






