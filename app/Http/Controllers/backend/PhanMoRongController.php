<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\GioiThieu;
use App\Models\ChinhSach;
use DB;

class PhanMoRongController extends Controller
{

  //..........................giới thiệu................................
  public function getGioiThieu(){
    $gioithieu = GioiThieu::first();
    return view('backend.phanmorong.gioithieu',compact('gioithieu'));
  }
  public function postGioiThieu(Request $req){
    $gioithieu = GioiThieu::first();
    if ($gioithieu) 
    {
      $gt = GioiThieu::find($gioithieu->id);
      $gt->GioiThieu = $req->gioithieu;

      $gt->save();
    }
    else
    {
      $gt = new GioiThieu();
      $gt->GioiThieu = $req->gioithieu;

      $gt->save();
    }

    return back()->with('thog_bao','Thay đổi thành công !!');
  }


  //..........................chính sách ưu đãi................................
  
  public function ChinhSach(){
    $list = ChinhSach::all();
    return view('backend.phanmorong.chinhsach',compact('list'));
  }

  public function getAddChinhSach(){
    return view('backend.phanmorong.addchinhsach');
  }

  public function postAddChinhSach(Request $req){
    $chinhsach               = new ChinhSach();
    $chinhsach->TenChinhSach = title_case($req->tenchinhsach);
    $chinhsach->MoTa         = $req->mota;

    $file = $req->file('image')->getClientOriginalName();
    $filename = rand().'_'.$file;
    $chinhsach->Image = $filename;     
    $req->file('image')->move('uploads/chinhsach/',$filename);

    $chinhsach->save();
    return redirect('admin/phanmorong/chinhsach')->with('thog_bao','Thêm mới chính sách - ưu đãi thành công !!');
  }

  public function getEditChinhSach($id){
    $edit = ChinhSach::find($id);
    return view('backend.phanmorong.editchinhsach',compact('edit'));
  }
  public function postEditChinhSach(Request $req,$id){
    $editcs               = ChinhSach::find($id);
    $editcs->TenChinhSach = title_case($req->tenchinhsach);
    $editcs->MoTa         = $req->mota;

    $file_path = public_path('uploads/chinhsach/').$editcs->Image;
    if ($req->hasFile('image')) {
      if (file_exists($file_path))
      {
        unlink($file_path);
      }

      $f = $req->file('image')->getClientOriginalName();
      $filename = rand().'_'.$f;
      $editcs->Image = $filename;       
      $req->file('image')->move('uploads/chinhsach/',$filename);
    }

    $editcs->save();
    return redirect('admin/phanmorong/chinhsach')->with('thog_bao','Thay đổi chính sách - ưu đãi thành công !!');
  }

  public function DelChinhSach($id){
    $delcs = ChinhSach::find($id);
    $delcs->delete();
    return redirect('admin/phanmorong/chinhsach')->with('thog_bao','Xóa chính sách - ưu đãi thành công !!');
  }
}
