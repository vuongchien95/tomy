<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminDVRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'unique:dichvu,TieuDe|max:255',
            'status' => 'required'
            'image' =>'required'
        ];
    }

    public function messages(){
        return 
        [
            'name.unique' => 'Dịch Vụ này bạn đã đăng từ trước',
            'name.max'    => 'Tiêu Đề bạn không được nhập quá 255 ký tự',
            'status.required' => 'Trạng Thái không được đê trống'
            'image.required' => 'Vui lòng chọn Ảnh minh họa'
        ];
    }
}
