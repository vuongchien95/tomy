<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DoiMKRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            're_password' => 'same:password',
            'password'=>'min:6'
        ];
    }
    public function messages(){
        return[
            're_password.same' => 'Mật khẩu không trùng khớp.',
            'password.min'=>'Mật khẩu phải lớn hơn 6 ký tự'
        ];
    }
}
