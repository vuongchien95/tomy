<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class AdminEditURequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'min:3|max:50'
        ];
    }

    public function messages(){
        return 
        [
            'name.min' => 'Tên phải từ 3 ký tự trở lên',
            'name.max'    => 'Tên bạn không được nhập quá 50 ký tự',
        ];
    }
}
