<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminEditSpRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'max:50',
            'status' => 'required',
            'danhmuc' =>'required',
            'trangthai' =>'required'
        ];
    }

    public function messages(){
        return 
        [
            'name.max'    => 'Tên Sản Phẩm bạn không được nhập quá 50 ký tự',
            'status.required' => 'Trạng Thái không được đê trống',
            'danhmuc.required' => 'Vui lòng chọn Thể Loại tương ứng',
            'trangthai.required' => 'Vui lòng chọn Trạng Thái'
        ];
    }
}
