<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminSpRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'unique:sanpham,Name|max:50',
            'status' => 'required',
            'danhmuc' =>'required',
            'image' =>'required',
            'trangthai' =>'required',
            'kieu'=>'required'
        ];
    }

    public function messages(){
        return 
        [
            'name.unique' => 'Sản Phẩm này bạn đã đăng từ trước',
            'name.max'    => 'Tên Sản Phẩm bạn không được nhập quá 50 ký tự',
            'status.required' => 'Trạng Thái không được đê trống',
            'danhmuc.required' => 'Vui lòng chọn Thể Loại tương ứng',
            'image.required' => 'Vui lòng chọn Ảnh minh họa',
            'trangthai.required' => 'Vui lòng chọn Trạng Thái',
            'kieu.required'=> 'Vui lòng chọn kiểu sản phẩm'
        ];
    }
}
