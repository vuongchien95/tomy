<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FronURequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
      return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
        'name' => 'unique:users,name|min:3|max:50',
        'phone' => 'unique:users,phone',
        'email' => 'unique:users,email',
        'password'=>'min:6'
      ];
    }

    public function messages(){
      return 
      [
        'name.min' => 'Tên phải từ 3 ký tự trở lên',
        'name.unique' => 'Tên này đã tồn tại',
        'name.max'    => 'Tên bạn không được nhập quá 50 ký tự',
        'phone.unique' =>'Số điện thoại này đã tồn tại',
        'email.unique' => 'Email đã tồn tại',
        'password.min'=>'Mật khẩu phải lớn hơn 6 ký tự'
      ];
    }
  }
