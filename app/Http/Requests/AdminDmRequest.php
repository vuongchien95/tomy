<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminDmRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'unique:danhmuc,Name|min:2|max:30'
            /*,
            'status' => 'required'*/
        ];
    }

    public function messages(){
        return 
        [
            'name.unique' => 'Tên Danh Mục đã tồn tại',
            'name.min'    => 'Tên Danh Mục yêu cầu từ 2 ký tự trở lên',
            'name.max'    => 'Tên Danh Mục không được nhập quá 30 ký tự'/*,
            'status.required' => 'Trạng Thái không được đê trống'*/
        ];
    }
}
