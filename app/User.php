<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'passwword', 'remember_token',
    ];

    public function YeuThich(){
        return $this->hasMany('App\Models\YeuThich','idUser','id');
        // 1 user có thể yêu thích nhiều sản phẩm khác nhau
    }
    public function TinTuc(){
        return $this->hasMany('App\Models\TinTuc','idUser','id');
        // 1 user có thể đăng nhiều tin tức
    }

    public function DichVu(){
        return $this->hasMany('App\Models\DichVu','idUser','id');
        // 1 user có thể đăng nhiều dich vụ
    }

    public function DonHang(){
        return $this->hasMany('App\Models\DonHang','idUser','id');
        // 1 người dùng thì có nhiều đơn hàng.
    }

    public function SanPham(){
        return $this->hasMany('App\Models\SanPham','idUser','id');
        // 1 user có thể đăng nhiều sản phẩm
    }

    public function BinhLuan(){
        return $this->hasMany('App\Models\BinhLuan','idUser','id');
        // 1 người dùng thì có nhiều đơn hàng.
    }
}
