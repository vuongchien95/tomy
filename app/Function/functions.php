<?php
	// Các functon được để trong composer.json
	// Và được khai báo trong autoload
	function stripUnicode($str){
	  if(!$str) return false;
	   $unicode = array(
	       	'a'=>'á|à|ả|ã|ạ|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ',
            'A'=>'Á|À|Ả|Ã|Ạ|Ă|Ắ|Ằ|Ẳ|Ẵ|Ặ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
            'd'=>'đ',
            'D'=>'Đ',
            'e'=>'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ',
            'E'=>'É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
            'i'=>'í|ì|ỉ|ĩ|ị',
            'I'=>'Í|Ì|Ỉ|Ĩ|Ị',
            'o'=>'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',
            'O'=>'Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',
            'u'=>'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',
            'U'=>'Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',
            'y'=>'ý|ỳ|ỷ|ỹ|ỵ',
            'Y'=>'Ý|Ỳ|Ỷ|Ỹ|Ỵ',
            '' =>'?|(|)|[|]|{|}|#|%|-|<|>|,|:|;|.|&|–|/'
	   );
		foreach($unicode as $khongdau=>$codau){
			$arr=explode("|", $codau);
			$str=str_replace($arr, $khongdau, $str);
		}
		return $str;
	}
	function changeTitle($str){
		$str = trim($str);
		if ($str=="") {
			return "";
		}
		$str = str_replace('"', '', $str);
		$str = str_replace(" ' ", '', $str);
		$str = stripUnicode($str);
		$str = mb_convert_case($str, MB_CASE_LOWER,'utf-8');
		// MB_CASE_UPPER : IN HOA
		// MB_CASE_TITLE : In Chữ Cái Đầu Tiên
		// MB_CASE_LOWER : in thường
		$str = str_replace(' ', ' - ', $str);
		return $str;
	}
	// Su dung de quy tạo menu
	function cat_parent($data,$parent = 0,$str="--",$select = 0){
		foreach ($data as $value) {
			$id = $value["id"];
			$name=$value["Name"];
			if ($value['DanhMuc_Cha'] == $parent) {
				if ($select != 0 && $id == $select) {
					echo "<option value='$id' selected = 'selected'>$str $name</option>";	
				}
				else{
					echo "<option value='$id'>$str $name</option>";	
				}
				cat_parent($data,$id,$str." --" ,$select);
			}		
		}
	}


//

//   function cate($data, $parent_id = 0, $char1 = '', $char2 = '', $level = 1)
// {
//     // BƯỚC 2.1: LẤY DANH SÁCH CATE CON
//     $cate_child = array();
//     foreach ($data as $key => $item)
//     {
//         // Nếu là chuyên mục con thì hiển thị
//         if ($item->parent_id == $parent_id)
//         {
//             $cate_child[] = $item;
//             unset($data[$key]);
//         }
//     }
     
//     // BƯỚC 2.2: HIỂN THỊ DANH SÁCH CHUYÊN MỤC CON NẾU CÓ
//     if ($cate_child)
//     {
        
//         foreach ($cate_child as $key => $item)
//         {
//             // Hiển thị tiêu đề chuyên mục
//             echo $char1;
//             echo "<li class='dropdown'>";
//             if ($level == 1) {
//                 echo "<a href='#' class='link dropdown-toggle' data-toggle='dropdown'>$item->name<b class='caret'></b></a>";
//             }else {
//                  // echo "<a href='#' class='dropdown-toggle'>$item->name</a>";
//                  echo "<a href='#' class='link dropdown-toggle' data-toggle='dropdown'>$item->name<b class='caret'></b></a>";
//             }
            

//             // Tiếp tục đệ quy để tìm chuyên mục con của chuyên mục đang lặp
//             cate($data, $item->id, '<ul class="dropdown-menu">', '</ul>', 2);
//             echo $char2;
//             echo '</li>';
            
//         }
       
//     }
// }
// 
// 
// <?php
/**
 * [showCategories description] Đệ quy bên quản lý danh mục
 */
// function showCategories($categories, $cat_parent_id = 0, $string = '')
// {
//     $cate_child = array();
//     foreach ($categories as $key => $item)
//     {
//         if ($item->cat_parent_id == $cat_parent_id)
//         {
//             $cate_child[] = $item;
//             unset($categories[$key]);
//         }
//     }
    
//     if ($cate_child)
//     {
//         foreach ($cate_child as $key => $item)
//         {
//             echo '<li><a class="link-cate" title="Sửa" href="sua-danh-muc/'.$item->cat_id.'">'.$string.$item->cat_name.'</a><div style="float:right">';
//             echo '<a class="link-cate" title="Sửa" href="sua-danh-muc/'.$item->cat_id.'"><i class="icon-pencil"></i> Sửa </a>';
//             echo '<a class="link-cate" title="Xóa" href="xoa-danh-muc/'.$item->cat_id.'"><i class="icon-remove"></i> Xóa </a>';
//             echo '</div>';

//                 showCategories($categories, $item->cat_id, $string.'---| ');
//             echo '</li>';
//         }
//     }
// }


// function selectCates( $categories, $cat_parent_id = 0, $string = '', $cate = null, $cateIDParen = '')
// {
//     $cate_child = array();

//     foreach ($categories as $key => $itemCT)
//     {
//         if ($itemCT->cat_parent_id == $cat_parent_id)
//         {
//             $cate_child[] = $itemCT;
//             if($itemCT->cat_parent_id == 0)
//             {
//                 // $cateIDParen[] = $itemCT;
//             }
//             else
//             {
//                 $cateIDParen = $itemCT->cat_parent_id;
//             }
//             unset($categories[$key]);
//         }
//     }

//     if ($cate_child) {
//         foreach ($cate_child as $key => $item)
//         {
//             if($item->cat_id == $cate) { $checked = 'selected'; }
//             else { $checked = ''; }

//             echo '<option data-id='.$item->cat_id.' data-par='.json_encode($cateIDParen).' '.$checked.' value="'.$item->cat_id.'">'.$string.$item->cat_name.'</option>';
//             selectCates( $categories, $item->cat_id, $string.'---| ', $cate, $cateIDParen);
//         }
//     }
// }


// function menuFrontEnd($categories, $cat_parent_id = 0, $string_first = '', $string_last = '', $class_li = '')
// {
//     $cate_child = array();
//     foreach ($categories as $key => $item)
//     {
//         if ($item->cat_parent_id == $cat_parent_id)
//         {
//             $cate_child[] = $item;
//             unset($categories[$key]);
//         }
//     }

//     if ($cate_child)
//     {
//         echo $string_first;
//         foreach ($cate_child as $key => $item)
//         {
//             if($key > 1) {
//                 $class = 'dropdown-menu-right';
//             }
//             else { $class = ''; }
            
//             echo "<li class='".$class_li."'>";
//             echo "<a href='/vinhomes/".$item->cat_id."/".$item->cat_slug.".html'>".$item->cat_name."</a>";
//             menuFrontEnd($categories, $item->cat_id, '<ul class="dropdown'.$class.'">', '</ul>', 'drop');
//             echo "</li>";
//         }
//         echo $string_last;
//     }
// }


// <script type="text/javascript">
//       $(document).ready(function() {
//           $('li.drop').hover(
//            function() {
//                $(this).children("ul").fadeIn(10);
//            },
//            function() {
//                $(this).children("ul").fadeOut(10);
//               }
//              )
//       })
//   </script>

//   $('#select_cate option').each(function(){
//             var dis = $(this).data('par');
//             var elm = $(this).val();
//             if(dis != '')
//             {
//                 $('#select_cate option[data-id='+dis+']').attr("disabled",true);
//             }

//         });

?>