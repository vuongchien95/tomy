<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class YeuThich extends Model
{
	protected $table = 'yeuthich';
    protected $guarded =[];


    public function SanPham(){
    	return $this->hasOne('App\Models\SanPham','idSanPham','id');
    	// 1 sản phẩm chỉ yêu thích được 1 lần/user
    }

    public function User(){
    	return $this->belongsTo('App\User','idUser','id');
    	// 1 sp yêu thích chỉ thuộc về 1 user
    }
}
