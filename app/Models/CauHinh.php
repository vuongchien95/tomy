<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CauHinh extends Model
{
    protected $table = 'cauhinh';
    protected $guarded =[];
    public $timestamps = false;
}
