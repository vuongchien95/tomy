<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DanhMuc extends Model
{
    protected $table = 'danhmuc';
    protected $guarded =[];
    // public $timestamps = false;

    public function SanPham(){
    	return $this->hasMany('App\Models\SanPham','idDanhMuc','id');
    	//hasMany: 1 danh mục thì có nhiều sản phẩm.
    }

    public function TinTuc(){
      return $this->hasMany('App\Models\TinTuc','idDanhMuc','id');
      //hasMany: 1 danh mục thì có nhiều tin tức.
    }

    public function DichVu(){
      return $this->hasMany('App\Models\DichVu','idDanhMuc','id');
      //hasMany: 1 danh mục thì có nhiều dịch vụ.
    }

    public function User(){
      return $this->belongsTo('App\User','idUser','id');
      // 1 danh muc thuoc ve 1 ng đăng
    }
}
