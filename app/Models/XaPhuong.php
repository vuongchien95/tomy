<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class XaPhuong extends Model
{
    protected $table = 'xaphuong';
    protected $guarded =[];//
    public $timestamps = false;

    public function QuanHuyen(){
      return $this->belongsTo('App\Models\QuanHuyen','idQuanHuyen','id');
      // 1 xã phường chỉ thuộc về 1 quận huyện
    }
}
