<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CTDonHang extends Model
{
    protected $table = 'chitiet_donhang';
    protected $guarded =[];
    // public $timestamps = false;

    public function DonHang(){
    	return $this->belongsTo('App\Models\DonHang','idDonHang','id');
   		// 1 chi tiết chỉ thuộc về 1 đơn hàng.
    }

    public function SanPham(){
    	return $this->belongsTo('App\Models\SanPham','idSanPham','id');
    	// 1 chi tiết thì chỉ thuộc về 1 sản phẩm.
    }
}
