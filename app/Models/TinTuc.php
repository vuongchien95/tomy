<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TinTuc extends Model
{
    protected $table = 'tintuc';
    protected $guarded =[];


    // public function DanhMuc(){
    // 	return $this->belongsTo('App\Models\DanhMuc','idDanhMuc','id');
    // 	// 1 tin tức thuộc về 1 danh mục
    // }

    public function User(){
    	return $this->belongsTo('App\User','idUser','id');
    	// 1 tin tuc thuoc ve 1 ng đăng
    }
}
