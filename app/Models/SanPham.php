<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SanPham extends Model
{
	protected $table = 'sanpham';
    protected $guarded =[];


    public function DanhMuc(){
    	return $this->belongsTo('App\Models\DanhMuc','idDanhMuc','id');
    	// 1 sản phẩm thuộc về 1 danh mục
    }

    public function User(){
        return $this->belongsTo('App\User','idUser','id');
        // 1 sản phẩm thuộc về 1 ng đăng
    }

    public function CTDonHang(){
    	return $this->hasMany('App\Models\CTDonHang','idSanPham','id');
    	// 1 sản phẩm có nhiều hóa đơn
    }

    public function YeuThich(){
    	return $this->hasOne('App\Models\YeuThich','idSanPham','id');
    	// 1 sp chỉ yêu thích được 1 lần/user
    }
}
