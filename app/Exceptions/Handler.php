<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
      'password',
      'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
      parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
      return parent::render($request, $exception);

        // return response("<div style='color:grey;text-align:center;font-size:40px;margin-top:1em'><span style='font-size:111px;color:black;margin-bottom:1em'>404</span><span style='font-size:20px;color:black;border-bottom: 1px solid black;display: block;width: 300px;margin: 0 auto'>NOT FOUND</span><br/><div style='font-size:20px;margin-bottom: 1em'>404 - Không tìm thấy nội dung yêu cầu</div></div><br>
        //   <div style='text-align: center'><a href='javascript:history.back()' style='background: black;padding: 8px 15px;border-radius: 5px;text-decoration: none;color:#fff'>Quay Lại</a></div>", 404);


    }
    
  }
