<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Schema;
use App\Models\CauHinh;
use App\Models\LienHe;
use App\Models\LoGo;
use App\Models\DanhMuc;
use App\Models\SanPham;
use App\Models\TinTuc;
use Cart;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
      Schema::defaultStringLength(191);
      view()->composer('frontend.layout.master',function($view){
        $data['cauhinh']    = CauHinh::first();
        $data['menu_cap_1'] = DanhMuc::where('DanhMuc_Cha',0)->get();
        $data['cart']       = Cart::content();
        $data['total']      = Cart::subtotal(0);
        $data['lienhe'] = LienHe::first();
        // Cart::destroy();
        $view->with($data);
      });
      view()->composer('frontend.layout.header',function($view){
        $data['lienhe'] = LienHe::first();
        $view->with($data);
      });
      view()->composer('frontend.layout.menu',function($view){
        $data['logo']       = LoGo::first();
        $data['menu_cap_1'] = DanhMuc::where('DanhMuc_Cha',0)->get();
        $data['cart']       = Cart::content();
        $data['total']      = Cart::subtotal(0);
        $view->with($data);
      });
      view()->composer('frontend.layout.footer',function($view){
        $data['lienhe'] = LienHe::first();
        $view->with($data);
      });
      view()->composer('frontend.layout.sidebarSp',function($view){
        $data['sanphambanchay']    = SanPham::where('AnHien',1)->inRandomOrder()->limit(4)->get();
        $data['menu_cap_1']        = DanhMuc::where('DanhMuc_Cha',0)->get();
        $view->with($data);
      });
      view()->composer('frontend.layout.sidebarTt',function($view){
        $data['tintuc'] = TinTuc::where('AnHien',1)->inRandomOrder()->limit(10)->get();
        $view->with($data);
      });
      view()->composer('frontend.layout.toolbar',function($view){
        $data['cart']       = Cart::content();
        $data['total']      = Cart::subtotal(0);
        $view->with($data);
      });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
  }
