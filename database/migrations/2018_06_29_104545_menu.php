<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Menu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('menu', function (Blueprint $table) {
        $table->increments('id');

        $table->string('TenMenu', 255);
        $table->string('Slug', 255);
        $table->string('SapXep', 255)->nullable();
        $table->tinyInteger('AnHien')->nullable();
        

        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('menu');
    }
  }
