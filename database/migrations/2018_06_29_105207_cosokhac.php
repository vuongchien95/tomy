<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Cosokhac extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('cosokhac', function (Blueprint $table) {
        $table->increments('id');
        $table->string('TenCoSo', 255)->nullable();
        $table->string('NgDaiDien', 255)->nullable();
        $table->string('DiaChi', 255)->nullable();
        $table->string('Sdt', 255)->nullable();
        $table->string('Hotline', 255)->nullable();
        $table->string('Email', 255)->nullable();
        $table->string('Fb', 255)->nullable();
        $table->string('Zalo', 255)->nullable();
        $table->string('Google', 255)->nullable();
        $table->text('Map')->nullable();
        
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('cosokhac');
    }
  }
