<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Tintuc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tintuc', function (Blueprint $table) {
            $table->increments('id');
            // $table->integer('idDanhMuc')->unsigned();
            // $table->foreign('idDanhMuc')->references('id')->on('danhmuc')->onDelete('cascade');
            $table->integer('idUser')->unsigned()->nullable();
            $table->foreign('idUser')->references('id')->on('users')->onDelete('cascade');
            $table->string('TieuDe', 255);
            $table->string('Image', 100)->nullable();
            $table->string('Slug', 255);
            $table->text('TomTat')->nullable();
            $table->text('TuKhoaSeo')->nullable();
            $table->text('TieuDeSeo')->nullable();
            $table->text('MoTaSeo')->nullable();
            $table->text('MoTa')->nullable();
            $table->tinyInteger('AnHien')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tintuc');
    }
}
