<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SanphamChitiet extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('sanpham_chitiet', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('idSanPham')->unsigned();
        $table->foreign('idSanPham')->references('id')->on('sanpham')->onDelete('cascade');
        $table->text('SizeMauSL')->nullable();
        $table->string('MaGiamGia', 100)->nullable();
        $table->integer('Gia')->nullable();

        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('sanpham_chitiet');
    }
  }
