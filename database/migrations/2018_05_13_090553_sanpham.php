<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Sanpham extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sanpham', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idDanhMuc')->unsigned();
            $table->foreign('idDanhMuc')->references('id')->on('danhmuc')->onDelete('cascade');
            $table->integer('idUser')->unsigned()->nullable();
            $table->foreign('idUser')->references('id')->on('users')->onDelete('cascade');
            $table->string('Name', 255);
            $table->string('Slug', 255);
            $table->string('MaSp', 100)->nullable();
            $table->string('Image', 100);
            $table->string('ThuongHieu', 100)->nullable();
            $table->integer('Gia');
            $table->integer('GiaSale')->nullable();
            $table->integer('SoLanXem')->nullable();
            $table->text('TuKhoaSeo')->nullable();
            $table->text('TieuDeSeo')->nullable();
            $table->text('MoTaSeo')->nullable();
            $table->text('MoTa')->nullable();
            $table->tinyInteger('TrangThai')->nullable();
            $table->integer('Kieu')->nullable();
            $table->tinyInteger('AnHien')->nullable();
            

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sanpham');
    }
}
