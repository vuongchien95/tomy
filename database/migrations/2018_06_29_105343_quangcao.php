<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Quangcao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('quangcao', function (Blueprint $table) {
        $table->increments('id');
        $table->string('TenQC', 100)->nullable();
        $table->string('Link', 100)->nullable();
        $table->string('Image', 100)->nullable();
        $table->tinyInteger('AnHien')->nullable();

        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('quangcao');
    }
}
