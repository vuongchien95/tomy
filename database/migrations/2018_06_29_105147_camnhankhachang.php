<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Camnhankhachang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('camnhankh', function (Blueprint $table) {
        $table->increments('id');
        $table->string('TenKH', 255);
        $table->string('Slug', 255);
        $table->string('Image', 100)->nullable();
        $table->string('CamNhan', 255)->nullable();
        $table->tinyInteger('AnHien')->nullable();
        
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('camnhankh');
    }
}
