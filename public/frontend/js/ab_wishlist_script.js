(function (window, document, jqueryVer, callback) {
    var $, state, done = false;
    if (!($ = window.jQuery) || jqueryVer > $.fn.jquery || callback($, done)) {
        var script = document.createElement("script");
        script.type = "text/javascript",
    script.src = "https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js",
    script.onload = script.onreadystatechange = function () {
        if (!done && (!(state = this.readyState) || state === "loaded" || state === "complete")) {
            callback(($ = window.jQuery).noConflict(1), done = true);
            $(script).remove();
        }
    };
        try {
            document.body.appendChild(script);
        }
        catch (ex) {
            try {
                document.documentElement.childNodes[0].appendChild(script);
            }
            catch (ex) { }
        }
    }
})(window, document, "1.9.1", function ($, done) {
    'use strict';

    window.ABWishList = window.ABWishList || {};
    window.ABWishList.isLoading = false;
    window.ABWishListFormatMoney = "";
    var proxyLink = '/apps/iwish';
    if (typeof Bizweb == 'object') {
        $.getJSON('https://' + window.Bizweb.store + '/meta.js', function (data) {
            window.ABWishListFormatMoney = data.money_format;
        });
    }
    function wishListSetCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";domain=" + window.location.hostname + ";" + expires;
    }

    function wishListGetCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    function wishListSubmit(url, submitObj, method) {
        if (method !== "post" && method !== "get") {
            method = "post";
        }
        var form = document.createElement("form");
        form.setAttribute("method", method);
        form.setAttribute("action", url);
        for (var item in submitObj) {
            if (submitObj.hasOwnProperty(item)) {
                var input = document.createElement("input");
                input.setAttribute("type", "hidden");
                input.setAttribute("name", item);
                input.setAttribute("value", submitObj[item]);
                form.appendChild(input)
            }
        }
        document.head.appendChild(form);
        form.submit()
    }

    function wishListLoadPage(page) {
        var postObj = {
            cust: iwish_cid,
            page: page,
            search: $('#wishlist_search_input').val()
        };
        $('#wishlist_loading_more').show();
        ABWishList.isLoading = true;
        $.post(proxyLink, postObj, function (data) {
            var htmlObject = document.createElement('div');
            htmlObject.innerHTML = data;
            var $wishList = $(htmlObject).find('.wishlist-view');
            var $loadMore = $(htmlObject).find('#wishlist_loadmore');

            if ($wishList.length > 0) {
                $wishList.find('.money').each(function () {
                    var $this = $(this);
                    if (typeof Bizweb == 'object') {
                        var price = $this.attr('data-price');
                        if (typeof price != 'undefined') {
                            $this.html(Bizweb.formatMoney(parseFloat(price), window.ABWishListFormatMoney));
                        }
                    }
                });

                $('.wishlist-view').append($wishList.html());
                $('#wishlist_loadmore').attr('data-wishlist-page', $loadMore.attr('data-wishlist-page'))
          .attr('data-wishlist-totalpage', $loadMore.attr('data-wishlist-totalpage'));
            }

            $('#wishlist_loading_more').hide();
            ABWishList.isLoading = false;
        }, 'html');
    }

    ABWishList.ChangeListStyle = function (toListStyle) {
        if (toListStyle) {
            $('.wishlist-list-style').addClass('active');
            $('.wishlist-grid-style').removeClass('active');
            $('.wishlist-view').removeClass('wishlist-grid-view').addClass('wishlist-list-view');
            wishListSetCookie('iwish_view_style', 'list', 365);
        }
        else {
            $('.wishlist-list-style').removeClass('active');
            $('.wishlist-grid-style').addClass('active');
            $('.wishlist-view').addClass('wishlist-grid-view').removeClass('wishlist-list-view');
            wishListSetCookie('iwish_view_style', 'grid', 365);
        }
    }

    ABWishList.LoadMore = function () {
        var $loadMore = $('.wishlist-load-more');
        if ($loadMore.length <= 0) return;
        var totalPage = parseInt($loadMore.attr('data-wishlist-totalpage'), 10);
        var page = parseInt($loadMore.attr('data-wishlist-page'), 10);
        if (page >= totalPage) {
            return;
        }
        wishListLoadPage(page + 1);
    }

    ABWishList.SubmitSearch = function () {
        $('.wishlist-view').empty();
        wishListLoadPage(1);
    }

    ABWishList.RemoveItem = function (vid) {
        var postObj = {
            actionx: 'remove',
            cust: iwish_cid,
            vid: vid
        };

        $.post(proxyLink, postObj, function (data) {
            var result = $(data).filter('#iwish_post_result').val();
            if (result === undefined) result = $(data).find('#iwish_post_result').val();
            if (result !== undefined) {
                var result1 = result.toString().toLowerCase() === 'true';
                if (result1) {
                    $('.wishlist-product[data-wishlist-varid=' + vid + ']').remove();
                    var page = $('.wishlist-load-more').attr('data-wishlist-page');
                    $('.wishlist-view-page[data-wishlist-page=' + page + ']').remove();
                    wishListLoadPage(page);
                }
            }
        });
    }

    $(document).ready(function () {
        var style = wishListGetCookie('iwish_view_style');
        ABWishList.ChangeListStyle(style !== 'grid');

        $(window).scroll(function () {
            var $results = $("#wishlist_loadmore");
            if ($results.length <= 0) return;
            if ($(window).scrollTop() + $(window).height() >= $results.offset().top && !ABWishList.isLoading) {
                ABWishList.LoadMore();
            }
        });

        $('#wishlist_search_input').keyup(debounce(ABWishList.SubmitSearch, 700));
    });
});