$(document).ready(function(){
  $('.muahang').on('click',function(){
    $('#popup-cart').modal('show');
    $('#quick-view-product').modal('hide');
    getUrl = $(location).attr('href');
    id = $(this).data('id');
    $.get('/them-gio-hang/'+id,function(data){
      // location.reload(true);
      $("#popup-cart-desktop" ).load( getUrl+" #popup-cart-desktop" ); //model cart
      $("#cart__" ).load( getUrl+" #cart__" ); //toolbar
      // $('#countCart').load( getUrl+" #countCart" ); //so luong trên menu
      // $('#itemsCart').load( getUrl+" #itemsCart" ); //load li san phẩm menu
      $('#load-div-to').load( getUrl+" #load-div-to" ); //load div to
      $('#background_id').load( getUrl+" #background_id" ); //so luong trong trang gio hang
      $('#allCart').load( getUrl+" #allCart" ); //all cart desktop 
      $('#cart-mb').load( getUrl+" #cart-mb" ); //cart mobile
    });
  });

  $('.delAllCart').on('click',function(){
    getUrl = $(location).attr('href');
    $.get('/xoa-all-gio-hang',function(data){
      // location.reload(true);
      $("#popup-cart-desktop" ).load( getUrl+" #popup-cart-desktop" ); //model cart
      $("#cart__" ).load( getUrl+" #cart__" ); //toolbar
      // $('#countCart').load( getUrl+" #countCart" ); //so luong trên menu
      // $('#itemsCart').load( getUrl+" #itemsCart" ); //load li san phẩm menu
      $('#load-div-to').load( getUrl+" #load-div-to" ); //load div to
      $('#background_id').load( getUrl+" #background_id" ); //so luong trong trang gio hang
      $('#allCart').load( getUrl+" #allCart" ); //all cart desktop 
      $('#cart-mb').load( getUrl+" #cart-mb" ); //cart mobile
    });
  });

  $('#tinhThanhChange').change(function(){
    var idTinhThanh = $(this).val();
    // alert(idTinhThanh);
    $.get('/where-quan-huyen/'+idTinhThanh,function(data){
      $('#quanHuyenChange').html(data);
    });
  });

  $('#quanHuyenChange').change(function(){
    var idQuanHuyen = $(this).val();
    // alert(idQuanHuyen);
    $.get('/where-xa-phuong/'+idQuanHuyen,function(data){
      $('#xaPhuongChange').html(data);
    });
  });

  $('.dangNhap').click(function(e){
    e.preventDefault();
    getUrl = $(location).attr('href');
    name = $('#name').val();
    password = $('#password').val();
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.post('/dang-nhap.html',{'name':name,'password':password},function(data){
      console.log(data);
      if (data == 'true') {
        window.location.href = '/';
      }else{
        alert('Tài khoản hoặc mật khẩu không đúng ');
      }
    });
  });

  $('.quick-view').on('click',function(){
    id = $(this).data('id');
    $('#quick-view-product').modal();
    $.get('/xem-nhanh/'+id,function(data){
      var data = JSON.parse(data);
      $('#head-qv').html("<h3 class='qwp-name'><a class='textxx' href='san-pham/"+data.Slug+"' title='"+data.Name+"'>"+data.Name+"</a></h3>");
      $('#view_full_size').html("<a class='img-product' title='"+data.Name+"' href='san-pham/"+data.Slug+"'><img id='product-featured-image-quickview' class='img-responsive product-featured-image-quickview' src='/uploads/sanpham/"+data.Image+"'></a>");
      $('#vendor').html(data.ThuongHieu);
      if (data.GiaSale) {
        $('#priceSp').html(data.GiaSale+'₫');
      }else{
        $('#priceSp').html(data.Gia+'₫');
      }
      $('#text3line').html(data.MoTa);
      
      $('.quantity_wanted_p').html("<a href='javascript:void(0)' style='margin-left: 10em;' onclick='muahang("+data.id+")' class='btn btn-primary button_cart_buy_enable' title='Mua hàng'><span>Thêm vào giỏ hàng</span></a>");
    });
  });

  $('.iWishAdd').click(function(){
    id = $(this).data('id');
    user = $(this).attr('ok');
    // alert(user); 
    $.get('/yeu-thich/'+id+'/'+user,function(data){
      if (data == 'false') {
        alert('Sản phẩm này đã có trong danh sách yêu thích của bạn !');
      }else{
        // alert('Thêm vào danh sách yêu thích thành công !');
        swal("Thêm vào danh sách yêu thích thành công !", "", "success");
      }
    });
  });

  $('.iWishAdda').click(function(){
    // swal('Vui lòng đăng nhập để thực hiện tác vụ này !!');
    swal("Vui lòng đăng nhập để thực hiện tác vụ này !!","", "error");
  });

  $('.changeKichThuoc').change(function(){
    id = $(this).data('id');
    size = $(this).val();
    $.get('/ajax-size/'+id+'/'+size,function(data){
      $('.changeMauSac').html(data);
    });
  });

  $('.changeMauSac').change(function(){
    id = $(this).data('id');
    mau = $(this).val();
    size = $('.changeKichThuoc').val();
    $.get('/ajax-mau/'+id+'/'+mau+'/'+size,function(data){
      $html = "<button class='btn_num num_1 button button_qty' onclick='var result = document.getElementById(\"qtym\"); var qtypro = result.value; if( !isNaN( qtypro ) &amp;&amp; qtypro > 1 ) result.value--;return false;' type='button'>-</button>";
      $html += "<input type='number' id='qtym' name='quantity' min='1' class='form-control prd_quantity' value='1'>";
      $html += "<button class='btn_num num_2 button button_qty' onclick='var result = document.getElementById(\"qtym\"); var qtypro = result.value; if( !isNaN( qtypro ) &amp;&amp; qtypro < "+data+") result.value++;return false;' type='button'>+</button>";           
      $('#ajaxQty').html($html);
    });
  });

});
function muahang(id){
  $('#popup-cart').modal('show');
  $('#quick-view-product').modal('hide');
  getUrl = $(location).attr('href');
  $.get('/them-gio-hang/'+id,function(data){
      // location.reload(true);
      $("#popup-cart-desktop" ).load( getUrl+" #popup-cart-desktop" ); //model cart
      $("#cart__" ).load( getUrl+" #cart__" ); //toolbar
      // $('#countCart').load( getUrl+" #countCart" ); //so luong trên menu
      // $('#itemsCart').load( getUrl+" #itemsCart" ); //load li san phẩm menu
      $('#load-div-to').load( getUrl+" #load-div-to" ); //load div to
      $('#background_id').load( getUrl+" #background_id" ); //so luong trong trang gio hang
      $('#allCart').load( getUrl+" #allCart" ); //all cart desktop 
      $('#cart-mb').load( getUrl+" #cart-mb" ); //cart mobile
    });
}
function xoaCart(id){
  var id = $('.xoaSanPham_'+id).attr('data-id');
  getUrl = $(location).attr('href');
  $.get('/xoa-gio-hang/'+id,{'rowId':id},function(data){
    $("#popup-cart-desktop" ).load( getUrl+" #popup-cart-desktop" );
    $("#cart__" ).load( getUrl+" #cart__" );
      // $('#countCart').load( getUrl+" #countCart" );
      // $('#itemsCart').load( getUrl+" #itemsCart" );
      $('#load-div-to').load( getUrl+" #load-div-to" );
      $('#background_id').load( getUrl+" #background_id" ); //giohang
      $('#allCart').load( getUrl+" #allCart" ); //all cart
      $('#cart-mb').load( getUrl+" #cart-mb" ); //cart mobile
    });
}  
function qtySp(id){
  sl = $('.qtyItem'+id).val();
  id = $('.qtyItem'+id).data('id');
  getUrl = $(location).attr('href');
  $.get('/cap-nhat-sl/'+id+'/'+sl,function(result){
    var result = JSON.parse(result);
    $('#'+id).html(result.sub_total);
    $('#total_cart_all').html(result.total);
    $("#popup-cart-desktop" ).load( getUrl+" #popup-cart-desktop" );
    $("#cart__" ).load( getUrl+" #cart__" );
    $('#load-div-to').load( getUrl+" #load-div-to" );
    $('#background_id').load( getUrl+" #background_id" ); //giohang
    $('#allCart').load( getUrl+" #allCart" ); //all cart
    $('#cart-mb').load( getUrl+" #cart-mb" ); //cart mobile
  });
}
function check(gia){
  $.get('/ajax-gia/'+gia,function(data){
    if(data.html != '' && data.html != undefined){
      $('#hien_sanpham').html(data.html);
      $('#links').hide();
    }else{
      $('#hien_sanpham').html('<p style="font-size: 21px;color:#759ba1;margin: 20px 0;text-align: center">Không có sản phẩm nào phù hợp</p>');
      $('#links').hide();
    }
  });
}