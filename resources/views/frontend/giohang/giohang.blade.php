@extends('frontend.layout.master')

@section('title','Tony4men - Giỏ Hàng')
@section('keywords','Tony4men - Hàng chất lượng đảm bảo, giá tốt, đẹp như ý')
@section('description','Tony4men - Hàng chất lượng đảm bảo, giá tốt, đẹp như ý')
@section('url',url('/gio-hang.html'))
@section('titleseo','Tony4men - Giỏ Hàng')
@section('type','cart')
@section('descriptionseo','Tony4men - Hàng chất lượng đảm bảo, giá tốt, đẹp như ý')
@section('image')

@section('content')
<section class="breadcrumb_background">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="wrap_breadcrumb a-center">
          <h1 class="title-head-page margin-top-0">Giỏ hàng của bạn</h1>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="bread-crumb">
  <span class="crumb-border"></span>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 a-left">
        <ul class="breadcrumb" itemscope="" itemtype="">
          <li class="home">
            <a itemprop="url" href="{{ url('/') }}"><span itemprop="title">Trang chủ</span></a>            
            <span class="mr_lr"> / </span>
          </li>
          <li><strong><span itemprop="title">Giỏ hàng</span></strong></li>
        </ul>
      </div>
    </div>
  </div>
</section>
<div class="container">
  <div class="row">
    <div class="col-lg-12">
      <div class="title_head margin-top-10 margin-bottom-10">
        <h2 class="title_center_page left ">
          <span class="background_" id="background_id" style="font-size: 18px;color: #545454;">Giỏ hàng của bạn có (<span class="cartCount  count_item_pr">{{ Cart::count() }}</span> sản phẩm) <i class="fa fa-caret-right"></i></span>
        </h2>
      </div>
    </div>
  </div>
</div>
<section class="main-cart-page main-container col1-layout">
  <div class="main container hidden-xs hidden-sm">
    <div class="col-main cart_desktop_page cart-page" id="allCart">
      <div class="cart page_cart hidden-xs">
        <form action="" method="post" novalidate="" class="margin-bottom-0">
          <div class="bg-scroll">
            <div class="cart-thead">
              <div style="width: 15%" class="a-center">Ảnh sản phẩm</div>
              <div style="width: 35%" class="a-center">Tên sản phẩm</div>
              <div style="width: 17%" class="a-center"><span class="nobr">Đơn giá</span></div>
              <div style="width: 14%" class="a-center">Số lượng</div>
              <div style="width: 14%" class="a-center">Thành tiền</div>
              <div style="width: 5%" class="a-center">Xoá</div>
            </div>
            <div class="cart-tbody">
              @if (Cart::count() > 0)
              @foreach ($cart as $items)
              <div class="item-cart">
                <div style="width: 15%" class="image">
                  <a class="product-image" title="{{ $items->name }}" href="{{ url('san-pham/'.$items->options['slug']) }}">
                    <img width="75" height="auto" alt="{{ $items->name }}" src="{{ asset('uploads/sanpham/'.$items->options['image']) }}">
                  </a>
                </div>
                <div style="width: 35%" class="a-center">
                  <h3 class="product-name text-center" style="margin-left: 0 !important"> 
                    <a class="text2line" href="{{ url('san-pham/'.$items->options['slug']) }}">{{ $items->name }}</a> 
                  </h3>
                  <p class="addpass" style="color:#fff;margin:0px;">
                    <span class="add_sus" style="color:#898989;">
                      <i style="margin-right:5px; color:#3cb878; font-size:14px;" class="fa fa-check"></i>
                      Mã Sp: {{ $items->options['masp'] }}
                    </span>
                  </p>
                  @if ($items->options['kichthuoc'] != null)
                    <p class="addpass" style="color:#fff;margin:5px 0 0;">
                    <span class="add_sus" style="color:#898989;">
                      Size : {{ $items->options['kichthuoc'] }} - Màu : {{ $items->options['mausac'] }}
                    </span>
                  </p>
                  @else
                    <p class="addpass" style="color:#fff;margin:5px 0 0;">
                    <span class="add_sus" style="color:#898989;">
                      Size - Màu lấy theo thông tin trên ảnh
                    </span>
                  </p>
                  @endif
                  
                </div>
                <div style="width: 17%" class="a-center">
                  <span class="item-price"> 
                    <span class="price">{{ number_format($items->price,0,',','.') }}₫</span>
                  </span>
                </div>
                <div style="width: 14%" class="a-center">
                  <div class="input_qty_pr">
                    <input type="number" min="1" width="100" class="qtyItemCss qtyItem{{ $items->id }}" id="data-id" data-id="{{ $items->rowId }}" onchange="qtySp({{ $items->id }})" value="{{ $items->qty }}">
                  </div>
                </div>
                <div style="width: 14%" class="a-center">
                  <span class="cart-price"> 
                    <span class="price" id="{{ $items->rowId }}">{!! number_format(($items->qty) * ($items->price),0,',','.') !!}₫</span> 
                  </span>
                </div>
                <div style="width: 5%" class="a-center">
                  <a class="remove-itemx remove-item-cart xoaSanPham_{{ $items->id }}" onclick="return xoaCart({{ $items->id }})" title="Xóa" href="javascript:;" id="data-id" data-id="{{ $items->rowId }}">
                    <span><i class="fa fa-times"></i></span>
                  </a>
                </div>
              </div>
              @endforeach
              @else
              <div style="width: 100%;padding-top: 1em;font-size: 16px;text-align: center;color: #8a8a8a;font-weight: bold;" class="a-center">Giỏ hàng của bạn hiện không có sản phẩm nào !!</div>
              @endif
            </div>
          </div>
        </form>
        <div class="cart-collaterals cart_submit row">
          <div class="totals col-sm-12 col-md-12 col-xs-12">
            <div class="totals">
              <div class="inner">
                <div class="col-lg-6 col-md-6 col-sm-6">
                  <button class="btn btn-white delAllCart" title="Tiếp tục mua hàng" type="button" style="float: left !important;padding: 0px 20px;">
                    <span>Xóa tất cả giỏ hàng</span>
                  </button>
                </div>
                <div class="fot_totals shopping-cart-table-total col-lg-6 col-md-6 col-sm-6">
                  <div class="total_price">
                    <span class="total_text"></span>
                    <span class="total_p">Tổng tiền thanh toán: 
                      <span class="totals_price price" id="total_cart_all">{{ $total }}₫</span>
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="checkout_button margin-bottom-50">
          <button class="btn btn-primary button btn-proceed-checkout f-right" title="Tiến hành đặt hàng" type="button" onclick="window.location.href='{{ url('thanh-toan.html') }}'">
            <span>Tiến hành đặt hàng</span>
          </button>
          <button class="btn btn-white f-right" title="Tiếp tục mua hàng" type="button" onclick="window.location.href='{{ url('/') }}'">
            <span>Tiếp tục mua hàng</span>
          </button>
        </div>
      </div>
    </div>
  </div>
  <div class="cart-mobile hidden-md hidden-lg container" id="cart-mb">
    <form action="" method="post" novalidate="" class="margin-bottom-0">
      <div class="header-cart-content" style="background:#fff;">
        @if (Cart::count() > 0)
        @foreach ($cart as $items)
        <div class="cart_page_mobile content-product-list">
          <div class="item-product item-mobile-cart item">
            <div class="item-product-cart-mobile">
              <a class="product-images1" href="{{ url('san-pham/'.$items->options['slug']) }}" title="{{ $items->name }}">
                <img width="80" height="150" alt="{{ $items->name }}" src="{{ asset('uploads/sanpham/'.$items->options['image']) }}">
              </a>
            </div>
            <div class="title-product-cart-mobile">
              <h3>
                <a class="text2line" href="{{ url('san-pham/'.$items->options['slug']) }}" title="{{ $items->name }}">{{ $items->name }}
                </a>
              </h3>
              <p>Giá: <span>{{ number_format($items->price,0,',','.') }}₫</span></p>
            </div>
            <div class="select-item-qty-mobile">
              <div class="txt_center in_put">
                <input type="number" min="1" class="qtyItem{{ $items->id }}" readonly="readonly" id="data-id" data-id="{{ $items->rowId }}" onchange="qtySp({{ $items->id }})" name="Lines" size="4" value="{{ $items->qty }}">
              </div>
              <a class="xoaSanPham_{{ $items->id }}" onclick="return xoaCart({{ $items->id }})" title="Xóa" href="javascript:;" id="data-id" data-id="{{ $items->rowId }}">Xoá
              </a>
            </div>
          </div>
        </div>
        @endforeach
        @else
        <div class="text-center"> Giỏ hàng trống !</div>
        @endif
        <div class="header-cart-price" style="">
          <div class="title-cart a-center">
            <span class="total_mobile a-center">Tổng tiền: 
              <span class=" totals_price_mobile" id="total_cart_all">{{ $total }}₫</span>
            </span>
          </div>
          <div class="checkout">
            <button class="btn-proceed-checkout-mobile" title="Tiến hành thanh toán" type="button" onclick="window.location.href='{{ url('thanh-toan.html') }}'">
              <span>Tiến hành thanh toán</span>
            </button>
            <button class="btn btn-white contin" title="Tiếp tục mua hàng" type="button" onclick="window.location.href='{{ url('/') }}'">
              <span>Tiếp tục mua hàng</span>
            </button>
          </div>
        </div>
      </div>
    </form>
  </div>
</section>
@endsection