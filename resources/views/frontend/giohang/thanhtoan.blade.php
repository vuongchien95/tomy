<!DOCTYPE html>
<html class="anyflexbox boxshadow display-table">
<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="keywords" content="Thời trang nam, Chuyên Quần Jeans Nam, Áo denim"/>
  <meta name="description" content="Tony4men - Thanh toán đơn hàng" />
  <title>Tony4men - Thanh toán đơn hàng</title>
  <meta property="og:title" content="Thời trang nam, Chuyên Quần Jeans Nam, Áo denim" />
  <meta property="og:type" content="checkout" />
  <meta property="og:description" content="Thời trang nam, Chuyên Quần Jeans Nam, Áo denim" />
  <link rel="icon" href="{{ asset('uploads/cauhinh/45044255_89786203_favicon.jpg') }}" type="image/x-icon" />
  <script src='{{ asset('frontend/js/jquery-2.1.3.min.js') }}' type='text/javascript'></script>
  <link href='{{ asset('frontend/css/bootstrap.min.css') }}' rel='stylesheet' type='text/css' />
  <link href='{{ asset('frontend/css/nprogress.css') }}' rel='stylesheet' type='text/css' />
  <link href='{{ asset('frontend/css/font-awesome.min.css') }}' rel='stylesheet' type='text/css' />
  <link href="{{ asset('frontend/css/checkout_dev_2.css') }}" rel="stylesheet" type="text/css">
  <link href='{{ asset('frontend/css/checkout.css') }}' rel='stylesheet' type='text/css' />
</head>
<body class="body--custom-background-color ">
  <div class="banner" data-header="">
    <div class="wrap">
      <div class="shop logo logo--left ">
        <h1 class="shop__name">
          <a href="{{ url('/') }}">
            Tony4men
          </a>
        </h1>
      </div>
    </div>
  </div>
  <button class="order-summary-toggle" bind-event-click="Bizweb.StoreCheckout.toggleOrderSummary(this)">
    <div class="wrap">
      <h2>
        <label class="control-label">Đơn hàng</label>
        <label class="control-label hidden-small-device">
          ({{ Cart::count() }} sản phẩm)
        </label>
      </h2>
      <a class="underline-none expandable pull-right" href="javascript:void(0)">
        Xem chi tiết
      </a>
    </div>
  </button>
  <form method="post" action="{{ route('thanh-toan') }}" data-toggle="validator" class="content stateful-form formCheckout">
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <div class="wrap" context="checkout">
      <div class='sidebar '>
        <div class="sidebar_header">
          <h2>
            <label class="control-label">Đơn hàng</label>
            <label class="control-label">({{ Cart::count() }} sản phẩm)</label>
          </h2>
          <hr class="full_width"/>
        </div>
        <div class="sidebar__content">
          <div class="order-summary order-summary--product-list order-summary--is-collapsed">
            <div class="summary-body summary-section summary-product" >
              <div class="summary-product-list">
                <table class="product-table">
                  <tbody>
                    @foreach ($cart as $items)
                    <tr class="product product-has-image clearfix">
                      <td>
                        <div class="product-thumbnail">
                          <div class="product-thumbnail__wrapper">
                            <img src='{{ asset('uploads/sanpham/'.$items->options['image']) }}' class='product-thumbnail__image' />
                          </div>
                          <span class="product-thumbnail__quantity" aria-hidden="true">{{ $items->qty }}</span>
                        </div>
                      </td>
                      <td class="product-info">
                        <span class="product-info-name">
                          {{ $items->name }}
                        </span>
                      </td>
                      <td class="product-price text-right"> {{ number_format($items->price,0,',','.') }}₫ </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
                <div class="order-summary__scroll-indicator">
                  Cuộn chuột để xem thêm
                  <i class="fa fa-long-arrow-down" aria-hidden="true"></i>
                </div>
              </div>
            </div>
            <hr class="m0"/>
          </div>
          <div class="order-summary order-summary--discount">
            <div class="summary-section">
              <div class="form-group m0">
                <div class="field__input-btn-wrapper">
                  <div class="field__input-wrapper">
                    <input name="code" type="text" class="form-control discount_code" placeholder="Nhập mã giảm giá" value="" id="checkout_reduction_code"/>
                  </div>
                  <button class="btn btn-primary event-voucher-apply" type="button">Áp dụng</button>
                </div>
              </div>
              <div class="error mt10 hide"> Mã khuyến mãi không hợp lệ </div>
            </div>
            <hr class="m0"/>
          </div>
          <div class="order-summary order-summary--total-lines">
            <div class="summary-section border-top-none--mobile">
              <div class="total-line total-line-subtotal clearfix">
                <span class="total-line-name pull-left"> Tạm tính </span>
                <span class="total-line-subprice pull-right"> {{ $total }}₫ </span>
              </div>
              <div class="total-line total-line-shipping clearfix">
                <span class="total-line-name pull-left"> Phí vận chuyển </span>
                <span class="total-line-shipping pull-right"> Miễn phí </span>
              </div>
              <div class="total-line total-line-total clearfix">
                <span class="total-line-name pull-left"> Tổng cộng </span>
                <span class="total-line-price pull-right"> {{ $total }}₫ </span>
              </div>
            </div>
          </div>
          <div class="form-group clearfix hidden-sm hidden-xs">
            <div class="field__input-btn-wrapper mt10">
              <a class="previous-link" href="{{ url('gio-hang.html') }}">
                <i class="fa fa-angle-left fa-lg" aria-hidden="true"></i>
                <span>Quay về giỏ hàng</span>
              </a>
              <input class="btn btn-primary btn-checkout" data-loading-text="Đang xử lý" type="submit" value="ĐẶT HÀNG" />
            </div>
          </div>
          <div class="form-group has-error">
            <div class="help-block ">
              <ul class="list-unstyled">
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="main" role="main">
        <div class="main_header">
          <div class="shop logo logo--left ">
            <h1 class="shop__name">
              <a href="{{ url('/') }}">
                Tony4men
              </a>
            </h1>
          </div>
        </div>
        <div class="main_content">
          <div class="row">
            @if (Auth::check())
            <div class="col-md-6 col-lg-6">
              <div class="section">
                <div class="section__header">
                  <div class="layout-flex layout-flex--wrap">
                    <h2 class="section__title layout-flex__item layout-flex__item--stretch">
                      <i class="fa fa-id-card-o fa-lg section__title--icon hidden-md hidden-lg" aria-hidden="true"></i>
                      <label class="control-label">Xin chào bạn, {{ Auth::user()->name }}</label>
                    </h2>
                  </div>
                </div>
                <div class="section__content">
                  <div class="form-group">
                    <div>  Chúng tôi sẽ lấy thông tin theo tài khoản của bạn đã đăng ký từ trước ! </div>
                  </div>
                </div>
                <div class="section">
                <div class="section__content">
                  <div class="form-group m0">
                    <textarea name="note" value="{{ old('note') }}" class="field__input form-control m0" placeholder="Ghi chú"></textarea>
                  </div>
                </div>
              </div>
              </div>
            </div>
            @else
            <div class="col-md-6 col-lg-6">
              <div class="section">
                <div class="section__header">
                  <div class="layout-flex layout-flex--wrap">
                    <h2 class="section__title layout-flex__item layout-flex__item--stretch">
                      <i class="fa fa-id-card-o fa-lg section__title--icon hidden-md hidden-lg" aria-hidden="true"></i>
                      <label class="control-label">Thông tin mua hàng</label>
                    </h2>
                    <a class="layout-flex__item section__title--link" href="{{ url('dang-nhap.html') }}">
                      <i class="fa fa-user-circle-o fa-lg" aria-hidden="true"></i>
                      Đăng nhập 
                    </a>
                  </div>
                </div>

                @if (count($errors) > 0)
                <div class="section__content clearfix">
                  <div class="form-group">
                    <div class="alert alert-danger col-md-12 text-center">
                      @foreach ($errors->all() as $err)
                      <span><i class="ace-icon fa fa-exclamation-triangle"></i> {{$err}} </span> <br/>
                      @endforeach
                    </div>
                  </div>
                </div>
                
                @endif

                <div class="section__content">
                  <div class="form-group">
                    <div>
                      <label class="field__input-wrapper">
                        <input name="email" type="email" class="ip field__input form-control" id="_email" data-error="Vui lòng nhập email đúng định dạng" placeholder="Email" required value="{{ old('email') }}"  pattern="^([a-zA-Z0-9_\-\.\+]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$" />
                      </label>
                    </div>
                    <div class="help-block with-errors">
                    </div>
                  </div>
                  <div class="billing">
                    <div>
                      <div class="form-group">
                        <div class="field__input-wrapper">
                          <input name="name" type="text" class="ip field__input form-control" id="" data-error="Vui lòng nhập họ tên" value="{{ old('name') }}" required placeholder="Họ và Tên" />
                        </div>
                        <div class="help-block with-errors"></div>
                      </div>
                      <div class="form-group">
                        <div class="field__input-wrapper">
                          <input name="phone" value="{{ old('phone') }}" class="ip field__input form-control" id="" placeholder="Số điện thoại" pattern="[0-9]{9,11}" required data-error="Vui lòng nhập số điện thoại"/>
                        </div>
                        <div class="help-block with-errors"></div>
                      </div>
                      
                      <div class="form-group">
                        <div class="field__input-wrapper field__input-wrapper--select">
                          <label class="field__label" for="tinhthanh">
                            Tỉnh thành
                          </label>
                          <select class="field__input field__input--select form-control" name="tinhthanh" id="tinhThanhChange" required data-error="Bạn chưa chọn tỉnh thành">
                            <option value=''>--- Chọn tỉnh thành ---</option>
                            @foreach ($tinhThanh as $items)
                            <option  value="{{ $items->id }}">{{ $items->Name }}</option>
                            @endforeach
                          </select>
                        </div>
                        <div class="help-block with-errors"></div>
                      </div>

                      <div class="form-group">
                        <div class="field__input-wrapper field__input-wrapper--select">
                          <label class="field__label" for="quanhuyen">
                            Quận huyện
                          </label>
                          <select class="field__input field__input--select form-control" name="quanhuyen" id="quanHuyenChange" required data-error="Bạn chưa chọn quận huyện">
                            <option value="">--- Chọn quận huyện ---</option>
                          </select>
                        </div>
                        <div class="help-block with-errors"></div>
                      </div>

                      <div class="form-group">
                        <div class="field__input-wrapper field__input-wrapper--select">
                          <label class="field__label" for="xaphuong">
                            Xã phường
                          </label>
                          <select class="field__input field__input--select form-control" name="xaphuong" id="xaPhuongChange" required data-error="Bạn chưa chọn xã phường">
                            <option value="">--- Chọn xã phường ---</option>
                          </select>
                        </div>
                        <div class="help-block with-errors"></div>
                      </div>

                      <div class="form-group">
                        <div class="field__input-wrapper">
                          <input name="address" value="{{ old('address') }}" type="text" class="ip field__input form-control" id="" placeholder="Địa chỉ" required data-error="Vui lòng nhập địa chỉ"/>
                        </div>
                        <div class="help-block with-errors"></div>
                      </div>

                      <div bind-show="!otherAddress" class="form-group">
                        <div class="error hide" bind-show="requiresShipping && loadedShippingMethods && shippingMethods.length == 0  && BillingProvinceId  ">
                          <label>Khu vực này không hỗ trợ vận chuyển</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="section">
                <div class="section__content">
                  <div class="form-group m0">
                    <textarea name="note" value="{{ old('note') }}" class="field__input form-control m0" placeholder="Ghi chú"></textarea>
                  </div>
                </div>
              </div>
            </div>
            @endif
            
            <div class="col-md-6 col-lg-6">
              <div class="section shipping-method">
                <div class="section__header">
                  <h2 class="section__title">
                    <i class="fa fa-truck fa-lg section__title--icon hidden-md hidden-lg" aria-hidden="true"></i>
                    <label class="control-label">Vận chuyển</label>
                  </h2>
                </div>
                <div class="section__content">
                  <div class="content-box">
                    <div class="content-box__row">
                      <div class="radio-wrapper">
                        <div class="radio__input">
                          <input class="input-radio" type="radio"  checked="checked"  value="" name="ShippingMethod" id="shipping_method_320231">
                        </div>
                        <label class="radio__label" for="shipping_method_320231">
                          <span class="radio__label__primary">Giao hàng tận nơi</span>
                          <span class="radio__label__accessory">
                            <span class="content-box__emphasis"> Miễn Phí </span>
                          </span>
                        </label> 
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="section payment-methods">
                <div class="section__header">
                  <h2 class="section__title">
                    <i class="fa fa-credit-card fa-lg section__title--icon hidden-md hidden-lg" aria-hidden="true"></i>
                    <label class="control-label">Thanh toán</label>
                  </h2>
                </div>
                <div class="section__content">
                  <div class="content-box">
                    <div class="content-box__row">
                      <div class="radio-wrapper">
                        <div class="radio__input">
                          <input class="input-radio" type="radio" value="" name="PaymentMethodId" id="" data-check-id="4" checked>
                        </div>
                        <label class="radio__label" for="">
                          <span class="radio__label__primary">Thanh toán tại nhà (COD)</span>
                          <span class="radio__label__accessory">
                            <ul>
                              <li class="payment-icon-v2 payment-icon--4">
                                <i class="fa fa-money payment-icon-fa" aria-hidden="true"></i>
                              </li>
                            </ul>
                          </span>
                        </label>
                      </div>
                    </div>
                    <div class="radio-wrapper content-box__row content-box__row--secondary" id="" >
                      <div class="blank-slate"><p>Giao hàng trong vòng 2 - 4 ngày</p></div>
                    </div>
                  </div>
                  <div class="content-box">
                    <div class="content-box__row">
                      <div class="radio-wrapper">
                        <div class="radio__input">
                          <input style="cursor: not-allowed;" class="input-radio" type="radio" value="" name="PaymentMethodId" id="" data-check-id="4" disabled="">
                        </div>
                        <label class="radio__label" for="">
                          <span class="radio__label__primary">Bằng ví điện tử bảo kim (Ngân lượng)</span>
                          <span class="radio__label__accessory">
                            <ul>
                              <li class="payment-icon-v2 payment-icon--4">
                                <i class="fa fa-money payment-icon-fa" aria-hidden="true"></i>
                              </li>
                            </ul>
                          </span>
                        </label>
                      </div>
                    </div>
                    <div class="radio-wrapper content-box__row content-box__row--secondary" id="" >
                      <div class="blank-slate"><p>An toàn - nhanh chóng - bảo mật</p></div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="section hidden-md hidden-lg">
                <div class="form-group clearfix m0">
                  <input class="btn btn-primary btn-checkout" type="submit" value="ĐẶT HÀNG" />
                </div>
                <div class="text-center mt20">
                  <a class="previous-link" href="{{ url('gio-hang.html') }}">
                    <i class="fa fa-angle-left fa-lg" aria-hidden="true"></i>
                    <span>Quay về giỏ hàng</span>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="main_footer footer unprint">
          <div class="mt10"></div>
        </div>
      </div>
    </div>
  </form>
  <script type="text/javascript">
    $(document).ready(function(){
      $('#tinhThanhChange').change(function(){
        var idTinhThanh = $(this).val();
        // alert(idTinhThanh);
        $.get('/where-quan-huyen/'+idTinhThanh,function(data){
          $('#quanHuyenChange').html(data);
        });
      });

      $('#quanHuyenChange').change(function(){
        var idQuanHuyen = $(this).val();
        // alert(idQuanHuyen);
        $.get('/where-xa-phuong/'+idQuanHuyen,function(data){
          $('#xaPhuongChange').html(data);
        });
      });
    });
  </script>
  <script src='{{ asset('frontend/js/jquery-2.1.3.min.js') }}' type='text/javascript'></script>
  <script src='{{ asset('frontend/js/bootstrap.min.js') }}' type='text/javascript'></script>
  <script src='{{ asset('frontend/js/twine.min.js') }}' type='text/javascript'></script>
  <script src='{{ asset('frontend/js/validator.min.js') }}' type='text/javascript'></script>
  <script src='{{ asset('frontend/js/nprogress.js') }}' type='text/javascript'></script>
  <script src='{{ asset('frontend/js/money-helper.js') }}' type='text/javascript'></script>
  <script src="{{ asset('frontend/js/ua-parser.pack.js') }}" type='text/javascript'></script>
  <script src="{{ asset('frontend/js/checkout_dev_2.js') }}" type='text/javascript'></script>
  <script type="text/javascript">
    $(document).ajaxStart(function () {
      NProgress.start();
    });
    $(document).ajaxComplete(function () {
      NProgress.done();
    });
    context = {}
    $(function () {
      Twine.reset(context).bind().refresh();
    });
    $(document).ready(function () {
      setTimeout(function(){
      }, 50);
    });
  </script>
</body>
</html>