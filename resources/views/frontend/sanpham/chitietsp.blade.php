@extends('frontend.layout.master')

@section('title','Tony4men - '.$sanpham->Name)
@section('keywords',$sanpham->TuKhoaSeo)
@section('description',$sanpham->MoTaSeo)
@section('url',url('/san-pham/'.$sanpham->Slug))
@section('titleseo','Tony4men - '.$sanpham->TieuDeSeo)
@section('type','product')
@section('descriptionseo',$sanpham->MoTaSeo)
@section('image')

@section('content')
<script src='{{ asset('frontend/js/jquery.cookie.min.js') }}' type='text/javascript'></script>
<style type="text/css">
  #qtym::-webkit-inner-spin-button, 
  #qtym::-webkit-outer-spin-button { 
    -webkit-appearance: none; 
    margin: 0; 
  }
</style>
<section class="breadcrumb_background">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="wrap_breadcrumb a-center">
          <p class="title-head-page margin-top-0">{!! $sanpham->Name !!}</p>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="bread-crumb">
  <span class="crumb-border"></span>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 a-left">
        <ul class="breadcrumb" itemscope="" itemtype="">
          <li class="home">
            <a itemprop="url" href="{{ url('/') }}"><span itemprop="title">Trang chủ</span></a>            
            <span class="mr_lr"> / </span>
          </li>
          <li><strong><span itemprop="title">{!! $sanpham->Name !!}</span></strong></li>
          <li>
          </li>
        </ul>
      </div>
    </div>
  </div>
</section>
<section class="product margin-top-20" itemscope="" itemtype="">
  <meta itemprop="url" content="{{ url('san-pham/'.$sanpham->Slug) }}">
  <meta itemprop="image" content="{{ asset('uploads/sanpham/'.$sanpham->Image) }}">
  <div class="container">
    <div class="row">
      <div class="details-product">
        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
          <div class="rows">
            <div class="product-detail-left product-images col-xs-12 col-sm-6 col-md-6 col-lg-6">
              <div class="row">
                <div class="col_large_default large-image">
                  <a href="{{ asset('uploads/sanpham/'.$sanpham->Image) }}" class="large_image_url checkurl" data-rel="prettyPhoto[product-gallery]">
                    <div style="height:400px;width:400px;" class="zoomWrapper">
                      <div style="height:400px;width:400px;" class="zoomWrapper">
                        <img id="img_01" class="img-responsive" title="{!! $sanpham->Name !!}" alt="{!! $sanpham->Name !!}" src="{{ asset('uploads/sanpham/'.$sanpham->Image) }}" data-zoom-image="{{ asset('uploads/sanpham/'.$sanpham->Image) }}" style="position: absolute;">
                      </div>
                    </div>
                  </a>
                  <div class="hidden">
                    @if ($sanphamImg)
                    @foreach ($sanphamImg as $items)
                    <div class="item">
                      <a href="{{ asset('uploads/images/sanphamimg/'.$items->Image) }}" data-image="{{ asset('uploads/images/sanphamimg/'.$items->Image) }}" data-zoom-image="{{ asset('uploads/images/sanphamimg/'.$items->Image) }}" data-rel="prettyPhoto[product-gallery]"></a>
                    </div>
                    @endforeach
                    @endif
                  </div>
                </div>
                <div id="gallery_02" class="col-sm-12 col-xs-12 col-lg-5 col-md-5 owl_width no-padding owl-carousel owl-theme thumbnail-product thumb_product_details not-dqowl" data-loop="false" data-md-items="4" data-sm-items="4" data-xs-items="4" data-margin="10">
                  @if ($sanphamImg)
                  @foreach ($sanphamImg as $items)
                  <div class="item">
                    <a href="#" data-image="{{ asset('uploads/images/sanphamimg/'.$items->Image) }}" data-zoom-image="{{ asset('uploads/images/sanphamimg/'.$items->Image) }}">
                      <img data-img="{{ asset('uploads/images/sanphamimg/'.$items->Image) }}" src="{{ asset('uploads/images/sanphamimg/'.$items->Image) }}" alt="{!! $sanpham->Name !!}" title="{!! $sanpham->Name !!}">
                    </a>
                  </div>
                  @endforeach
                  @endif
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 details-pro">
              <h1 class="title-product" itemprop="name">{!! $sanpham->Name !!}</h1>
              <div class="group-status">
                <span class="first_status">Thương hiệu: <span class="status_name">{!! $sanpham->ThuongHieu !!}</span></span>
                <span class="first_status"> &nbsp;|&nbsp; Tình trạng: 
                  @if ($sanpham->TrangThai == 1)
                  <span class="status_name availabel">Còn hàng</span></span>
                  @else
                  <span class="status_name availabel">Hết hàng</span></span>
                  @endif
                </div>
                <div class="price-box" itemscope="" itemtype="">
                  <span class="special-price">
                    <span class="price product-price" itemprop="price">{!! isset($sanpham->GiaSale) ? number_format($sanpham->GiaSale,0,',','.') : number_format($sanpham->Gia,0,',','.') !!}₫</span> 
                    <meta itemprop="priceCurrency" content="VND">
                  </span>
                  <!-- Giá Khuyến mại -->
                  <span class="old-price">
                    <del class="price product-price-old" itemprop="priceSpecification" style="display: none;">
                    </del> 
                    <meta itemprop="priceCurrency" content="VND">
                  </span>
                  <!-- Giá gốc -->
                </div>
                <div class="taxable hidden">
                  (<span class="vat">Giá chưa bao gồm VAT</span><span class="status_name availabel"></span>)
                </div>
                <div class="product-summary product_description">
                  <div class="rte description text3line">
                    {!! str_limit($sanpham->MoTa, 180) !!}
                  </div>
                </div>
                <div class="form-product col-sm-12">
                  <form action="{{ url('them-gio-hang/'.$sanpham->id) }}" enctype="multipart/form-data" id="" method="get" class="form-inline">
                    <div class="box-variant clearfix">
                      <fieldset class="form-group">
                        @if (isset($spChiTiet) && count($spChiTiet) > 0)
                        <div class="selector-wrapper" style="">
                          <label for="kichthuoc">Kích thước</label>
                          <select class="single-option-selector changeKichThuoc" id="data-id" data-id="{{ $sanpham->id }}" name="kichthuoc" required="">
                            <option value="">Chọn kích thước</option>
                            @foreach ($spChiTiet as $key => $items)
                            <option value="{{ $items['size'] }}">{{ $items['size'] }}</option>
                            @endforeach
                          </select>
                        </div>
                        <div class="selector-wrapper" style="">
                          <label for="mausac">Màu sắc</label>
                          <select class="single-option-selector changeMauSac" id="data-id" data-id="{{ $sanpham->id }}" name="mausac" required="">
                            <option value="">Chọn màu sắc</option>
                          </select>
                        </div>
                        @endif
                      </fieldset>
                    </div>
                    <div class="form-group form_button_details">
                      <header class="not_bg">Số lượng:</header>
                      <div class="custom input_number_product custom-btn-number form-control" id="ajaxQty"> 

                        <button class="btn_num num_1 button button_qty" onclick="var result = document.getElementById('qtym'); var qtypro = result.value; if( !isNaN( qtypro ) &amp;&amp; qtypro > 1 ) result.value--;return false;" type="button">-</button>
                        <input type="number" id="qtym" name="quantity" min="1" class="form-control prd_quantity" value="1">
                        <button class="btn_num num_2 button button_qty" onclick="var result = document.getElementById('qtym'); var qtypro = result.value; if( !isNaN( qtypro ) &amp;&amp; qtypro < 15) result.value++;return false;" type="button">+</button>
                      </div>

                      <button type="submit" class="btn btn-lg  btn-cart button_cart_buy_enable" title="Cho vào giỏ hàng">Đặt mua</button>                 
                      <div class="iwi">
                        @if (Auth::check())
                        <a title="Yêu thích" id="data-id" data-id="{{ $sanpham->id }}" ok="{{ Auth::user()->id }}" class="button_wh_40 iWishAdd iwishAddWrapper" href="javascript:;" >
                          <i class="fa fa-heart-o"></i>
                        </a>
                        @else
                        <a title="Yêu thích" class="button_wh_40 iWishAdda iwishAddWrapper" href="javascript:;" ><i class="fa fa-heart-o"></i>
                        </a>
                        @endif
                      </div>
                    </div>
                  </form>
                </div>
                <div class="phone_details">
                  <div class="fb-like" data-href="{{ url('/san-pham/'.$sanpham->Slug) }}" data-layout="standard" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
                  <span class="phone">
                    Gọi ngay <a href="callto:{!! $lienhe->Sdt !!}">{!! $lienhe->Sdt !!}</a> 
                    để đặt và giao hàng 
                  </span>
                </div>
              </div>
            </div>
          </div>
          <!-- Sidebar  -->
          <div class="col-lg-3 col-md-3 col-12 col-xs-12 hidden-sm hidden-xs">
            <div class="right_module">
              <div class="module_service_details">
                <div class="wrap_module_service">
                  @foreach ($chinhsach as $items)
                  <div class="item_service">
                    <div class="wrap_item_">
                      <div class="content_service">
                        <div class="img_">
                          <img src="{{ asset('uploads/chinhsach/'.$items->Image) }}" title="{!! $items->TenChinhSach !!}">
                        </div>
                        <div class="cont">{!! $items->TenChinhSach !!}</div>
                        <span style="font-size: 10px;">{!! $items->MoTa !!}</span>
                      </div>
                    </div>
                  </div>
                  @endforeach
                </div>
              </div>
            </div>
          </div>
          <!-- Endsidebar -->
          <!-- Tab -->
          <div class="col-xs-12 col-lg-12 col-sm-12 col-md-12">
            <div class="row margin-top-30 xs-margin-top-15">
              <div class="col-xs-12 col-lg-12 col-sm-12 col-md-12">
                <!-- Nav tabs -->
                <div class="product-tab e-tabs">
                  <ul class="tabs tabs-title clearfix">
                    <li class="tab-link current" data-tab="tab-1">
                      <h3><span>Thông tin sản phẩm</span></h3>
                    </li>
                    <li class="tab-link" data-tab="tab-2">
                      <h3><span>Bình Luận </span></h3>
                    </li>
                    <li class="tab-link" data-tab="tab-3">
                      <h3><span>Đánh giá sản phẩm</span></h3>
                    </li>
                  </ul>
                  <div id="tab-1" class="tab-content current">
                    <div class="rte">
                      <p> Các nội dung Hướng dẫn mua hàng viết ở đây  </p>
                    </div>
                  </div>
                  <div id="tab-2" class="tab-content">
                    <div class="rte">
                      <div class="fb-comments" data-href="{{ url('/san-pham/'.$sanpham->Slug) }}" data-width="1126" data-numposts="5"></div>
                    </div>
                  </div>
                  <div id="tab-3" class="tab-content">
                    <div class="rte">
                      <div id="bizweb-product-reviews" class="bizweb-product-reviews" data-id="8988803">
                        <div>
                          <div id="bizweb-product-reviews-sub" style="text-align: center;">
                            <span class="">
                              <input type="button" onclick="BPR.toggleForm(this);return false;" id="btnnewreview" value="Viết đánh giá" style="width: 30%">
                            </span>
                            <div class="bizweb-product-reviews-form" id="bpr-form_8988803" style="display:none;">
                              <form method="" action="" id="" name="">
                                <h4>Đánh giá</h4>
                                <fieldset class="bpr-form-contact">
                                  <div class="bpr-form-contact-name require">
                                    <input type="text" maxlength="128" id="review_author" name="author" value="" placeholder="Nhập tên của bạn" required="required">
                                    <span class="bpr-form-message-error"></span>
                                  </div>
                                  <div class="bpr-form-contact-email require">
                                    <input type="text" maxlength="128" id="review_email" name="email" value="" placeholder="nguyenvan@gmail.com" required="required">
                                    <span class="bpr-form-message-error"></span>
                                  </div>
                                </fieldset>
                                <fieldset class="bpr-form-review">
                                  <div class="bpr-form-review-title">
                                    <input type="text" maxlength="512" id="review_title" name="title" value="" placeholder="Tiêu đề" required="required">
                                    <span class="bpr-form-message-error"></span>
                                  </div>
                                  <div class="bpr-form-review-body">
                                    <textarea maxlength="1500" id="review_body" name="body" rows="5" placeholder="Nội dung" required="required"></textarea>
                                    <span class="bpr-form-message-error"></span>
                                  </div>
                                </fieldset>
                                <fieldset class="bpr-form-review-error">
                                  <p class="error"></p>
                                </fieldset>
                                <fieldset class="bpr-form-actions">
                                  <input type="button" onclick="ok()" value="Gửi" class="bpr-button-submit">
                                </fieldset>
                              </form>
                            </div>
                            <script type="text/javascript">
                              function ok(){
                                if($('#review_author').val() == "" || $('#review_email').val() == "" || $('#review_title').val() == "" || $('#review_body').val() == ""){
                                  alert('Bạn vui lòng nhập đầy đủ thông tin');
                                }else{
                                  alert('Cám ơn bạn đã đánh giá sản phẩm!');
                                  $('.fancybox-overlay').hide();
                                }
                              }
                            </script>
                            <div style="display:none;" id="bpr-thanks" class="bizweb-product-reviews-form">
                              <b>Cám ơn bạn đã đánh giá sản phẩm!</b>
                            </div>
                            <div id="bpr-list" class="bizweb-product-reviews-list"></div>
                            <div id="bpr-more-reviews"></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- Endtab -->
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 margin-bottom-50">
            <div class="section_popular_product  not_bf_af margin-top-10 ">
              <div class="title_section_center border_bottom_ not_bf after_and_before">
                <h2 class="title cooktail_">
                  <a class="title a-position" href="javascript:void(0)" title="Sản Phẩm Cùng Loại"> Sản Phẩm Cùng Loại</a>
                </h2>
              </div>
              <div class="border_wrap col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="owl_product_comback recent_product">
                  <div class="product_comeback_wrap">
                    <div class="owl_product_item_content owl-carousel" data-dot="false" data-nav="true" data-lg-items="4" data-md-items="3" data-sm-items="3" data-xs-items="1" data-margin="0">
                      @foreach ($sanphamlq as $items)
                      <div class="owl-item active" style="width: 345px;">
                        <div class="item saler_item col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <div class="owl_item_product padding_style product-col">
                            <div class="product-box">
                              <div class="product-thumbnail">
                                <a href="{{ url('san-pham/'.$items->Slug) }}" class="image_link display_flex" data-images="{{ asset('uploads/sanpham/'.$items->Image) }}" title="{{ $items->Name }}">
                                  <img src="{{ asset('uploads/sanpham/'.$items->Image) }}" alt="{{ $items->Name }}">
                                </a>
                                <div class="product-action-grid clearfix">
                                  <form action="" method="post" class="variants form-nut-grid" enctype="multipart/form-data">
                                    <div>
                                      <a title="xem nhanh" href="javascript:;" id="data-id" data-id="{{ $items->id }}" class="button_wh_40 btn_view right-to quick-view">
                                        <i class="fa fa-search"></i>
                                      </a>
                                      <a href="javascript:;" id="data-id" data-id="{{ $items->id }}" class="button_wh_40 btn-cart left-to muahang" title="Mua hàng"><span>Mua hàng</span>
                                      </a>
                                      @if (Auth::check())
                                      <a title="Yêu thích" id="data-id" data-id="{{ $items->id }}" ok="{{ Auth::user()->id }}" class="button_wh_40 iWishAdd iwishAddWrapper" href="javascript:;" >
                                        <i class="fa fa-heart-o"></i>
                                      </a>
                                      @else
                                      <a title="Yêu thích" class="button_wh_40 iWishAdda iwishAddWrapper" href="javascript:;" ><i class="fa fa-heart-o"></i>
                                      </a>
                                      @endif
                                    </div>
                                  </form>
                                </div>
                              </div>
                              <div class="product-info a-center">
                                <h3 class="product-name"><a class="text2line" href="{{ url('san-pham/'.$items->Slug) }}" title="{{ $items->Name }}">{{ $items->Name }}</a></h3>
                                @if ($items->GiaSale > 0)
                                <div class="price-box clearfix">      
                                  <span class="price product-price">{{ number_format($items->Gia,0,',','.') }}₫</span>
                                  <span class="price product-price-old">
                                    {{ number_format($items->GiaSale,0,',','.') }}₫     
                                  </span>
                                </div>
                                @else
                                <span class="price product-price">{{ number_format($items->Gia,0,',','.') }}₫</span>
                                @endif
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      @endforeach
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
@section('script')
<script>  
  var ww = $(window).width();
  function validate(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode( key );
    var regex = /[0-9]|\./;
    if( !regex.test(key) ) {
      theEvent.returnValue = false;
      if(theEvent.preventDefault) theEvent.preventDefault();
    }
  }
  var selectCallback = function(variant, selector) {
    if (variant) {

      var form = jQuery('#' + selector.domIdPrefix).closest('form');

      for (var i=0,length=variant.options.length; i<length; i++) {

        var radioButton = form.find('.swatch[data-option-index="' + i + '"] :radio[value="' + variant.options[i] +'"]');
        if (radioButton.size()) {
          radioButton.get(0).checked = true;
        }
      }
    }
    //var addToCart = jQuery('.form-product .btn-cart'),
    form = jQuery('.form-product .form_button_details'),
    productPrice = jQuery('.details-pro .special-price .product-price'),
    qty = jQuery('.availabel'),
    sale = jQuery('.details-pro .old-price .product-price-old'),
    comparePrice = jQuery('.details-pro .old-price .product-price-old');

    /*** VAT ***/
    if (variant){
      if (variant.taxable){
        $('.taxable .vat').text('Giá đã bao gồm VAT');
      } else {
        $('.taxable .vat').text('Giá chưa bao gồm VAT');
      }
    }

    if (variant && variant.available) {
      if(variant.inventory_management == "bizweb"){
        if (variant.inventory_quantity != 0) {
          qty.html('Còn hàng');
        } else if (variant.inventory_quantity == ''){
          qty.html('Hết hàng');
        }
      } else {
        qty.html('Còn hàng');
      }
      addToCart.text('Đặt mua').removeAttr('disabled'); 
      addToCart.removeClass('hidden');
      if(variant.price == 0){
        productPrice.html('Liên hệ'); 
        comparePrice.hide();
        form.addClass('hidden');
        sale.removeClass('sale');
        if(variant.inventory_management == "bizweb"){
          if (variant.inventory_quantity != 0) {
            qty.html('Còn hàng');
          } else if (variant.inventory_quantity == ''){
            qty.html('Hết hàng');
          }
        } else {
          qty.html('Còn hàng');
        }
      }else{
        form.removeClass('hidden');
        productPrice.html(Bizweb.formatMoney(variant.price, "₫"));
        // Also update and show the product's compare price if necessary
        if ( variant.compare_at_price > variant.price ) {
          comparePrice.html(Bizweb.formatMoney(variant.compare_at_price, "₫")).show();
          sale.addClass('sale');
          if(variant.inventory_management == "bizweb"){
            if (variant.inventory_quantity != 0) {
              qty.html('Còn hàng');
            } else if (variant.inventory_quantity == ''){
              qty.html('Hết hàng');
            }
          } else {
            qty.html('Còn hàng');
          }
        } else {
          comparePrice.hide();  
          sale.removeClass('sale');
          if(variant.inventory_management == "bizweb"){
            if (variant.inventory_quantity != 0) {
              qty.html('Còn hàng');
            } else if (variant.inventory_quantity == ''){
              qty.html('Hết hàng');
            }
          } else {
            qty.html('Còn hàng');
          }
        }                           
      }

    } else {  
      qty.html('Hết hàng');
      addToCart.text('Hết hàng').attr('disabled', 'disabled');
      form.removeClass('hidden');
      if(variant){
        if(variant.price != 0){
          form.removeClass('hidden');
          addToCart.addClass('hidden');
          productPrice.html(Bizweb.formatMoney(variant.price, "₫"));
          // Also update and show the product's compare price if necessary
          if ( variant.compare_at_price > variant.price ) {
            form.addClass('hidden');
            comparePrice.html(Bizweb.formatMoney(variant.compare_at_price, "₫")).show();
            sale.addClass('sale');
            if(variant.inventory_management == "bizweb"){
              if (variant.inventory_quantity != 0) {
                qty.html('Còn hàng');
              } else if (variant.inventory_quantity == ''){
                qty.html('Hết hàng');
                form.removeClass('hidden');
                addToCart.removeClass('hidden');
              }
            } else {
              qty.html('Còn hàng');
            }
          } else {
            comparePrice.hide();   
            sale.removeClass('sale');
            if(variant.inventory_management == "bizweb"){
              if (variant.inventory_quantity != 0) {
                qty.html('Còn hàng');
              } else if (variant.inventory_quantity == ''){
                qty.html('Hết hàng');
                form.removeClass('hidden');
                addToCart.removeClass('hidden');
              }
            } else {
              qty.html('Còn hàng');
            }
          }     
        }else{
          productPrice.html('Liên hệ'); 
          comparePrice.hide();
          form.addClass('hidden');  
          sale.removeClass('sale');
          addToCart.addClass('hidden');
        }
      }else{
        productPrice.html('Liên hệ'); 
        comparePrice.hide();
        form.addClass('hidden');  
        sale.removeClass('sale');
        addToCart.addClass('hidden');
      }

    }
    /*begin variant image*/
    if (variant && variant.image) {  
      var originalImage = jQuery(".large-image img"); 
      var newImage = variant.image;
      var element = originalImage[0];
      Bizweb.Image.switchImage(newImage, element, function (newImageSizedSrc, newImage, element) {
        jQuery(element).parents('a').attr('href', newImageSizedSrc);
        jQuery(element).attr('src', newImageSizedSrc);
        if (ww >= 1200){

          $("#img_01").data('zoom-image', newImageSizedSrc).elevateZoom({
            responsive: true,
            gallery:'gallery_02',
            cursor: 'pointer',
            galleryActiveClass: "active"
          });
          $("#img_01").bind("click", function(e) {
            var ez = $('#img_02').data('elevateZoom');
          });
          
        }
      });
      
      setTimeout(function(){
        $('.checkurl').attr('href',$(this).attr('src'));
        if (ww >= 1200){

          $('.zoomContainer').remove();
          $("#img_01").elevateZoom({
            responsive: true,
            gallery:'gallery_02',
            cursor: 'pointer',
            galleryActiveClass: "active"
          });
          
        }
      },200);

    } 
  };
  jQuery(function($) {
     // Add label if only one product option and it isn't 'Title'. Could be 'Size'.
      // Hide selectors if we only have 1 variant and its title contains 'Default'.
      $('.selector-wrapper').hide();
      $('.selector-wrapper').css({
        'text-align':'left',
        'margin-bottom':'15px',
        'display':'block'
      });
    });
  
  jQuery('.swatch :radio').change(function() {
    var optionIndex = jQuery(this).closest('.swatch').attr('data-option-index');
    var optionValue = jQuery(this).val();
    jQuery(this)
    .closest('form')
    .find('.single-option-selector')
    .eq(optionIndex)
    .val(optionValue)
    .trigger('change');
  });
  if (ww >= 1200){

    $(document).ready(function() {
      if($(window).width()>1200){
        $('#img_01').elevateZoom({
          gallery:'gallery_02', 
          zoomWindowWidth:420,
          zoomWindowHeight:500,
          zoomWindowOffetx: 10,
          easing : true,
          scrollZoom : true,
          cursor: 'pointer', 
          galleryActiveClass: 'active', 
          imageCrossfade: true
        });
      }
    });
    
  }
  $("#img_02").click(function(e){
    e.preventDefault();
    var hr = $(this).attr('src');
    $('#img_01').attr('src',hr);
    $('.large_image_url').attr('href',hr);
    $('#img_01').attr('data-zoom-image',hr);
  });
  
  
  function scrollToxx() {
    $('html, body').animate({ scrollTop: $('.product-tab.e-tabs').offset().top }, 'slow');
    $('.product-tab .tab-link').removeClass('current');
    $('.product-tab .tab-link[data-tab=tab-3]').addClass('current');
    $('.product-tab .tab-content').removeClass('current');
    $('.product-tab .tab-content#tab-3').addClass('current');

    return false;
  }
  /*For recent product*/
  var alias = 'nuoc-chanh-leo';
  /*end*/
  if (ww >= 1200){

    $(document).ready(function() {
      $('#img_01').elevateZoom({
        gallery:'gallery_02', 
        zoomWindowWidth:420,
        zoomWindowHeight:500,
        zoomWindowOffetx: 10,
        easing : true,
        scrollZoom : true,
        cursor: 'pointer', 
        galleryActiveClass: 'active', 
        imageCrossfade: true

      });
    });
    
  }
  $('#gallery_00 img, .swatch-element label').click(function(e){

    $('.checkurl').attr('href',$(this).attr('src'));
    if (ww >= 1200){

      setTimeout(function(){
        $('.zoomContainer').remove();       
        $('#zoom_01').elevateZoom({
          gallery:'gallery_02', 
          zoomWindowWidth:420,
          zoomWindowHeight:500,
          zoomWindowOffetx: 10,
          easing : true,
          scrollZoom : true,
          cursor: 'pointer', 
          galleryActiveClass: 'active', 
          imageCrossfade: true
        });
      },300);
      
    }
  });
</script>
<script>
  $(document).ready(function (e) {
    $("#gallery_02").owlCarousel({
      navigation : true,
      nav: true,
      navigationPage: false,
      navigationText : false,
      slideSpeed : 1000,
      pagination : true,
      dots: false,
      margin: 5,
      autoHeight:true,
      autoplay:false,
      autoplayTimeout:false,
      autoplayHoverPause:true,
      loop: false,
      responsive: {
        0: {
          items: 3
        },
        543: {
          items: 4
        },
        768: {
          items: 4
        },
        991: {
          items: 4
        },
        992: {
          items: 3
        },
        1200: {
          items: 3
        }
      }
    });
    $('#gallery_02 img, .swatch-element label').click(function(e){
      e.preventDefault();
      var ths = $(this).attr('data-img');
      $('.large-image .checkurl').attr('href', ths);

      $('.large-image .checkurl img').attr('src', ths);

      /*** xử lý active thumb -- ko variant ***/
      var thumbLargeimg = $('.details-product .large-image a').attr('href').split('?')[0];
      var thumMedium = $('#gallery_02 .owl-item .item a').find('img').attr('src');
      var url = [];

      $('#gallery_02 .owl-item .item').each(function(){
        var srcImg = '';
        $(this).find('a img').each(function(){
          var current = $(this);
          if(current.children().size() > 0) {return true;}
          srcImg += $(this).attr('src');
        });
        url.push(srcImg);
        var srcimage = $(this).find('a img').attr('src').split('?')[0];
        if (srcimage == thumbLargeimg) {
          $(this).find('a').addClass('active');
        } else {
          $(this).find('a').removeClass('active');
        }
      });
    });
  });
</script>
@endsection