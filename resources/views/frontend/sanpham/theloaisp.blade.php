@extends('frontend.layout.master')

@section('title','Tony4men - '.$danhmuc->Name)
@section('keywords',$danhmuc->Name)
@section('description','Tony4men - Hàng chất lượng đảm bảo, giá tốt, đẹp như ý')
@section('url',url($danhmuc->Slug))
@section('titleseo','Tony4men - '.$danhmuc->Name)
@section('type','product')
@section('descriptionseo','Tony4men - Hàng chất lượng đảm bảo, giá tốt, đẹp như ý')
@section('image')

@section('content')
<section class="breadcrumb_background">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="wrap_breadcrumb a-center">
          <h1 class="title-head-page margin-top-0">{!! $danhmuc->Name !!}</h1>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="bread-crumb">
  <span class="crumb-border"></span>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 a-left">
        <ul class="breadcrumb" itemscope="" itemtype="">
          <li class="home">
            <a itemprop="url" href="{{ url('/') }}"><span itemprop="title">Trang chủ</span></a>            
            <span class="mr_lr"> / </span>
          </li>
          <li><strong><span itemprop="title"> {!! $danhmuc->Name !!} </span></strong></li>
        </ul>
      </div>
    </div>
  </div>
</section>
<div class="container margin-bottom-50">
  <div class="wrp_border_collection ">
    <div class="row">
      <section class="main_container collection collection_container col-lg-9 col-md-9 col-sm-12 col-lg-push-3 col-md-push-3">
        <div class="category-products products">
          <div class="sortPagiBar hidden-sm hidden-xs">
            <div class="srt">
              <div class="wr_sort col-sm-12">
                <div class="text-sm-left count_text" style="left: 0">
                  <div class="tt hidden-sm hidden-xs">
                    <span class="hidden-sm hidden-xs product_display">
                    Tổng cộng có {{ count($theloaisanpham) }} sản phẩm/trang </span>
                  </div>
                </div>
                <div class="text-sm-left">
                  <div class="sortPagiBar sortpage text-sm-right">
                    <div id="sort-by">
                      <label class="left hidden-xs">Sắp xếp theo: </label>
                      <div class="border_sort">
                        <select onchange="sortby(this.value)">
                          <option class="valued" value="">Mặc định</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          @if (count($theloaisanpham) > 0)
          <section class="products-view products-view-grid collection_reponsive">
            <div class="row row-gutter-14">
              @foreach ($theloaisanpham as $items)
              <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 product-col">
                <div class="product-box">
                  <div class="product-thumbnail">
                    <a href="{{ url('san-pham/'.$items->Slug) }}" class="image_link display_flex" data-images="" title="{{ $items->Name }}">
                      <img src="{{ asset('uploads/sanpham/'.$items->Image) }}" alt="{{ $items->Name }}">
                    </a>
                    <div class="product-action-grid clearfix">
                      <form action="" method="post" class="variants form-nut-grid" enctype="multipart/form-data">
                        <div>
                          <a title="xem nhanh" href="javascript:;" id="data-id" data-id="{{ $items->id }}" class="button_wh_40 btn_view right-to quick-view">
                            <i class="fa fa-search"></i>
                          </a>
                          <a href="javascript:;" id="data-id" data-id="{{ $items->id }}" class="button_wh_40 btn-cart left-to muahang" title="Mua hàng"><span>Mua hàng</span>
                          </a>
                          @if (Auth::check())
                          <a title="Yêu thích" id="data-id" data-id="{{ $items->id }}" ok="{{ Auth::user()->id }}" class="button_wh_40 iWishAdd iwishAddWrapper" href="javascript:;" >
                            <i class="fa fa-heart-o"></i>
                          </a>
                          @else
                          <a title="Yêu thích" class="button_wh_40 iWishAdda iwishAddWrapper" href="javascript:;" ><i class="fa fa-heart-o"></i>
                          </a>
                          @endif
                        </div>
                      </form>
                    </div>
                  </div>
                  <div class="product-info a-center">
                    <h3 class="product-name">
                      <a class="text2line" href="{{ url('san-pham/'.$items->Slug) }}" title="{{ $items->Name }}">{{ $items->Name }}</a>
                    </h3>
                    <div class="price-box clearfix">
                      @if ($items->GiaSale)
                      <span class="price product-price">{{ number_format($items->GiaSale,0,',','.') }}₫</span>
                      <span class="price product-price-old">
                        {{ number_format($items->Gia,0,',','.') }}₫     
                      </span>
                        {{-- <span class="sale_count">- 
                        24% 
                      </span> --}}
                      @else
                      <span class="price product-price">{{ number_format($items->Gia,0,',','.') }}₫</span>
                      @endif   
                    </div>
                  </div>
                </div>
              </div>
              @endforeach
            </div>
            <div class="text-xs-right xs_padding col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <nav>
                <ul class="pagination clearfix">
                  {!! $theloaisanpham->links() !!}
                </ul>
              </nav>
            </div>
            <div class="hidden coun_cls">
              <span class="hidden-sm hidden-xs call-count">Tổng cộng có {{ count($theloaisanpham) }} sản phẩm/trang </span>
            </div>
          </section>
          @else
          <div class="col-lg-12 col-md-12 block_width text-center">
            <span class="warning" style="color: #f8452d;font-size: 20px;">Đang cập nhật sản phẩm</span>
          </div>
          @endif
        </div>
      </section>
      @include('frontend.layout.sidebarSp')
    </div>
  </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
</script>
@endsection