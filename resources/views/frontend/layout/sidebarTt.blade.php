<aside class="left left-content col-lg-3 col-md-3 col-sm-12 col-xs-12 col-lg-pull-9 col-md-pull-9">
  <div class="aside-item">
    <div>
      <div class="title_module">
        <h2><a href="javascript:void(0)" title="Tin tức nổi bật">Tin tức nổi bật</a></h2>
      </div>
      <div class="list-blogs">
        <div class="blog_list_item">
          @foreach ($tintuc as $items)
          <article class="blog-item blog-item-list ">
            <div class="blog-item-thumbnail img1">
              <a href="{{ url('tin-tuc/'.$items->Slug) }}">
                <picture>
                  <img src="{{ asset('uploads/tintuc/'.$items->Image) }}" style="max-width:100%;" class="img-responsive" alt="{!! $items->TieuDe !!}">
                </picture>
              </a>
            </div>
            <div class="ct_list_item">
              <h3 class="blog-item-name"><a href="{{ url('tin-tuc/'.$items->Slug) }}">{!! $items->TieuDe !!}</a></h3>
              <span class="time_post"><i class="fa fa-clock-o"></i>&nbsp; {!! $items->created_at !!}</span>
            </div>
          </article>
          @endforeach
        </div>
      </div>
    </div>
  </div>
</aside>