<style type="text/css">
  .liveSearch{
    border: 1px solid #759ba1;
    margin-top: -2px;
    width: 203%;
    background: #ffffff;
    position: absolute;
    z-index: 9999;
  }
  .aSearch{
    color: black !important;
    padding-left: 1em !important;
  }
</style>
<div class="top_info">
  <div class="container xs-no-padding">
    <div class="row">
      <div class="col-lg-7 col-md-7 hidden-sm a-left hidden-xs">
        <div class="inline_">
          <span>
            Địa chỉ: 
            {!! $lienhe->DiaChi !!}
          </span>
          <span class="child_">
            Hotline:
            <a href="callto:{!! $lienhe->Sdt !!}">(+84) {!! $lienhe->Sdt !!}</a>
          </span>
        </div>
      </div>
      
      <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 a-right ">
        <div class="inline_ f-right">
          <div class="search_top_">
            <div class="search_for">
              <form class="form_search" action="{{ url('tim-kiem.html') }}" method="get" role="search">    
                <input type="search" name="search" onkeyup="Search(this.value,'result');" value="" placeholder="Tìm kiếm" class="input_search" autocomplete="off" required="">
                <ul id="liveSearch" class="liveSearch" style="display: none">
                  <li style="text-align: left"><a href="" class="aSearch">okoaksdoa</a></li>
                </ul>
                <span class="input-group-btn">
                  <button class="btn icon-fallback-text">
                    <i class="icon-magnifier icons"></i>
                  </button>
                </span>
              </form>
            </div>
          </div>
          <div class="child_">
            @if (Auth::check())
              <a class="hidden-lg hidden-md" href="" title="">{{Auth::user()->name}} / </a> 
              <a class="hidden-lg hidden-md" href="{{ url('dang-xuat') }}.html" onclick="return confirm('Bạn có muốn đăng xuất?')" title="Đăng Xuất">Đăng xuất</a>
              <a class="hidden-sm hidden-xs register_btn" href="javascript;" title="">{{Auth::user()->name}} / </a> 
              <a class="hidden-sm hidden-xs login_btn" href="{{ url('dang-xuat') }}.html" onclick="return confirm('Bạn có muốn đăng xuất?')" title="Đăng Xuất">Đăng xuất</a>
            @else
              <a class="hidden-lg hidden-md" href="{{ url('dang-ky') }}.html" title="Đăng ký">Đăng ký / </a> 
              <a class="hidden-lg hidden-md" href="{{ url('dang-nhap') }}.html"  title="Đăng nhập">Đăng nhập</a>
              <a class="hidden-sm hidden-xs register_btn" data-toggle="modal" href="{{ url('dang-ky') }}.html" title="Đăng ký">Đăng ký / </a> 
              <a class="hidden-sm hidden-xs login_btn"  data-toggle="modal" data-target="#login_register" href="#"  title="Đăng nhập">Đăng nhập</a>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

{{-- <a class="hidden-sm hidden-xs register_btn" data-toggle="modal" data-target="#login_register" href="#" title="Đăng ký">Đăng ký / </a>  --}}