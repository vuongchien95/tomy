<!DOCTYPE html>
<html lang="vi">
<head>
  <meta http-equiv="content-Type" content="text/html; charset=utf-8"/>
  <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <title> @yield('title') </title>
  <script>
    var iwish_template='index';
    var iwish_cid=parseInt('0',10);   
  </script>
  <script src='{{ asset('frontend/js/iwishheader.js') }}' type='text/javascript'></script>
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="keywords" content="@yield('keywords')"/>
  <meta name="description" content="@yield('description')">
  <meta property="og:url" content="@yield('url')" />
  <meta property="og:title" content="@yield('titleseo')" />
  <meta property="og:type" content="@yield('type')" />
  <meta property="og:description" content="@yield('descriptionseo')" />
  <meta property="og:image" content="@yield('image')" />
  <meta property="fb:app_id" content="2319166614977109" />
  <link rel="icon" href="{{ asset('uploads/cauhinh/'.$cauhinh->Favicon) }}" type="image/x-icon" />
  {{-- <link href='{{ asset('frontend/css/fonts.css') }}' rel='stylesheet' type='text/css' /> --}}
  {{-- <link href="{{ asset('frontend/css/fontsOpenSans.css') }}" rel="stylesheet">--}}
  {{-- <link href="{{ asset('frontend/css/fontsNotoSerif.css') }}" rel="stylesheet"> --}}
  <link href='https://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css' />
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600&amp;subset=vietnamese" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Noto+Serif:400,700,700i&amp;subset=vietnamese" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="{{ asset('frontend/css/font-awesome.min.css') }} ">
  <link rel="stylesheet" href="{{ asset('frontend/css/bootstrap.min.css') }} ">
  <script src="{{ asset('frontend/js/jquery.min.js') }}"></script>
  <script src="{{ asset('frontend/js/bootstrap.min.js') }}"></script>
  <link rel="stylesheet" href="{{ asset('frontend/css/font-awesome1.min.css') }}">
  <link rel="stylesheet" href="{{ asset('frontend/css/ionicons.min.css') }}" >
  <link rel="stylesheet" href="{{ asset('frontend/css/themify-icons.css') }}" >
  <link rel="stylesheet" href="{{ asset('frontend/css/simple-line-icons.css') }}">
  <link rel='stylesheet' href='{{ asset('frontend/css/owl.carousel.min.css') }}' type='text/css' />
  <link href='{{ asset('frontend/css/base.scss.css') }}' rel='stylesheet' type='text/css' />
  <link href='{{ asset('frontend/css/style.scss.css') }}' rel='stylesheet' type='text/css' />
  <link href='{{ asset('frontend/css/update.scss.css') }}' rel='stylesheet' type='text/css' />
  <link href='{{ asset('frontend/css/module.scss.css') }}' rel='stylesheet' type='text/css' />
  <link href='{{ asset('frontend/css/responsive.scss.css') }}' rel='stylesheet' type='text/css' />
  <link href='{{ asset('frontend/css/jquery.mmenu.scss.css') }}' rel='stylesheet' type='text/css' />
  <link href="{{ asset('frontend/css/jquerysctipttop.css') }}" rel="stylesheet" type="text/css">
  <script src='{{ asset('frontend/js/jquery-2.2.3.min.js') }}' type='text/javascript'></script>
  <script src='{{ asset('frontend/js/jquery.gmap.min.js') }}' type='text/javascript'></script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script>
    (function() {
      function asyncLoad() {
        var urls = ["{{ asset('frontend/js/productreviews.min.js') }}"];
        for (var i = 0; i < urls.length; i++) {
          var s = document.createElement('script');
          s.type = 'text/javascript';
          s.async = true;
          s.src = urls[i];
          s.src = urls[i];
          var x = document.getElementsByTagName('script')[0];
          x.parentNode.insertBefore(s, x);
        }
      }
      window.attachEvent ? window.attachEvent('onload', asyncLoad) : window.addEventListener('load', asyncLoad, false);
    })();
  </script>
  <script type='text/javascript'>
    (function() {
      var log = document.createElement('script'); log.type = 'text/javascript'; log.async = true;
      log.src = '{{ asset('frontend/js/270285.js') }}';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(log, s);
    })();
  </script>
  <link href='{{ asset('frontend/css/iwish.css') }}' rel='stylesheet' type='text/css' />
  <script>var ProductReviewsAppUtil=ProductReviewsAppUtil || {};</script>
  <link rel="stylesheet" href="{{ asset('frontend/css/default.css') }}" type="text/css" media="screen" />
  <link rel="stylesheet" href="{{ asset('frontend/css/nivo-slider.css') }}" type="text/css" media="screen" />
  <link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Itim" rel="stylesheet">
</head>
<body>
  <div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.0&appId=2319166614977109&autoLogAppEvents=1';
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>

  <!-- Load Facebook SDK for JavaScript -->
  <div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js#xfbml=1&version=v2.12&autoLogAppEvents=1';
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>

  <!-- Your customer chat code -->
  <div class="fb-customerchat"
  attribution=setup_tool
  page_id="807234932803663"
  logged_in_greeting="Xin chào ! Bạn cần trợ giúp gì ?"
  logged_out_greeting="Xin chào ! Bạn cần trợ giúp gì ?">
</div>

<div class="page-body">
  <div class="hidden-md hidden-lg opacity_menu"></div>
  <div class="opacity_filter"></div>
  <div class="body_opactiy"></div>
  <!-- Main content -->
  @include('frontend.layout.header') {{-- nhúng header --}}
  @include('frontend.layout.menu') {{-- nhúng menu --}}
  @yield('content') {{-- nhúng content --}}
  @yield('script') {{-- nhúng script --}}
  <link href='{{ asset('frontend/css/bpr-products-module.css') }}' rel='stylesheet' type='text/css' />
  <div class="bizweb-product-reviews-module"></div>
  @include('frontend.layout.toolbar') {{-- nhúng toolbar --}}
  @include('frontend.layout.footer') {{-- nhúng footer --}}
  <!--javascript -->
  <script type="text/javascript" src="{{ asset('frontend/js/jqueryslider.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('frontend/js/jquery.nivo.slider.js') }}"></script> 
  <script src='{{ asset('frontend/js/owl.carousel.min.js') }}' type='text/javascript'></script> 
  <script src='{{ asset('frontend/js/option-selectors.js') }}' type='text/javascript'></script>
  <script src='{{ asset('frontend/js/cs.script.js') }}' type='text/javascript'></script>
  <script src='{{ asset('frontend/js/jquery.mmenu.min.js') }}' type='text/javascript'></script>
  <script src='{{ asset('frontend/js/double_tab_togo.js') }}' type='text/javascript'></script>
  <script src='{{ asset('frontend/js/bootstrap1.min.js') }}' type='text/javascript'></script>
  <script src='{{ asset('frontend/js/api.jquery.js') }}' type='text/javascript'></script>
  <!-- Plugin JS -->
  <!-- Add to cart -->  
      {{-- <div class="ajax-load">
        <span class="loading-icon">
          <svg version="1.1"  xmlns="" xmlns:xlink="" x="0px" y="0px"
            width="24px" height="30px" viewBox="0 0 24 30" style="enable-background:new 0 0 50 50;" xml:space="preserve">
            <rect x="0" y="10" width="4" height="10" fill="#333" opacity="0.2">
              <animate attributeName="opacity" attributeType="XML" values="0.2; 1; .2" begin="0s" dur="0.6s" repeatCount="indefinite" />
              <animate attributeName="height" attributeType="XML" values="10; 20; 10" begin="0s" dur="0.6s" repeatCount="indefinite" />
              <animate attributeName="y" attributeType="XML" values="10; 5; 10" begin="0s" dur="0.6s" repeatCount="indefinite" />
            </rect>
            <rect x="8" y="10" width="4" height="10" fill="#333"  opacity="0.2">
              <animate attributeName="opacity" attributeType="XML" values="0.2; 1; .2" begin="0.15s" dur="0.6s" repeatCount="indefinite" />
              <animate attributeName="height" attributeType="XML" values="10; 20; 10" begin="0.15s" dur="0.6s" repeatCount="indefinite" />
              <animate attributeName="y" attributeType="XML" values="10; 5; 10" begin="0.15s" dur="0.6s" repeatCount="indefinite" />
            </rect>
            <rect x="16" y="10" width="4" height="10" fill="#333"  opacity="0.2">
              <animate attributeName="opacity" attributeType="XML" values="0.2; 1; .2" begin="0.3s" dur="0.6s" repeatCount="indefinite" />
              <animate attributeName="height" attributeType="XML" values="10; 20; 10" begin="0.3s" dur="0.6s" repeatCount="indefinite" />
              <animate attributeName="y" attributeType="XML" values="10; 5; 10" begin="0.3s" dur="0.6s" repeatCount="indefinite" />
            </rect>
          </svg>
        </span>
      </div>
      <div class="loading awe-popup">
        <div class="overlay"></div>
        <div class="loader" title="2">
          <svg version="1.1"  xmlns="" xmlns:xlink="" x="0px" y="0px"
            width="24px" height="30px" viewBox="0 0 24 30" style="enable-background:new 0 0 50 50;" xml:space="preserve">
            <rect x="0" y="10" width="4" height="10" fill="#333" opacity="0.2">
              <animate attributeName="opacity" attributeType="XML" values="0.2; 1; .2" begin="0s" dur="0.6s" repeatCount="indefinite" />
              <animate attributeName="height" attributeType="XML" values="10; 20; 10" begin="0s" dur="0.6s" repeatCount="indefinite" />
              <animate attributeName="y" attributeType="XML" values="10; 5; 10" begin="0s" dur="0.6s" repeatCount="indefinite" />
            </rect>
            <rect x="8" y="10" width="4" height="10" fill="#333"  opacity="0.2">
              <animate attributeName="opacity" attributeType="XML" values="0.2; 1; .2" begin="0.15s" dur="0.6s" repeatCount="indefinite" />
              <animate attributeName="height" attributeType="XML" values="10; 20; 10" begin="0.15s" dur="0.6s" repeatCount="indefinite" />
              <animate attributeName="y" attributeType="XML" values="10; 5; 10" begin="0.15s" dur="0.6s" repeatCount="indefinite" />
            </rect>
            <rect x="16" y="10" width="4" height="10" fill="#333"  opacity="0.2">
              <animate attributeName="opacity" attributeType="XML" values="0.2; 1; .2" begin="0.3s" dur="0.6s" repeatCount="indefinite" />
              <animate attributeName="height" attributeType="XML" values="10; 20; 10" begin="0.3s" dur="0.6s" repeatCount="indefinite" />
              <animate attributeName="y" attributeType="XML" values="10; 5; 10" begin="0.3s" dur="0.6s" repeatCount="indefinite" />
            </rect>
          </svg>
        </div>
      </div>--}}
      <div id="popup-cart" class="modal fade in" role="dialog" style="display: none;">
        <div id="popup-cart-desktop" class="clearfix">
          <div class="title-popup-cart">Thêm vào giỏ hàng thành công !</div>
          <div class="title-quantity-popup left_" onclick="window.location.href='{{ url('gio-hang.html') }}';">
            Giỏ hàng của bạn có 
            <span class="count_item_cart">(<span class="count_item_pr">{{ Cart::count() }}</span> sản phẩm) 
            <i class="fa fa-caret-right"></i>
          </span>
        </div>
        <div class="content-popup-cart">
          <div class="thead-popup">
            <div style="width: 49.75%;" class="text-left">Sản phẩm</div>
            <div style="width: 15%;" class="text-center">Đơn giá</div>
            <div style="width: 20%;" class="text-center">Số lượng</div>
            <div style="width: 15%;" class="text-right">Thành tiền</div>
          </div>
          <div class="tbody-popup scrollbar-dynamic">
            @foreach ($cart as $items)
            <div class="item-popup">
              <div style="width: 18%;" class="border height image_ text-left">
                <div class="item-image">
                  <a class="product-image" href="{{ url('san-pham/'.$items->options['slug']) }}" title="{{ $items->name }}">
                    <img alt="{{ $items->name }}" src="{{ asset('uploads/sanpham/'.$items->options['image']) }}" width="90">
                  </a>
                </div>
              </div>
              <div style="width:32%;" class="height text-left">
                <div class="item-info">
                  <p class="item-name">
                    <a class="text2line" href="{{ url('san-pham/'.$items->options['slug']) }}" title="{{ $items->name }}">{{ $items->name }}
                    </a>
                  </p>
                  <a href="javascript:;" onclick="return xoaCart({{ $items->id }})" class="xoaSanPham_{{ $items->id }}" title="Xóa" id="data-id" data-id="{{ $items->rowId }}">
                    <i class="fa fa-close"></i>&nbsp;&nbsp;Xoá
                  </a>
                  <p class="addpass" style="color:#fff;margin:0px;">
                    <span class="add_sus" style="color:#898989;">
                      <i style="margin-right:5px; color:#3cb878; font-size:14px;" class="fa fa-check" aria-hidden="true"></i>Mã Sp: {{ $items->options['masp'] }}
                    </span>
                  </p>
                  @if ($items->options['kichthuoc'] != null)
                  <p class="addpass" style="color:#fff;margin:0px;">
                    <span class="add_sus" style="color:#898989;">
                      <i style="margin-right:5px; color:#3cb878; font-size:14px;" class="fa fa-check" aria-hidden="true"></i>Size : {{ $items->options['kichthuoc'] }} - Màu : {{ $items->options['mausac'] }}
                    </span>
                  </p>
                  @else
                  <p class="addpass" style="color:#fff;margin:0px;">
                    <span class="add_sus" style="color:#898989;">
                      Size - Màu lấy theo thông tin trên ảnh
                    </span>
                  </p>
                  @endif
                </div>
              </div>
              <div style="width: 15%;" class="border height text-center">
                <div class="item-price"><span class="price">{{ number_format($items->price,0,',','.') }}₫</span></div>
              </div>
              <div style="width: 20%;" class="border height text-center">
                <div class="qty_thuongdq">
                  <input type="number" min="1" width="100" class="qtyItemCss qtyItem{{ $items->id }}" id="data-id" data-id="{{ $items->rowId }}" onchange="qtySp({{ $items->id }})" value="{{ $items->qty }}">
                </div>
              </div>
              <div style="width: 15%;" class="border height text-right">
                <span class="cart-price">
                  <span class="price" id="{{ $items->rowId }}">{!! number_format(($items->qty) * ($items->price),0,',','.') !!}₫</span> 
                </span>
              </div>
            </div>
            @endforeach
          </div>
          <div class="tfoot-popup">
            <div class="tfoot-popup-1 clearfix">
              <div class="title-quantity-popup popup-total right_ bottom_">
                <p class="tongtien">Tổng tiền thanh toán: <span class="total-price" id="total_cart_all">{{ $total }}₫</span></p>
              </div>
              <div class="pull-left popup-ship">
                <p class="hidden">Miễn phí giao hàng toàn quốc</p>
                <a class="" title="Tiếp tục mua hàng" onclick="$('#popup-cart').modal('hide');">
                  <span><i class="fa fa-caret-left"></i> <span>Tiếp tục mua hàng</span></span>
                </a>
              </div>
              <div class="pull-right popup-total">
                <p class="hidden">Phí vận chuyển: <span class="vanchuyen">Tính khi thanh toán</span></p>
                <p class="tongtien hidden">Tổng tiền: <span class="total-price" id="total_cart_all">{{ $total }}₫</span></p>
                <a class="button btn-proceed-checkout" title="Tiến hành đặt hàng" href="{{ url('thanh-toan') }}.html"><span>Tiến hành đặt hàng</span></a>
              </div>
            </div>
            <div class="tfoot-popup-2 clearfix">
            </div>
          </div>
        </div>
        <a title="Close" class="quickview-close close-window" href="javascript:;" onclick="$('#popup-cart').modal('hide');"><i class="fa  fa-close"></i></a>
      </div>
    </div>
    <script type="text/javascript" src="{{ asset('frontend/js/demoMyJs.js') }}"></script>
    <script src='{{ asset('frontend/js/cs.script.js') }}' type='text/javascript'></script>

    <script src='{{ asset('frontend/js/quickview.js') }}' type='text/javascript'></script>        
    <!-- Main JS -->  
    <script src='{{ asset('frontend/js/main.js') }}' type='text/javascript'></script>
    <script src='{{ asset('frontend/js/recentview.js') }}' type='text/javascript'></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/lightbox.css') }}">
    <script src='{{ asset('frontend/js/jquery.elevatezoom.js') }}' type='text/javascript'></script>
    <script src='{{ asset('frontend/js/jquery.elevatezoom308.min.js') }}' type='text/javascript'></script>
    <script src='{{ asset('frontend/js/jquery.prettyphoto.min005e.js') }}' type='text/javascript'></script>
    <script src='{{ asset('frontend/js/jquery.prettyphoto.init.min367a.js') }}' type='text/javascript'></script>
    <script src='{{ asset('frontend/js/showroom.js') }}' type='text/javascript'></script>
    <script src='{{ asset('frontend/js/jquery-gmaps-latlon-picker.js') }}' type='text/javascript'></script>
    <script src='{{ asset('frontend/js/productreviews.min.js') }}' type='text/javascript'></script>   
    <script src="{{ asset('frontend/js/slick.min.js') }}" type='text/javascript'></script>
    <script src="{{ asset('frontend/js/swiper.jquery.min.js') }}" type='text/javascript'></script>
    <!-- Product detail JS,CSS -->
    <script src='{{ asset('frontend/js/iwish.js') }}' type='text/javascript'></script>
    <!-- Quick view -->
    <div id="quick-view-product" class="quickview-product in" style="display: none">
      <div class="quickview-overlay fancybox-overlay fancybox-overlay-fixed"></div>
      <div class="quick-view-product">
        <div class="block-quickview primary_block row">
          <div class="product-left-column col-xs-12 col-sm-5 col-md-5 col-lg-5">
            <div class="clearfix image-block">
              <span class="view_full_size" id="view_full_size"></span>
              <div class="loading-imgquickview" style="display:none;"></div>
            </div>
            <div class="more-view-wrapper clearfix hidden">
              <div class="thumbs_quickview thumbs_list_quickview" id="thumbs_list_quickview">
                <ul class="product-photo-thumbs quickview-more-views-owlslider owl-loaded owl-drag" id="thumblist_quickview" style="visibility: visible;">
                  <div class="owl-stage-outer">
                    <div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: 0s; width: 258px;">
                      <div class="owl-item active" style="width: 80.667px; margin-right: 5px;">
                        <li><a href="javascript:void(0)" data-imageid="8987463" "="" data-zoom-image="https://bizweb.dktcdn.net/thumb/large/100/270/285/products/10.jpg"><img src="https://bizweb.dktcdn.net/thumb/large/100/270/285/products/10.jpg" alt="Office World" style="max-width:120px; max-height:120px;"></a></li>
                      </div>
                      <div class="owl-item active" style="width: 80.667px; margin-right: 5px;">
                        <li><a href="javascript:void(0)" data-imageid="8987463" "="" data-zoom-image="https://bizweb.dktcdn.net/thumb/large/100/270/285/products/08.jpg"><img src="https://bizweb.dktcdn.net/thumb/large/100/270/285/products/08.jpg" alt="Office World" style="max-width:120px; max-height:120px;"></a></li>
                      </div>
                      <div class="owl-item active" style="width: 80.667px; margin-right: 5px;">
                        <li><a href="javascript:void(0)" data-imageid="8987463" "="" data-zoom-image="https://bizweb.dktcdn.net/thumb/large/100/270/285/products/02.jpg"><img src="https://bizweb.dktcdn.net/thumb/large/100/270/285/products/02.jpg" alt="Office World" style="max-width:120px; max-height:120px;"></a></li>
                      </div>
                    </div>
                  </div>
                  <div class="owl-nav disabled">
                    <div class="owl-prev disabled"></div>
                    <div class="owl-next disabled"></div>
                  </div>
                  <div class="owl-dots disabled">
                    <div class="owl-dot active"><span></span></div>
                  </div>
                </ul>
              </div>
            </div>
          </div>
          <div class="product-center-column product-info product-item col-xs-5 col-sm-7 col-md-7 col-lg-7" id="">
            <div class="head-qv" id="head-qv"></div>
            <div class="quickview-info">
              <span class="vendor_">Thương hiệu: <span class="vendor" id="vendor"></span></span>

              <span class="prices">
                <span class="price" id="priceSp"></span>
                <del class="old-price" style="display: none;"></del>
              </span>
              <div class="vat_ hidden">
                (<span class="vat_qv">Giá chưa bao gồm VAT</span><span class="status_qv availabel_qv"></span>)
              </div>
            </div>
            <div class="product-description">
              <div class="rte" id="text3line" style="height: 210px;overflow: hidden;"></div>
            </div>
            <div>
              <form action="" method="post" enctype="multipart/form-data" class="quick_option" id="">
                {{ csrf_field() }}
                <div class="selector-wrapper col-md-6 hidden" style="display: none !important;">
                  <label for="kichthuoc" style="font-size: 14px;font-weight: bold;"> Kích Thước: </label>
                  <select class="single-option-selector" data-option="option1" id="kichthuoc">
                    <option value="XL">XL</option>
                    <option value="L">L</option>
                    <option value="M">M</option>
                  </select>
                </div>
                <div class="selector-wrapper col-md-6 hidden" style="display: none !important;">
                  <label for="mau" style="font-size: 14px;font-weight: bold;"> Màu: </label>
                  <select class="single-option-selector" data-option="option2" id="mau">
                    <option value="Đỏ">Đỏ</option>
                    <option value="Nâu">Nâu</option>
                  </select>
                </div>
                <div class="clearfix"></div>
                <div class="quantity_wanted_p">
                  <div class="input_qty_qv">
                    <a class="btn_num num_1 button button_qty hidden" onclick="var result = document.getElementById('quantity-detail'); var qtyqv = result.value; if( !isNaN( qtyqv ) &amp;&amp; qtyqv > 1 ) result.value--;return false;">-</a>
                    <input type="text" id="quantity-detail" class="hidden" name="quantity" value="1" onkeypress="validate(event)" onkeyup="valid(this,'numbers')" onblur="valid(this,'numbers')" class="form-control prd_quantity">
                    <a class="btn_num num_2 button button_qty hidden" onclick="var result = document.getElementById('quantity-detail'); var qtyqv = result.value; if( !isNaN( qtyqv )) result.value++;return false;">+</a>
                  </div>
                  <button type="button" name="add" style="margin-left: 10em;" class="btn btn-primary fix_add_to_cart button_cart_buy_enable">
                    <span>Thêm vào giỏ hàng</span>
                  </button>
                </div>
                <div class="total-price" style="display:none">0₫</div>
              </form>
            </div>
            
            <div class="phone_details text-center">
              <span class="phone"> Gọi ngay <a href="callto:{{ isset($lienhe) ? $lienhe->Sdt : null}}">(+84) {{ isset($lienhe) ? $lienhe->Sdt : null}}</a> để đặt và giao hàng tận nơi </span>
            </div>
          </div>
        </div>
        <a title="Close" class="quickview-close close-window" href="javascript:;"><i class="fa fa-times"></i></a>
      </div>
    </div>
  </div>
  <script type="text/javascript">
    $(window).load(function() {
      $('#slider').nivoSlider();
    });
    $("div.alert").delay(6000).slideUp();
  </script> 
  <script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-36251023-1']);
    _gaq.push(['_setDomainName', 'jqueryscript.net']);
    _gaq.push(['_trackPageview']);

    (function() {
      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
  </script>
  <script type="text/javascript">
    $(window).load(function() {
      $('#slider').nivoSlider({
        effect: 'random',
        slices: 15,
        boxCols: 8,
        boxRows: 4,
        animSpeed: 2000,
        pauseTime: 4000,
        startSlide: 0,
        directionNav: true,
        controlNav: true,
        controlNavThumbs: false,
        pauseOnHover: true,
        manualAdvance: false,
        prevText: 'Prev',
        nextText: 'Next',
        randomStart: false,
        beforeChange: function(){},
        afterChange: function(){},
        slideshowEnd: function(){},
        lastSlide: function(){},
        afterLoad: function(){}
      });
    });
  </script>
  <nav id="nav-mobile" class="hidden-md hidden-lg">
    <ul>
      <li>
        <a href="{{ url('/') }}" title="">Trang chủ</a>
      </li>
      @foreach ($menu_cap_1 as $items_cap_1)
      @php
      $menu_cap_2 = DB::table('danhmuc')->where('DanhMuc_Cha',$items_cap_1->id)->get();
      @endphp
      <li>
        @if (count($menu_cap_2) > 0)
        <a href="javascript:void(0)" title="">{!! $items_cap_1->Name !!}</a>
        @else
        <a href="{{ url($items_cap_1->Slug) }}">{!! $items_cap_1->Name !!}</a>
        @endif
        @if (count($menu_cap_2) > 0)
        <ul>
          @foreach ($menu_cap_2 as $items_cap_2)
          <li>
            <a href="{{ url($items_cap_2->Slug) }}" title="">{!! $items_cap_2->Name !!}</a>
          </li>
          @endforeach
        </ul>
        @endif
      </li>
      @endforeach
      <li>
        <a href="{{ url('gioi-thieu.html') }}" title="Giới thiệu Tony4men">Giới thiệu</a>
      </li>
      <li>
        <a href="{{ url('lien-he.html') }}" title="Liên hệ Tony4men">Liên hệ</a>
      </li>
    </ul>
  </nav>
</body>
</html>