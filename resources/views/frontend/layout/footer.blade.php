<footer class="footer">
  <div class="site-footer">
    <div class="container">
      <div class="footer-inner padding-bottom-50 padding-top-50">
        <div class="row">
          <div class="col-xs-12 col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="footer-widget">
              <div class="icon_fot">
                <span class="block_img_"><img src="{{ asset('img/home_.png') }}" title="Giờ mở cửa" alt="Giờ mở cửa"></span>
              </div>
              <h4 class="cliked margin-bottom-10 hasclick"><span>Giờ mở cửa</span></h4>
              <div class="content_f">
                <span class="first_title block_">Thứ 2-6: <span>8h00 am - 22h00 pm</span></span>
                <span class="first_title block_">Thứ 7-CN: <span>9h00 am - 21h00 pm</span></span>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="footer-widget">
              <div class="icon_fot">
                <span class="block_img_"><img src="{{ asset('img/email_.png') }}" title="Thông tin liên hệ" alt="Thông tin liên hệ"></span>
              </div>
              <h4 class="cliked margin-bottom-10 hasclick"><span>Thông tin liên hệ</span></h4>
              <div class="content_f">
                <span class="block_">{!! $lienhe->DiaChi !!}</span>
                <span class="block_">
                  Hotline: 
                  <a href="tel:{!! $lienhe->Sdt !!}">
                    (+84) {!! $lienhe->Sdt !!}
                  </a>
                </span>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-lg-4 col-md-4">
            <div class="footer-widget">
              <div class="icon_fot">
                <span class="block_img_">
                  <img src="{{ asset('img/connect_.png') }}" title="Kết nối với chúng tôi" alt="Kết nối với chúng tôi">
                </span>
              </div>
              <h4 class="cliked margin-bottom-10"><span>Kết nối với chúng tôi</span></h4>
              <ul class="follow_option">
                <li>
                  <a class="goplus" href="{!! $lienhe->Google !!}" title=""><i class="fa fa-google-plus"></i></a>
                </li>
                <li>
                  <a class="twitter" href="javascript:void(0)" title=""><i class="fa fa-twitter"></i></a>
                </li>
                <li>
                  <a class="fb" href="{!! $lienhe->Fb !!}" title="Theo dõi Facebook Tony4men"><i class="fa fa-facebook"></i></a>
                </li>
                <li>
                  <a class="in" href="javascript:void(0)" title=""><i class="fa fa-linkedin"></i></a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="copyright clearfix">
    <div class="container">
      <div class="inner clearfix">
        <div class="row tablet">
          <div id="copyright" class="col-lg-6 col-md-6 col-sm-12 col-xs-12 fot_copyright">
            <span class="wsp"><span class="mobile">@ Bản quyền thuộc về <b>Tony4men</b></span><span class="hidden-xs"> | </span>
                <span class="mobile">Cung cấp bởi <a href="http://facebook.com/vuongchien.2412" title="Vương Chiến" rel="nofollow" target="_blank">Vương Chiến</a></span>
            </span>
          </div>
          <div id="link_footer_menu" class="col-lg-6 col-md-6 col-sm-12 col-xs-12 menu_footer_link">
            <ul class="ul_main_footer">
              <li><a href="{{ url('/') }}" title="Trang chủ">Trang chủ</a></li>
              <li><a href="{{ url('san-pham') }}.html" title="Sản phẩm">Sản phẩm</a></li>
              <li><a href="{{ url('tin-tuc') }}.html" title="Tin tức">Tin tức</a></li>
              <li><a href="{{ url('gioi-thieu') }}.html" title="Giới thiệu">Giới thiệu</a></li>
              <li><a href="{{ url('lien-he') }}.html" title="Liên hệ">Liên hệ</a></li>
            </ul>
          </div>
        </div>
      </div>
      <a href="#" id="back-to-top" class="backtop"  title="Lên đầu trang"><i class="fa fa-angle-up"></i></a>
      <a class="hidden-lg phone_backtotop left_center" title="Gọi ngay" href="tel:{!! $lienhe->Sdt !!}"><i class="fa fa-phone"></i></a>
    </div>
  </div>
</footer>
