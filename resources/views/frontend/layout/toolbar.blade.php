<div class="toolbar_slide show">
  <div class="wrap_toolbar">
    <div class="item_ cart__" id="cart__">
      <a href="{{ url('gio-hang.html') }}">
        <span class="count_fixed">
          <span class="count_item_pr">{{ Cart::count() }}</span>
        </span>
        <img src="{{ asset('frontend/img/cart_.png') }}" alt="Giỏ hàng">
      </a>
      <div class="tooltip_bar"><span>Giỏ hàng của bạn</span></div>
    </div>
    @if (Auth::check())
    <div class="item_ wishd">
      <a class="" href="{{ url('yeu-thich.html') }}">Yêu thích</a>
      <div class="tooltip_bar"><span>Sản phẩm yêu thích</span></div>
    </div>
    @else
    <div class="item_ wishd">
      <a class="" href="javascript:;">Yêu thích</a>
      <div class="tooltip_bar"><span>Vui lòng đăng nhập</span></div>
    </div>
    @endif
    
  </div>
</div>