<aside class="dqdt-sidebar sidebar left left-content col-xs-12 col-lg-3 col-md-3 col-sm-12 col-lg-pull-9 col-md-pull-9">
  <aside class="aside-item collection-category">
    <div class="title_module">
      <h2><span>Danh mục sản phẩm</span></h2>
    </div>
    <div class="aside-content aside-cate-link-cls">
      <nav class="cate_padding nav-category navbar-toggleable-md">
        <ul class="nav-ul nav navbar-pills">
          @foreach ($menu_cap_1 as $items_cap_1)
          @php
          $menu_cap_2 = DB::table('danhmuc')->where('DanhMuc_Cha',$items_cap_1->id)->get();
          @endphp
          <li class="nav-item  lv1">
            @if (count($menu_cap_2) > 0)
            <a href="javascript:;" class="nav-link">{!! $items_cap_1->Name !!}</a>
            @else
            <a href="{{ url($items_cap_1->Slug) }}" class="nav-link ">{!! $items_cap_1->Name !!}</a>
            @endif

            @if (count($menu_cap_2) > 0)
            <i class="fa fa-angle-down"></i>
            <ul class="dropdown-menu">
              @foreach ($menu_cap_2 as $items_cap_2)
              <li class="nav-item lv2 ">
                <a class="nav-link" href="{{ url($items_cap_2->Slug) }}">{!! $items_cap_2->Name !!}</a>
              </li>
              @endforeach
            </ul>
            @endif
          </li>
          @endforeach
        </ul>
      </nav>
    </div>
  </aside>
  <div class="aside-filter">
    <aside class="aside-item filter-vendor">
      <div class="aside-title aside-title-fillter">
        <div class="title_module margin-bottom-10">
          <h2><span>Màu Sắc</span></h2>
        </div>
      </div>
      <div class="aside-content filter-group">
        <ul>
          <li class="filter-item filter-item--check-box filter-item--green ">
            <span>
              <label class="label_relative" for="mausac1">
                <input type="checkbox" id="mausac1" value="(Gong Cha)">
                <i class="fa"></i>
                <span class="filter_tt">Xanh</span>
              </label>
            </span>
          </li>
          <li class="filter-item filter-item--check-box filter-item--green ">
            <span>
              <label class="label_relative" for="mausac2">
                <input type="checkbox" id="mausac2" value="(Gong Cha)">
                <i class="fa"></i>
                <span class="filter_tt">Đỏ</span>
              </label>
            </span>
          </li>
          <li class="filter-item filter-item--check-box filter-item--green ">
            <span>
              <label class="label_relative" for="mausac3">
                <input type="checkbox" id="mausac3" value="(Gong Cha)">
                <i class="fa"></i>
                <span class="filter_tt">Đen</span>
              </label>
            </span>
          </li>
          <li class="filter-item filter-item--check-box filter-item--green ">
            <span>
              <label class="label_relative" for="mausac4">
                <input type="checkbox" id="mausac4" value="(Gong Cha)">
                <i class="fa"></i>
                <span class="filter_tt">Vàng</span>
              </label>
            </span>
          </li>
          <li class="filter-item filter-item--check-box filter-item--green ">
            <span>
              <label class="label_relative" for="mausac5">
                <input type="checkbox" id="mausac5" value="(Gong Cha)">
                <i class="fa"></i>
                <span class="filter_tt">Trắng</span>
              </label>
            </span>
          </li>
          <li class="filter-item filter-item--check-box filter-item--green ">
            <span>
              <label class="label_relative" for="mausac6">
                <input type="checkbox" id="mausac6" value="(Gong Cha)">
                <i class="fa"></i>
                <span class="filter_tt">Nâu</span>
              </label>
            </span>
          </li>
          <li class="filter-item filter-item--check-box filter-item--green ">
            <span>
              <label class="label_relative" for="mausac7">
                <input type="checkbox" id="mausac7" value="(Gong Cha)">
                <i class="fa"></i>
                <span class="filter_tt">Xám</span>
              </label>
            </span>
          </li>
          <li class="filter-item filter-item--check-box filter-item--green ">
            <span>
              <label class="label_relative" for="mausac8">
                <input type="checkbox" id="mausac8" value="(Gong Cha)">
                <i class="fa"></i>
                <span class="filter_tt">Bạc</span>
              </label>
            </span>
          </li>
        </ul>
      </div>
    </aside>
    <aside class="aside-item filter-price">
      <div class="aside-title aside-title-fillter">
        <div class="title_module margin-bottom-10">
          <h2><span>Giá</span></h2>
        </div>
      </div>
      <div class="aside-content filter-group">
        <ul>
          <li class="filter-item filter-item--check-box filter-item--green">
            <span>
              <label for="duoi200">
                <input type="radio" onclick="check(this.value)" class="class" name="ckeckGia" id="duoi200" value="200" >
                <i class="fa"></i>
                Giá dưới 200.000đ
              </label>
            </span>
          </li>
          <li class="filter-item filter-item--check-box filter-item--green">
            <span>
              <label for="200-300">
                <input type="radio" onclick="check(this.value)" class="class" name="ckeckGia" id="200-300" value="300" >
                <i class="fa"></i>
                200.000đ - 300.000đ             
              </label>
            </span>
          </li>
          <li class="filter-item filter-item--check-box filter-item--green">
            <span>
              <label for="300-400">
                <input type="radio" onclick="check(this.value)" class="class" name="ckeckGia" id="300-400" value="400" >
                <i class="fa"></i>
                300.000đ - 400.000đ             
              </label>
            </span>
          </li>
          <li class="filter-item filter-item--check-box filter-item--green">
            <span>
              <label for="400-500">
                <input type="radio" onclick="check(this.value)" class="class" name="ckeckGia" id="400-500" value="500" >
                <i class="fa"></i>
                400.000đ - 500.000đ             
              </label>
            </span>
          </li>
          <li class="filter-item filter-item--check-box filter-item--green">
            <span>
              <label for="500-1tr">
                <input type="radio" onclick="check(this.value)" class="class" name="ckeckGia" id="500-1tr" value="1000000">
                <i class="fa"></i>
                500.000đ - 1.000.000đ             
              </label>
            </span>
          </li>
          <li class="filter-item filter-item--check-box filter-item--green">
            <span>
              <label for="tren1tr">
                <input type="radio" onclick="check(this.value)" class="class" name="ckeckGia" id="tren1tr" value="2000000">
                <i class="fa"></i>
                Giá trên 1.000.000đ
              </label>
            </span>
          </li>
        </ul>
      </div>
    </aside>
    <aside class="aside-item filter-vendor">
      <div class="aside-title aside-title-fillter">
        <div class="title_module margin-bottom-10">
          <h2><span>Kích thước</span></h2>
        </div>
      </div>
      <div class="aside-content filter-group sizes-list">
        <ul>
          <li class="filter-item size filter-item--check-box filter-item--green filter-size">
            <span>
              <label class="label_relative" for="filter-xxl">
                <input type="checkbox" id="filter-xxl" onchange="toggleFilter(this)" data-group="tag2" data-field="tags" data-text="XXL" value="(XXL)" data-operator="OR">
                <i class="fa"></i>
                <span class="tags_size">XXL (áo)</span>
              </label>
            </span>
          </li>
          <li class="filter-item size filter-item--check-box filter-item--green filter-size">
            <span>
              <label class="label_relative" for="filter-xl">
                <input type="checkbox" id="filter-xl" onchange="toggleFilter(this)" data-group="tag2" data-field="tags" data-text="XL" value="(XL)" data-operator="OR">
                <i class="fa"></i>
                <span class="tags_size">XL (áo)</span>
              </label>
            </span>
          </li>
          <li class="filter-item size filter-item--check-box filter-item--green filter-size">
            <span>
              <label class="label_relative" for="filter-l">
                <input type="checkbox" id="filter-l" onchange="toggleFilter(this)" data-group="tag2" data-field="tags" data-text="L" value="(L)" data-operator="OR">
                <i class="fa"></i>
                <span class="tags_size">L (áo)</span>
              </label>
            </span>
          </li>
          <li class="filter-item size filter-item--check-box filter-item--green filter-size">
            <span>
              <label class="label_relative" for="filter-m">
                <input type="checkbox" id="filter-m" onchange="toggleFilter(this)" data-group="tag2" data-field="tags" data-text="M" value="(M)" data-operator="OR">
                <i class="fa"></i>
                <span class="tags_size">M (áo)</span>
              </label>
            </span>
          </li>
          <li class="filter-item size filter-item--check-box filter-item--green filter-size">
            <span>
              <label class="label_relative" for="filter-s">
                <input type="checkbox" id="filter-s" onchange="toggleFilter(this)" data-group="tag2" data-field="tags" data-text="S" value="(S)" data-operator="OR">
                <i class="fa"></i>
                <span class="tags_size">S (áo)</span>
              </label>
            </span>
          </li>
          <li class="filter-item size filter-item--check-box filter-item--green filter-size">
            <span>
              <label class="label_relative" for="filter-s">
                <input type="checkbox" id="filter-s" onchange="toggleFilter(this)" data-group="tag2" data-field="tags" data-text="S" value="(S)" data-operator="OR">
                <i class="fa"></i>
                <span class="tags_size">28 (quần)</span>
              </label>
            </span>
          </li>
          <li class="filter-item size filter-item--check-box filter-item--green filter-size">
            <span>
              <label class="label_relative" for="filter-s">
                <input type="checkbox" id="filter-s" onchange="toggleFilter(this)" data-group="tag2" data-field="tags" data-text="S" value="(S)" data-operator="OR">
                <i class="fa"></i>
                <span class="tags_size">29 (quần)</span>
              </label>
            </span>
          </li>
          <li class="filter-item size filter-item--check-box filter-item--green filter-size">
            <span>
              <label class="label_relative" for="filter-s">
                <input type="checkbox" id="filter-s" onchange="toggleFilter(this)" data-group="tag2" data-field="tags" data-text="S" value="(S)" data-operator="OR">
                <i class="fa"></i>
                <span class="tags_size">30 (quần)</span>
              </label>
            </span>
          </li>
          <li class="filter-item size filter-item--check-box filter-item--green filter-size">
            <span>
              <label class="label_relative" for="filter-s">
                <input type="checkbox" id="filter-s" onchange="toggleFilter(this)" data-group="tag2" data-field="tags" data-text="S" value="(S)" data-operator="OR">
                <i class="fa"></i>
                <span class="tags_size">31 (quần)</span>
              </label>
            </span>
          </li>
          <li class="filter-item size filter-item--check-box filter-item--green filter-size">
            <span>
              <label class="label_relative" for="filter-s">
                <input type="checkbox" id="filter-s" onchange="toggleFilter(this)" data-group="tag2" data-field="tags" data-text="S" value="(S)" data-operator="OR">
                <i class="fa"></i>
                <span class="tags_size">32 (quần)</span>
              </label>
            </span>
          </li>
          <li class="filter-item size filter-item--check-box filter-item--green filter-size">
            <span>
              <label class="label_relative" for="filter-s">
                <input type="checkbox" id="filter-s" onchange="toggleFilter(this)" data-group="tag2" data-field="tags" data-text="S" value="(S)" data-operator="OR">
                <i class="fa"></i>
                <span class="tags_size">33 (quần)</span>
              </label>
            </span>
          </li>
          <li class="filter-item size filter-item--check-box filter-item--green filter-size">
            <span>
              <label class="label_relative" for="filter-s">
                <input type="checkbox" id="filter-s" onchange="toggleFilter(this)" data-group="tag2" data-field="tags" data-text="S" value="(S)" data-operator="OR">
                <i class="fa"></i>
                <span class="tags_size">34 (quần)</span>
              </label>
            </span>
          </li>
          <li class="filter-item size filter-item--check-box filter-item--green filter-size">
            <span>
              <label class="label_relative" for="filter-s">
                <input type="checkbox" id="filter-s" onchange="toggleFilter(this)" data-group="tag2" data-field="tags" data-text="S" value="(S)" data-operator="OR">
                <i class="fa"></i>
                <span class="tags_size">38 (giày - dép)</span>
              </label>
            </span>
          </li>
          <li class="filter-item size filter-item--check-box filter-item--green filter-size">
            <span>
              <label class="label_relative" for="filter-s">
                <input type="checkbox" id="filter-s" onchange="toggleFilter(this)" data-group="tag2" data-field="tags" data-text="S" value="(S)" data-operator="OR">
                <i class="fa"></i>
                <span class="tags_size">39 (giày - dép)</span>
              </label>
            </span>
          </li>
          <li class="filter-item size filter-item--check-box filter-item--green filter-size">
            <span>
              <label class="label_relative" for="filter-s">
                <input type="checkbox" id="filter-s" onchange="toggleFilter(this)" data-group="tag2" data-field="tags" data-text="S" value="(S)" data-operator="OR">
                <i class="fa"></i>
                <span class="tags_size">40 (giày - dép)</span>
              </label>
            </span>
          </li>
          <li class="filter-item size filter-item--check-box filter-item--green filter-size">
            <span>
              <label class="label_relative" for="filter-s">
                <input type="checkbox" id="filter-s" onchange="toggleFilter(this)" data-group="tag2" data-field="tags" data-text="S" value="(S)" data-operator="OR">
                <i class="fa"></i>
                <span class="tags_size">41 (giày - dép)</span>
              </label>
            </span>
          </li>
          <li class="filter-item size filter-item--check-box filter-item--green filter-size">
            <span>
              <label class="label_relative" for="filter-s">
                <input type="checkbox" id="filter-s" onchange="toggleFilter(this)" data-group="tag2" data-field="tags" data-text="S" value="(S)" data-operator="OR">
                <i class="fa"></i>
                <span class="tags_size">42 (giày - dép)</span>
              </label>
            </span>
          </li>
          <li class="filter-item size filter-item--check-box filter-item--green filter-size">
            <span>
              <label class="label_relative" for="filter-s">
                <input type="checkbox" id="filter-s" onchange="toggleFilter(this)" data-group="tag2" data-field="tags" data-text="S" value="(S)" data-operator="OR">
                <i class="fa"></i>
                <span class="tags_size">43 (giày - dép)</span>
              </label>
            </span>
          </li>
        </ul>
      </div>
    </aside>
  </div>
  <script>
    var selectedSortby;
    var tt = 'Thứ tự';
    var selectedViewData = "data";
    var filter = new Bizweb.SearchFilter()

    function toggleFilter(e) {
      _toggleFilter(e);
      renderFilterdItems();
      doSearch(1);
    }
    function _toggleFilterdqdt(e) {
      var $element = $(e);
      var group = 'Khoảng giá';
      var field = 'price_min';
      var operator = 'OR';   
      var value = $element.attr("data-value");  
      filter.deleteValuedqdt(group, field, value, operator);
      filter.addValue(group, field, value, operator);
      renderFilterdItems();
      doSearch(1);
    }

    function _toggleFilter(e) {
      var $element = $(e);
      var group = $element.attr("data-group");
      var field = $element.attr("data-field");
      var text = $element.attr("data-text");
      var value = $element.attr("value");
      var operator = $element.attr("data-operator");
      var filterItemId = $element.attr("id");

      if (!$element.is(':checked')) {
        filter.deleteValue(group, field, value, operator);
      }
      else{
        filter.addValue(group, field, value, operator);
      }

      $(".catalog_filters li[data-handle='" + filterItemId + "']").toggleClass("active");
    }

    function renderFilterdItems() {
      var $container = $(".filter-container__selected-filter-list ul");
      $container.html("");

      $(".filter-container input[type=checkbox]").each(function(index) {
        if ($(this).is(':checked')) {
          var id = $(this).attr("id");
          var name = $(this).closest("label").text();
          
          addFilteredItem(name, id);
        }
      });

      if($(".filter-container input[type=checkbox]:checked").length > 0)
        $(".filter-container__selected-filter").show();
      else
        $(".filter-container__selected-filter").hide();
    }
    function addFilteredItem(name, id) {
      var filteredItemTemplate = "<li class='filter-container__selected-filter-item' for='{3}'><a href='javascript:void(0)' onclick=\"{0}\"><i class='fa fa-close'></i> {1}</a></li>";
      filteredItemTemplate = filteredItemTemplate.replace("{0}", "removeFilteredItem('" + id + "')");
      filteredItemTemplate = filteredItemTemplate.replace("{1}", name);
      filteredItemTemplate = filteredItemTemplate.replace("{3}", id);
      var $container = $(".filter-container__selected-filter-list ul");
      $container.append(filteredItemTemplate);
    }
    function removeFilteredItem(id) {
      $(".filter-container #" + id).trigger("click");
    }
    function clearAllFiltered() {
      filter = new Bizweb.SearchFilter();


      $(".filter-container__selected-filter-list ul").html("");
      $(".filter-container input[type=checkbox]").attr('checked', false);
      $(".filter-container__selected-filter").hide();

      doSearch(1);
    }
    function doSearch(page, options) {
      if(!options) options = {};
                 //NProgress.start();
                 $('.aside.aside-mini-products-list.filter').removeClass('active');
                 awe_showPopup('.loading');
                 filter.search({
                  view: selectedViewData,
                  page: page,
                  sortby: selectedSortby,
                  success: function (html) {
                    var $html = $(html);
                     // Muốn thay thẻ DIV nào khi filter thì viết như này
                     var $categoryProducts = $($html[0]); 
                     var xxx = $categoryProducts.find('.call-count');
                     $('.tt span').text(xxx.text());

                     $(".category-products").html($categoryProducts.html());
                     pushCurrentFilterState({sortby: selectedSortby, page: page});
                     awe_hidePopup('.loading');
                     initQuickView();
                     $('.add_to_cart').click(function(e){
                      e.preventDefault();
                      var $this = $(this);               
                      var form = $this.parents('form');              
                      $.ajax({
                        type: 'POST',
                        url: '/cart/add.js',
                        async: false,
                        data: form.serialize(),
                        dataType: 'json',
                        error: addToCartFail,
                        beforeSend: function() {  
                          if(window.theme_load == "icon"){
                            awe_showLoading('.btn-addToCart');
                          } else{
                            awe_showPopup('.loading');
                          }
                        },
                        success: addToCartSuccess,
                        cache: false
                      });
                    });
                     $('html, body').animate({
                      scrollTop: $('.category-products').offset().top
                    }, 0);

                     // setTimeout(function(){           
                     //   owl_thumb_image();
                     //   hover_thumb_image();
                     // },200);
                     // count_product();
                     
                     resortby(selectedSortby);
                     callbackW();
                     $('.open-filters').removeClass('open');
                     $('.dqdt-sidebar').removeClass('open');
                     $('.tt span').text(xxx.text());
                     if (window.BPR !== undefined){
                      return window.BPR.initDomEls(), window.BPR.loadBadges();
                    }


                  }
                });   

               }

               function sortby(sort) {      
                switch(sort) {
                  case "price-asc":
                  selectedSortby = "price_min:asc";            
                  break;
                  case "price-desc":
                  selectedSortby = "price_min:desc";
                  break;
                  case "alpha-asc":
                  selectedSortby = "name:asc";
                  break;
                  case "alpha-desc":
                  selectedSortby = "name:desc";
                  break;
                  case "created-desc":
                  selectedSortby = "created_on:desc";
                  break;
                  case "created-asc":
                  selectedSortby = "created_on:asc";
                  break;
                  default:
                  selectedSortby = "";
                  break;
                }

                doSearch(1);
              }

              function resortby(sort) {
                switch(sort) {          
                  case "price_min:asc":
                  tt = "Giá tăng dần";
                  break;
                  case "price_min:desc":
                  tt = "Giá giảm dần";
                  break;
                  case "name:asc":
                  tt = "Từ A-Z";
                  break;
                  case "name:desc":
                  tt = "Từ Z-A";
                  break;
                  case "created_on:desc":
                  tt = "Cũ đến mới";
                  break;
                  case "created_on:asc":
                  tt = "Mới đến cũ";
                  break;
                  default:
                  tt = "Mặc định";
                  break;
                }        
                $('#sort-by > div > select .valued').html(tt);


              }


              function _selectSortby(sort) {       
                resortby(sort);
                switch(sort) {
                  case "price-asc":
                  selectedSortby = "price_min:asc";
                  break;
                  case "price-desc":
                  selectedSortby = "price_min:desc";
                  break;
                  case "alpha-asc":
                  selectedSortby = "name:asc";
                  break;
                  case "alpha-desc":
                  selectedSortby = "name:desc";
                  break;
                  case "created-desc":
                  selectedSortby = "created_on:desc";
                  break;
                  case "created-asc":
                  selectedSortby = "created_on:asc";
                  break;
                  default:
                  selectedSortby = sort;
                  break;
                }
              }

              function toggleCheckbox(id) {
                $(id).click();
              }

              function pushCurrentFilterState(options) {

                if(!options) options = {};
                var url = filter.buildSearchUrl(options);
                var queryString = url.slice(url.indexOf('?'));        
                if(selectedViewData == 'data_list'){
                  queryString = queryString + '&view=list';        
                }
                else{
                  queryString = queryString + '&view=grid';          
                }

                pushState(queryString);
              }

              function pushState(url) {
                window.history.pushState({
                  turbolinks: true,
                  url: url
                }, null, url)
              }
              function switchView(view) {       
                switch(view) {
                  case "list":
                  selectedViewData = "data_list";            
                  break;
                  default:
                  selectedViewData = "data";

                  break;
                }        
                var url = window.location.href;
                var page = getParameter(url, "page");
                if(page != '' && page != null){
                 doSearch(page);
               }else{
                 doSearch(1);
               }
             }

             function selectFilterByCurrentQuery() {
              var isFilter = false;
              var url = window.location.href;
              var queryString = decodeURI(window.location.search);
              var filters = queryString.match(/\(.*?\)/g);
              if(queryString) {
               isFilter = true;
             }
             if(filters && filters.length > 0) {
              filters.forEach(function(item) {
                item = item.replace(/\(\(/g, "(");
                var element = $(".filter-container input[value='" + item + "']");
                element.attr("checked", "checked");
                _toggleFilter(element);
              });

              isFilter = true;
            }

            var sortOrder = getParameter(url, "sortby");
            if(sortOrder) {
              _selectSortby(sortOrder);
            }

            if(isFilter) {
              doSearch(page);
            }
          }
          
          function getParameter(url, name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(url);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
          }
          
          $( document ).ready(function() {
            selectFilterByCurrentQuery();
            $('.filter-group .filter-group-title').click(function(e){
              $(this).parent().toggleClass('active');
            });

            $('.filter-mobile').click(function(e){
              $('.aside.aside-mini-products-list.filter').toggleClass('active');
            });

            $('#show-admin-bar').click(function(e){
              $('.aside.aside-mini-products-list.filter').toggleClass('active');
            });

            $('.filter-container__selected-filter-header-title').click(function(e){
              $('.aside.aside-mini-products-list.filter').toggleClass('active');
            });
          });

        </script> 
        <div class="off_today off_today_collection hidden-sm hidden-xs margin-top-10">
          <div class="title_module margin-bottom-15">
            <h2><a href="javascript:void(0)" title="Sản phẩm bán chạy">Sản phẩm bán chạy</a></h2>
          </div>
          <div class="sale_off_today">
            <div class="not-dqowl wrp_list_product">
              @foreach ($sanphambanchay as $items)
              <div class="item_small">
                <div class="product-box product-list-small">
                  <div class="product-thumbnail">
                    <a href="{{ url('san-pham/'.$items->Slug) }}" title="{{ $items->Name }}">
                      <img src="{{ asset('uploads/sanpham/'.$items->Image) }}" alt="{{ $items->Name }}">
                    </a>  
                  </div>
                  <div class="product-info a-left">
                    <h3 class="product-name"><a class="ab" href="{{ url('san-pham/'.$items->Slug) }}" title="{{ $items->Name }}">{{ $items->Name }}</a></h3>
                    @if ($items->GiaSale)
                    <span class="price product-price">{{ number_format($items->GiaSale,0,',','.') }}₫</span>
                    <span class="price product-price-old">
                      {{ number_format($items->Gia,0,',','.') }}₫     
                    </span>
                    
                    @else
                    <span class="price product-price">{{ number_format($items->Gia,0,',','.') }}₫</span>
                    @endif
                  </div>
                </div>
              </div>
              @endforeach
            </div>
          </div>
        </div>
        <span class="border-das-sider"></span>
      </aside>
      <div id="open-filters" class="open-filters hidden-lg hidden-md">
        <span class="fter">
          <i class="fa fa-filter"></i>
          <span>Lọc</span>
        </span>
      </div>