<div class="wrap_header_top">
  <div class="header_top">
    <div class="topbar_wrap">
      <div class="container">
        <div class="row">
          <div class="head_content col-lg-12 col-md-12 col-sm-12">
            <div class="row">
              <div class="menu-bar hidden-md hidden-lg">
                <a href="#nav-mobile">
                  <img src='{{ asset('menu-bar.png') }}' alt='menu bar' /> 
                </a>
              </div>
              <div class="logo_top col-lg-2 col-md-2">
                <h1 style="margin: 0;padding: 0" title="{{ $cauhinh->TieuDe }}" alt="{{ $cauhinh->TuKhoa }}" >
                  <a href="{{ url('/') }}" class="logo-wrapper" id="ok">         
                    <img src="{{ asset('uploads/images/logo/'.$logo->Image) }}" alt="Tony4men ">         
                  </a>
                </h1>
              </div>
              <div class="col-lg-9 col-md-9">
                <div class="main_nav_header">
                  <nav class="hidden-sm hidden-xs nav-main">
                    <div class="menu_hed head_1">
                      <ul class="nav nav_1">
                        <li class=" nav-item nav-items active  ">
                          <a class="nav-link" href="{{ url('/') }}">
                          Trang chủ </a>
                        </li>
                        @foreach ($menu_cap_1 as $items_cap_1)
                        @php
                        $menu_cap_2 = DB::table('danhmuc')->where('DanhMuc_Cha',$items_cap_1->id)->get();
                        @endphp
                        <li class="menu_hover nav-item nav-items ">
                          @if (count($menu_cap_2) > 0)
                          <a href="javascript:void(0)" class="nav-link ">{!! $items_cap_1->Name !!}</a>
                          @else
                          <a href="{{ url($items_cap_1->Slug) }}" class="nav-link ">{!! $items_cap_1->Name !!}</a>
                          @endif

                          @if (count($menu_cap_2) > 0)
                          <ul class="dropdown-menu border-box">
                            @foreach ($menu_cap_2 as $items_cap_2)
                            <li class="nav-item-lv2">
                              <a class="nav-link" href="{{ url($items_cap_2->Slug) }}">{!! $items_cap_2->Name !!}</a>
                            </li>
                            @endforeach
                          </ul>
                          @endif
                        </li>
                        @endforeach
                      </ul>
                    </div>
                  </nav>
                </div>
              </div>
              <div class="header_top_cart">
                <div class="search-cart ">
                  <div class="top-cart-contain" id="load-div-to">
                    <div class="mini-cart cart_hover text-xs-center">
                      <div class="heading-cart">
                        <a href="{{ url('gio-hang.html') }}">
                          <span class="background_cart"></span>
                          <span class="cart_num" id="cart-total"><span class="color_"><span class="cartCount  count_item_pr" id="countCart">{{ Cart::count() }}</span></span></span>
                        </a>
                      </div>
                      <div class="top-cart-content" >
                        <ul id="cart-sidebar" class="mini-products-list count_li">
                          @if (Cart::count() > 0)
                          <ul class="list-item-cart">
                            @foreach ($cart as $items)
                            <li class="item" id="itemsCart">
                              <div class="wrap_item">
                                <a class="product-image" href="{{ url('san-pham/'.$items->options['slug']) }}" title="{{ $items->name }}">
                                  <img alt="{{ $items->name }}" src="{{ asset('uploads/sanpham/'.$items->options['image']) }}" width="80">
                                </a>
                                <div class="detail-item">
                                  <div class="product-details">
                                    <a href="javascript:;" id="data-id" data-id="{{ $items->rowId }}" onclick="return xoaCart({{ $items->id }})" title="Xóa" class="xoaSanPham_{{ $items->id }} remove-item-cart fa fa-times">&nbsp;</a>
                                    <p class="product-name text2line">
                                     <a href="{{ url('san-pham/'.$items->options['slug']) }}" title="{{ $items->name }}">{{ $items->name }}</a></p>
                                   </div>
                                   <div class="product-details-bottom">
                                    <span class="price">{{ number_format($items->price,0,',','.') }}₫</span>
                                    <span class="quaty item_quanty_count"> x {{ $items->qty }} cái</span>
                                    @if ($items->options['kichthuoc'] != null)
                                    <div class="quantity-select qty_drop_cart" style="padding-left: 0">
                                      Size {{ $items->options['kichthuoc'] }} - {{ $items->options['mausac'] }}
                                    </div>
                                    @else
                                    <div class="quantity-select qty_drop_cart" style="padding-left: 0;line-height: 15px;font-size: 13px;">
                                      Size - Màu lấy theo thông tin trên ảnh
                                    </div>
                                    @endif

                                  </div>
                                </div>
                              </div>
                            </li>
                            @endforeach
                          </ul>
                          <div class="wrap_total">
                            <div class="top-subtotal">Tổng tiền: <span class="price" id="total_cart_all">{{ $total }}₫</span></div>
                            <div class="top-subtotal">Phí vận chuyển: 
                              <span class="pricex">Miễn phí</span>
                            </div>
                          </div>
                          <div class="wrap_button">
                            <div class="actions">
                              <a href="{{ url('thanh-toan') }}.html" class="btn btn-gray btn-checkout pink">
                                <span>Tiến hành thanh toán</span>
                              </a>
                            </div>
                          </div>
                          @else
                          <div class="no-item" style="font-size: 16px;">
                            <p>Không có sản phẩm nào trong giỏ hàng</p>
                          </div>
                          @endif
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade login_register login_popup" data-backdrop="static" data-keyboard="false" id="login_register" tabindex="-1" role="dialog"  aria-hidden="true">
  <div class="extend_content">
    <div class="extend_main_content">
      <img src="{{ asset('img/dang-nhap.jpg') }}" alt="Banner">
    </div>
  </div>
  <div class="modal-dialog wrap-modal-login account_popup_main_content" role="document">
    <span class="closed_modal" data-dismiss="modal"><i class="fa fa-times"></i></span>
    <div class="text-xs-center margin-top-90" id="login_popup">
      <div id="login">
        <h4 class="title-modal a-center ">Đăng nhập tài khoản</h4>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-hg-12">
            <form accept-charset='UTF-8' action="" method='post'>
              {{-- {{ csrf_field() }} --}}
              <div class="fw clearfix">
                <fieldset class="form-group">
                  <input type="text" class="form-control form-control-lg" value="{{ old('name') }}" name="name" id="name" placeholder="Nhập tên tài khoản" required>
                </fieldset>
                <fieldset class="form-group">
                  <input type="password" class="form-control form-control-lg" value="" name="password" id="password" placeholder="Nhập mật khẩu" required>
                </fieldset>
                <p class="margin-bottom-15">
                  <a href="#" class="btn-link-style btn-link-style-active" onclick="showRecoverPasswordForm();return false;">Quên mật khẩu ?</a>
                </p>
                <fieldset class="form-group">
                  <button class="btn btn_submit btn-full-width dangNhap" type="submit" value="">Đăng nhập</button>
                </fieldset>
              </div>
            </form>
            <div class="other_ways">
              Chưa có tài khoản <a href="{{ url('dang-ky.html') }}" class="register_btn" title="Đăng ký ngay">Đăng ký ngay</a>
            </div>
          </div>
        </div>
      </div>
      <div id="recover-password" style="display:none;" class="form-signup">
        <h4 class="title-modal a-center">Lấy lại mật khẩu</h4>
        <p>Chúng tôi sẽ gửi thông tin lấy lại mật khẩu vào email đăng ký tài khoản của bạn</p>
        <form accept-charset='UTF-8' action="{{ route('password.email') }}" id='' method='post'>
          @csrf
          <div class="form-signup clearfix">
            <fieldset class="form-group">
              <input type="email" class="form-control form-control-lg" value="" name="email" id="recover-email" placeholder="Nhập email đã đang ký" required>
            </fieldset>
          </div>
          <div class="action_bottom">
            <button class="btn btn-lg btn-width" type="submit" value="Gửi">Gửi</button> hoặc <button class="btn btn-width" onclick="hideRecoverPasswordForm();return false;">Hủy</button>
          </div>
        </form>
      </div>
      <script>
        function showRecoverPasswordForm() {
         document.getElementById('recover-password').style.display = 'block';
         document.getElementById('login').style.display='none';
       }
       function hideRecoverPasswordForm() {
         document.getElementById('recover-password').style.display = 'none';
         document.getElementById('login').style.display = 'block';
       }
       if (window.location.hash == '#recover') {
         showRecoverPasswordForm()
       }
     </script>
   </div>
   {{-- <div class="text-xs-center margin-top-90" id="register_popup" style="display:none;">
    <div id="register">
      <h4 class="title-modal a-center"> Đăng ký tài khoản</h4>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-hg-12">
          <form accept-charset='UTF-8' action='/account/register' id='customer_register' method='post'>
            <input name='FormType' type='hidden' value='customer_register' />
            <input name='utf8' type='hidden' value='true' />
            <div class="fw clearfix">
              <fieldset class="form-group">
                <input type="text" class="form-control form-control-lg" value="" name="firstName" id="firstName"  placeholder="Họ và tên" required>
              </fieldset>
              <fieldset class="form-group">
                <input type="email" class="form-control form-control-lg" value="" name="email" id="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" placeholder="Email" required>
              </fieldset>
              <fieldset class="form-group">
                <input type="password" class="form-control form-control-lg" value="" name="password" id="password" placeholder="Mật khẩu" required>
              </fieldset>
              <fieldset class="form-group">
                <button class="btn btn_submit btn-full-width" onClick="return fsubmit();">Đăng ký</button>
              </fieldset>
            </div>
          </form>
          <div class="other_ways">
            Bạn đã có tài khoản, hãy đăng nhập <a href="#" class="login_btn" title="Đăng nhập">tại đây</a>
          </div>
        </div>
      </div>
    </div>
  </div> --}}
</div>
</div>
<script>
  var firstNamePopUp = document.getElementById('firstName');
  function fsubmit(){
   if (firstNamePopUp.value.length > 50){
    alert('Tên quá dài. Vui lòng đặt tên ngắn hơn 50 ký tự');
    firstNamePopUp.focus();
    firstNamePopUp.select();
    return false;
  }
}

function ftest(){
  if($('#login_popup').css('display') == 'block'){
    register_click();
  } else if ($('#register_popup').css('display') == 'block'){
    login_click();
  }
}

function login_click(){
  $("#login_popup").show();
  $("#recover-password").hide();
  $("#login").show();
  $("#register_popup").hide();
  $('.other_action a').addClass('register_btn').removeClass('login_btn').attr('title','Đăng ký').text('Đăng ký').val('Đăng ký');
}
function register_click(){
  $("#register_popup").show();
  $("#login_popup").hide();
  $('.other_action a').addClass('login_btn').removeClass('register_btn').attr('title','Đăng nhập').text('Đăng nhập').val('Đăng nhập');
}

$('.login_btn').on('click', function(){
  login_click();
});

// $(".register_btn").on('click', function(){
//   register_click();
// });
</script>