@extends('frontend.layout.master')

@section('title','Tony4men - Tin Tức')
@section('keywords','Tony4men - Hàng chất lượng đảm bảo, giá tốt, đẹp như ý')
@section('description','Tony4men - Hàng chất lượng đảm bảo, giá tốt, đẹp như ý')
@section('url',url('/tin-tuc.html'))
@section('titleseo','Tony4men - Hàng chất lượng đảm bảo, giá tốt, đẹp như ý')
@section('type','news')
@section('descriptionseo','Tony4men - Hàng chất lượng đảm bảo, giá tốt, đẹp như ý')
@section('image')

@section('content')
<section class="breadcrumb_background">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="wrap_breadcrumb a-center">
          <h1 class="title-head-page margin-top-0">Tin tức</h1>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="bread-crumb">
  <span class="crumb-border"></span>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 a-left">
        <ul class="breadcrumb" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
          <li class="home">
            <a itemprop="url" href="{{ url('/') }}"><span itemprop="title">Trang chủ</span></a>            
            <span class="mr_lr"> / </span>
          </li>
          <li><strong><span itemprop="title">Tin tức</span></strong></li>
        </ul>
      </div>
    </div>
  </div>
</section>
<div class="container  margin-bottom-50 margin-top-15">
  <div class="row">
    <section class="right-content col-lg-9 col-md-9 col-sm-12 col-xs-12 col-lg-push-3 col-md-push-3">
      <div class="page_title">
        <h1 class="title_page_h1">Tin tức
          <span class="blog_count">({{ count($tintuc) }} Bài viết) </span>
        </h1>
      </div>
      <section class="list-blogs blog-main">
        <div class="row">
          @foreach ($tintuc as $items)
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 item_i">
            <article class="blog-item blog-item-grid row">
              <div class="wrap_blg">
                <div class="blog-item-thumbnail img1 col-lg-4 col-md-4 col-sm-4 col-xs-12">
                  <a href="{{ url('tin-tuc/'.$items->Slug) }}">
                    <picture>
                      <img src="{{ asset('uploads/tintuc/'.$items->Image) }}" style="max-width:100%;" class="img-responsive" alt="{!! $items->TieuDe !!}">
                    </picture>
                  </a>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 content_ar">
                  <h3 class="blog-item-name">
                    <a class="text2line" href="{{ url('tin-tuc/'.$items->Slug) }}">{!! $items->TieuDe !!}</a>
                  </h3>
                  <span class="time_post"><i class="fa fa-clock-o"></i>&nbsp; {!! $items->created_at !!}</span>
                  <div class="summary_ text3line">{!! $items->TomTat !!}</div>
                </div>
              </div>
            </article>
          </div>
          @endforeach
        </div>
        <div class="text-xs-right xs_padding col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <nav>
            <ul class="pagination clearfix">
              {!! $tintuc->links() !!}
            </ul>
          </nav>
        </div>
      </section>
    </section>
    @include('frontend.layout.sidebarTt')
  </div>
</div>
<div class="ab-module-article-mostview"></div>
@endsection