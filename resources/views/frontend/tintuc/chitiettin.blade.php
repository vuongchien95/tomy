@extends('frontend.layout.master')

@section('title','Tony4men - '.$chitiettin->TieuDe)
@section('keywords',$chitiettin->TuKhoaSeo)
@section('description',$chitiettin->MoTaSeo)
@section('url',url('/tin-tuc/'.$chitiettin->Slug))
@section('titleseo',$chitiettin->TieuDeSeo)
@section('type','news')
@section('descriptionseo',$chitiettin->MoTaSeo)
@section('image')

@section('content')
<section class="breadcrumb_background">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="wrap_breadcrumb a-center">
          <h1 class="title-head-page margin-top-0">{!! $chitiettin->TieuDe !!}</h1>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="bread-crumb">
  <span class="crumb-border"></span>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 a-left">
        <ul class="breadcrumb" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
          <li class="home">
            <a itemprop="url" href="/"><span itemprop="title">Trang chủ</span></a>            
            <span class="mr_lr"> / </span>
          </li>
          <li>
            <a itemprop="url" href="{{ url('tin-tuc') }}.html"><span itemprop="title">Tin tức</span></a> 
            <span class="mr_lr"> / </span>
          </li>
          <li><strong><span itemprop="title">{!! $chitiettin->TieuDe !!}</span></strong></li>
        </ul>
      </div>
    </div>
  </div>
</section>
<div class="container article-wraper">
  <div class="row">
    <section class="right-content col-lg-9 col-md-9 col-sm-12 col-xs-12 col-lg-push-3 col-md-push-3">
      <article class="article-main" itemscope="" itemtype="">
        <meta itemprop="mainEntityOfPage" content="{{ url('tin-tuc/'.$chitiettin->Slug) }}">
        <meta itemprop="description" content="{!! $chitiettin->MoTaSeo !!}">
        <meta itemprop="author" content="{!! $chitiettin->User->name !!}">
        
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 content_ar">
            <h1 class="title-head-article"><a href="javascript:void(0)" alt="{!! $chitiettin->TieuDe !!}" title="{!! $chitiettin->TieuDe !!}">{!! $chitiettin->TieuDe !!}</a></h1>
            <span class="time_post"><i class="fa fa-clock-o"></i>&nbsp; {!! $chitiettin->created_at !!}</span><span class="pot">&nbsp;&nbsp;|&nbsp;&nbsp;Đăng bởi: <span class="name_">{!! $chitiettin->User->name !!}</span></span>
            <div class="article-details">
              <div class="article-content">
                <div class="rte">{!! $chitiettin->MoTa !!}</div>
              </div>
            </div>
          </div>
          <div class="tags_share col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="share_row">
            </div>
          </div>
          <div class="blog_other col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="title_module">
              <h2><a href="javascript:void(0)" title="Tin tức khác">Tin tức khác</a></h2>
            </div>
            <ul class="list_blog margin-top-20">
              @foreach ($tintuckhac as $items)
              <li>
                <h3 class="blog-item-name">
                  <a class="text1line" href="{{ url('tin-tuc/'.$items->Slug) }}">{!! $items->TieuDe !!}</a>
                </h3>
              </li>
              @endforeach
            </ul>
          </div>
          <div class="col-xs-12 col-md-12 col-sm-12 col-lg-12">
            <div class="fb-comments" data-href="{{ url('tin-tuc/'.$chitiettin->Slug) }}" data-width="750" data-numposts="5"></div>
          </div>
        </div>
      </article>
    </section>
    @include('frontend.layout.sidebarTt')
  </div>
</div>
<div class="ab-module-article-mostview"></div>
@endsection