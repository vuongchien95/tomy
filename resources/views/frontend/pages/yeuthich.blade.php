@extends('frontend.layout.master')

@section('title','Tony4men - Yêu Thích')
@section('keywords','Tony4men - Hàng chất lượng đảm bảo, giá tốt, đẹp như ý')
@section('description','Tony4men - Hàng chất lượng đảm bảo, giá tốt, đẹp như ý')
@section('url',url('/yeu-thich.html'))
@section('titleseo','Tony4men - Hàng chất lượng đảm bảo, giá tốt, đẹp như ý')
@section('type','product')
@section('descriptionseo','Tony4men - Hàng chất lượng đảm bảo, giá tốt, đẹp như ý')
@section('image')

@section('content')
<div class="wishlist-page-body" style="clear: both">
  @if (isset($sanPham))
  <div class="wishlist-clearfix" style="clear: both">
    <div class="wishlist-search">            
      <input class="wishlist-input" type="text" placeholder="Tìm kiếm sản phẩm...">
    </div>
  </div>
  <div class="wishlist-clearfix" style="clear: both">
    <div class="wishlist-display-style">
      <a class="wishlist-list-style" href="javascript:;" onclick="ABWishList.ChangeListStyle(true);"><i class="wishlist-icon-th-list"></i></a>
      <a class="wishlist-grid-style active" href="javascript:;" onclick="ABWishList.ChangeListStyle();"><i class="wishlist-icon-th"></i></a>
    </div>
  </div>
  @endif
  <div class="wishlist-view wishlist-grid-view">
    <div class="wishlist-view-page" id="loadYT">
      @if (isset($sanPham))
      @foreach ($sanPham as $items)
      <div class="wishlist-product">
        <div class="wishlist-product-img">
          <a href="{{ url('san-pham/'.$items[0]->Slug) }}">
            <img class="" src="{{ asset('uploads/sanpham/'.$items[0]->Image) }}">
          </a>
        </div>
        <div class="wishlist-product-content">
          <a class="wishlist-product-name" href="{{ url('san-pham/'.$items[0]->Slug) }}">
            <p style="color: #46484a; font-size: 15px;">{{ $items[0]->Name }}</p>
            <p style="font-size: 15px; margin: 15px 0 0 !important">
              <a href="javascript:;" onclick="XoaYT({{ $items[0]->id }},{{ Auth::user()->id }})" class="" title="Xóa" id="data-id" data-id="">
                <i class="fa fa-close"></i>&nbsp;&nbsp;Xoá Sp
              </a>
            </p>
          </a>
          @if ($items[0]->GiaSale)
          <p style="color: #357ebd; font-size: 17px;">
            <span class="wishlist-product-price money">{{ number_format($items[0]->GiaSale,0,',','.') }}₫</span>
            <span class="wishlist-product-oldprice money">{{ number_format($items[0]->Gia,0,',','.') }}₫</span>
          </p>
          @else
          <p style="color: #357ebd; font-size: 17px;">
            <span class="wishlist-product-price money">{{ number_format($items[0]->Gia,0,',','.') }}₫</span>
          </p>
          @endif
        </div>
        <div class="wishlist-product-action wishlist-clearfix">
          <div class="wishlist-add-to-cart">
            <form action="" method="post" enctype="multipart/form-data">
              <button class="wishlist-btn wishlist-product-btn muahang" type="button" id="data-id" data-id="{{ $items[0]->id }}" title="Thêm vào giỏ hàng">
                Thêm vào giỏ hàng
              </button>
            </form>
          </div>
          <div class="wishlist-remove hidden">
            <a class="wishlist-a-tag wishlist-remove-link" href="javascript:;" style="color: #639cd4">Xóa sản phẩm</a>
          </div>
        </div>
      </div>
      @endforeach
      @else
      <div style="color: #5a6263ab;font-size: 21px;text-align: center;font-weight: 600;">
        Không có sản phẩm nào trong danh sách yêu thích của bạn !!
      </div>
      @endif
      
    </div>
  </div>
  <div id="wishlist_loading_more" style="text-align:center; display: none;">
    <img src="https://wishlists.bizwebapps.vn/Themes/Portal/Default/Images/loading_spinner.gif">
  </div>
  <div class="wishlist-load-more wishlist-clearfix" id="wishlist_loadmore" data-wishlist-page="1" data-wishlist-totalpage="1">
    <button class="wishlist-btn wishlist-load-more-btn" type="button" onclick="ABWishList.LoadMore()" style="display: none;">Tải thêm</button>
  </div>
</div>
<script src="{{ asset('frontend/js/debounce.min.js') }}"></script>
<script src="{{ asset('frontend/js/ab_wishlist_script.js') }}"></script>
@endsection
@section('script')
<script type="text/javascript">
  function XoaYT(idSp,idUser){
    alert('Xóa sản phẩm này khỏi danh sách yêu thích của bạn ?')
    getUrl = $(location).attr('href');
    $.get('/xoasp-yeu-thich/'+idSp+'/'+idUser,function(data){
      $('#loadYT').load( getUrl+" #loadYT" );
    });
  }
</script>
@endsection