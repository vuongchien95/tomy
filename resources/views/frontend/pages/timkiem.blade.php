@extends('frontend.layout.master')

@section('title','Tony4men - Tìm Kiếm Sản Phẩm')
@section('keywords','Tony4men - Hàng chất lượng đảm bảo, giá tốt, đẹp như ý')
@section('description','Tony4men - Hàng chất lượng đảm bảo, giá tốt, đẹp như ý')
@section('url',url('/tim-kiem.html'))
@section('titleseo','Tony4men - Hàng chất lượng đảm bảo, giá tốt, đẹp như ý')
@section('type','product')
@section('descriptionseo','Tony4men - Hàng chất lượng đảm bảo, giá tốt, đẹp như ý')
@section('image')

@section('content')
<section class="breadcrumb_background">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="wrap_breadcrumb a-center">
          <h1 class="title-head-page margin-top-0"> Tìm Kiếm </h1>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="bread-crumb">
  <span class="crumb-border"></span>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 a-left">
        <ul class="breadcrumb" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
          <li class="home">
            <a itemprop="url" href="{{ url('/') }}"><span itemprop="title">Trang chủ</span></a>            
            <span class="mr_lr"> / </span>
          </li>
          <li><strong><span itemprop="title">Tìm kiếm</span></strong></li>
        </ul>
      </div>
    </div>
  </div>
</section>
<section class="signup search-main margin-top-30">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <h2 class="title-head margin-bottom-20">Tìm thấy {{ count($sanPham) }} Sp/Trang cho từ khóa "<span style="    color: red;font-weight: bold;"> {{ $search }} </span>"</h2>
      </div>
      <div class="products-view-grid products cls_search">
        <div class="row-gutter-14">
          @foreach ($sanPham as $items)
          <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3 product-col">
            <div class="product-box">
              <div class="product-thumbnail">
                <a href="{{ url('san-pham/'.$items->Slug) }}" class="image_link display_flex" data-images="{{ asset('uploads/sanpham/'.$items->Image) }}" title="{{ $items->Name }}">
                  <img src="{{ asset('uploads/sanpham/'.$items->Image) }}" alt="{{ $items->Name }}">
                </a>
                <div class="product-action-grid clearfix">
                  <form action="" method="post" class="variants form-nut-grid" data-id="" enctype="multipart/form-data">
                    <div>
                      <a title="xem nhanh" href="javascript:;" id="data-id" data-id="{{ $items->id }}" class="button_wh_40 btn_view right-to quick-view">
                        <i class="fa fa-search"></i>
                      </a>
                      <a href="javascript:;" id="data-id" data-id="{{ $items->id }}" class="button_wh_40 btn-cart left-to muahang" title="Mua hàng"><span>Mua hàng</span>
                      </a>
                      @if (Auth::check())
                      <a title="Yêu thích" id="data-id" data-id="{{ $items->id }}" ok="{{ Auth::user()->id }}" class="button_wh_40 iWishAdd iwishAddWrapper" href="javascript:;" >
                        <i class="fa fa-heart-o"></i>
                      </a>
                      @else
                      <a title="Yêu thích" class="button_wh_40 iWishAdda iwishAddWrapper" href="javascript:;" ><i class="fa fa-heart-o"></i>
                      </a>
                      @endif
                    </div>
                  </form>
                </div>
              </div>
              <div class="product-info a-center">
                <h3 class="product-name">
                  <a class="text2line" href="{{ url('san-pham/'.$items->Slug) }}" title="{{ $items->Name }}">{{ $items->Name }}</a>
                </h3>
                <div class="price-box clearfix">
                  @if ($items->GiaSale)
                  <span class="price product-price">{{ number_format($items->GiaSale,0,',','.') }}₫</span>
                  <span class="price product-price-old">
                    {{ number_format($items->Gia,0,',','.') }}₫     
                  </span>
                  @else
                  <span class="price product-price">{{ number_format($items->Gia,0,',','.') }}₫</span>
                  @endif   
                </div>
              </div>
            </div>
          </div>
          @endforeach
        </div>
      </div>
      <div class="col-xs-12 text-xs-center margin-bottom-30">
        <nav>
          <ul class="pagination clearfix">
            {!! $sanPham->links() !!}
          </ul>
        </nav>
      </div>
    </div>
  </div>
</section>
<div class="bizweb-product-reviews-module"></div>
@endsection