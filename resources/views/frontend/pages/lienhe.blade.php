@extends('frontend.layout.master')

@section('title','Tony4men - Liên Hệ')
@section('keywords')
@section('description')
@section('url',url('/lien-he.html'))
@section('titleseo','Tony4men - Liên Hệ')
@section('type','product')
@section('descriptionseo')
@section('image')

@section('content')
<section class="breadcrumb_background">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="wrap_breadcrumb a-center">
          <h1 class="title-head-page margin-top-0">Liên hệ</h1>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="bread-crumb">
  <span class="crumb-border"></span>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 a-left">
        <ul class="breadcrumb" itemscope="" itemtype="">         
          <li class="home">
            <a itemprop="url" href="{{ url('/') }}"><span itemprop="title">Trang chủ</span></a>            
            <span class="mr_lr"> / </span>
          </li>
          <li><strong><span itemprop="title">Liên hệ</span></strong></li>
        </ul>
      </div>
    </div>
  </div>
</section>
<section class="page">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="title_head margin-top-20 margin-bottom-20">
          <h1 class="title_center_page left">
            <span class="background_">Hệ thống cửa hàng:</span>
          </h1>
        </div>
        <div class="content-page rte margin-bottom-50">
          <div class="row">
            <div class="select_maps col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <div class="wrap_padding">
                <div class="select_daily city-selector">
                  <select name="showroom_city_id" class="showroom_city_id">
                    <option label="Hà Nội" value="1">Hà Nội</option>
                    <option label="Hải Phòng" value="2">Hải Phòng</option>
                    <option label="TP. Hồ Chí Minh" value="3">TP. Hồ Chí Minh</option>
                  </select>
                </div>
                <div class="city_1 city-wrapper" style="display: block;">
                  <div class="nano has-scrollbar">
                    <div class="content">
                      <div class="showroom-item active" lat="21.037733" lon="105.811758">
                        <div class="title_addr"><span>{!! $lienhe->TenCty !!}</span></div>
                        <span class="showroom-address"><i class="fa fa-map-marker"></i> <span>{!! $lienhe->DiaChi !!}</span></span>
                        <span class="showroom-contact"><i class="fa fa-phone"></i> <span>
                          <a href="tel:{!! $lienhe->Sdt !!}">(+84) {!! $lienhe->Sdt !!} -  (+84) {!! $lienhe->Hotline !!}</a>
                        </span>
                      </span>
                      <span class="showroom-contact"><i class="fa fa-clock-o "></i> <span>Người đại diện: {!! $lienhe->NgDaiDien !!}</span></span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="box-maps col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <fieldset class="gllpLatlonPicker" id="_MAP_5158">
              {!! $lienhe->Map !!}
            </fieldset>
          </div>
        </div>
        <div class="contact margin-top-20">
          <div class="row">
            <div class="info_width margin-top-40">
              <div class="page-login page_cotact">
                <h2 class="a-left title_monster margin-bottom-15"><span>Liên hệ với chúng tôi</span></h2>
                <div id="pagelogin">
                  <form accept-charset="UTF-8" action="/contact" id="contact" method="post">
                    <input name="FormType" type="hidden" value="contact">
                    <input name="utf8" type="hidden" value="true">                    <div class="form-signup clearfix">
                      <div class="group_contact">
                        <fieldset class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                          <label class="hidden">Họ và tên:</label>
                          <input type="text" name="contact[name]" id="name" placeholder="Họ và tên:*" class="form-control  form-control-lg" required="">
                        </fieldset>
                        <fieldset class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                          <label class="hidden">Email:</label>
                          <input type="email" name="contact[email]" placeholder="Email:*" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,63}$" data-validation="email" id="email" class="form-control form-control-lg" required="">
                        </fieldset>
                        <fieldset class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">  
                          <label class="hidden">Số điện thoại:</label>
                          <input type="text" pattern="\d+" placeholder="Số điện thoại:" class="form-control form-control-comment form-control-lg" name="Phone" required="">
                        </fieldset>
                        <fieldset class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <label class="hidden">Bình luận:</label>
                          <textarea name="contact[body]" id="comment" placeholder="Bình luận:*" class="form-control content-area form-control-lg" rows="5" required=""></textarea>

                        </fieldset>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 a-left">
                          <button type="submit" class="btn-45-no-radius btn">Gửi liên hệ</button>
                        </div>

                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</section>
<div class="bizweb-product-reviews-module"></div>
<style>
.hasborder {
  padding-bottom: 60px;
  border-bottom:solid 1px #ebebeb;
}
.google-map {width:100%;}
.google-map .map {width:100%; height:400px; background:#dedede}
.gllpLatlonPicker {border:none;padding:0px; border: solid 10px #ebebeb;}
.gllpMap {
  width: 100%;
  height: 450px;
  border: solid 10px #fff;
}
</style>
@endsection