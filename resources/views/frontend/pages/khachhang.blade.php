@extends('frontend.layout.master')
@section('content')
<section class="breadcrumb_background">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="wrap_breadcrumb a-center">
          <h1 class="title-head-page margin-top-0">Trang khách hàng</h1>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="bread-crumb">
  <span class="crumb-border"></span>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 a-left">
        <ul class="breadcrumb" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
          <li class="home">
            <a itemprop="url" href="/"><span itemprop="title">Trang chủ</span></a>            
            <span class="mr_lr"> / </span>
          </li>
          <li><strong><span itemprop="title">Trang khách hàng</span></strong></li>
        </ul>
      </div>
    </div>
  </div>
</section>
<section class="signup page_customer_account margin-top-20">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-main-acount">
        <div id="parent" class="row">
          <div id="a" class="col-xs-12 col-sm-12 col-lg-9 col-left-account">
            <div class="title_head margin-bottom-20 m992">
              <h1 class="title_center_page ">
                <span>Trang khách hàng</span>
              </h1>
            </div>
            <div class="form-signup name-account m992">
              <p><strong>Xin chào, <a href="/account/addresses" style="color:#f8452d;"> Taylor</a>&nbsp;!</strong></p>
            </div>
            <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
              <div class="my-account">
                <div class="dashboard">
                  <div class="recent-orders">
                    <div class="table-responsive tab-all" style="overflow-x:auto;">
                      <table class="table table-cart" id="my-orders-table">
                        <thead class="thead-default">
                          <tr>
                            <th>Đơn hàng</th>
                            <th>Ngày</th>
                            <th>Chuyển đến</th>
                            <th>Địa chỉ</th>
                            <th>Giá trị đơn hàng</th>
                            <th>Tình trạng thanh toán</th>
                            <th>Mã đơn hàng</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr class="first odd">
                            <td><a style="color:#f8452d;" href="/account/orders/4618972">Chi tiết</a></td>
                            <td>24/ 06/ 2018</td>
                            <td>  Taylor</td>
                            <td> 
                              Đà Nẵng 
                            </td>
                            <td><span class="price">398.000₫</span></td>
                            <td>  
                              Chưa thanh toán          
                            </td>
                            <td class="a-center last"><span class="nobr"> <a style="color:#f8452d;" href="/account/orders/4618972">#1015</a></span></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="text-xs-right">
                    </div>
                  </div>
                  <div class="paginate-pages pull-right page-account">
                    <nav>
                      <ul class="pagination clearfix">
                        <li class="page-item disabled"><a class="page_button" href="#"><i class="fa fa-angle-left " aria-hidden="true"></i></a></li>
                        <li class="active page-item disabled"><a class="page-link" href="#">1</a></li>
                        <li class="page-item disabled"><a class="page_button" href="#"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                      </ul>
                    </nav>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div id="b" class="col-xs-12 col-sm-12 col-lg-3 col-right-account">
            <div class="title_head margin-bottom-20 mx991">
              <hh2 class="title_center_page">
                <span>Trang khách hàng</span>
              </hh2>
            </div>
            <div class="form-signup body_right mx991">
              <p><strong>Xin chào, <a href="/account/addresses" style="color:#f8452d;"> Taylor</a>&nbsp;!</strong></p>
            </div>
            <div class="block-account">
              <div class="block-title-account">
                <h5>Tài khoản của tôi</h5>
              </div>
              <div class="block-content form-signup">
                <p>Tên tài khoản: <strong style="line-height: 20px;">  Taylor!</strong></p>
                <p><i class="fa fa-home font-some" aria-hidden="true"></i>  <span>Địa chỉ: Đà Nẵng</span></p>
                <p><i class="fa fa-mobile font-some" aria-hidden="true"></i> <span>Điện thoại: 2926511648</span> </p>
                <p><i class="fa fa-map-marker font-some" aria-hidden="true"></i> <span> Địa chỉ 1: A/P AWASARI KHURD ,BHORMALA</span></p>
                <p><i class="fa fa-yelp font-some" aria-hidden="true"></i> <span> Công ty: </span></p>
                <p><i class="fa fa-plane font-some" aria-hidden="true"></i> <span> Quốc gia :Việt Nam</span></p>
                <p><i class="fa fa-code font-some" aria-hidden="true"></i> <span> Zip code: </span></p>
                <p style="margin-top:20px;"><a href="/account/addresses" class="btn btn-bg full-width btn-lg btn-style article-readmore">Sổ địa chỉ 1</a></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection