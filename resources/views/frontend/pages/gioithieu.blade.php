@extends('frontend.layout.master')

@section('title','Tony4men - Giới Thiệu')
@section('keywords')
@section('description')
@section('url',url('/gioi-thieu.html'))
@section('titleseo','Tony4men - Giới Thiệu')
@section('type','product')
@section('descriptionseo')
@section('image')

@section('content')
<section class="breadcrumb_background">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="wrap_breadcrumb a-center">
          <h1 class="title-head-page margin-top-0">Giới thiệu</h1>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="bread-crumb">
  <span class="crumb-border"></span>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 a-left">
        <ul class="breadcrumb" itemscope="" itemtype="">         
          <li class="home">
            <a itemprop="url" href="{{ url('/') }}"><span itemprop="title">Trang chủ</span></a>            
            <span class="mr_lr"> / </span>
          </li>
          <li><strong><span itemprop="title">Giới thiệu</span></strong></li>
        </ul>
      </div>
    </div>
  </div>
</section>
<section class="page">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 margin-bottom-50">
        {!! $gioithieu->GioiThieu !!}
      </div>
    </div>
  </div>
</section>
<div class="bizweb-product-reviews-module"></div>
@endsection