@extends('frontend.layout.master')

@section('title','Tony4men - Đăng Nhập')
@section('keywords')
@section('description')
@section('url',url('/dang-nhap.html'))
@section('titleseo','Tony4men - Đăng Nhập')
@section('type','product')
@section('descriptionseo')
@section('image')

@section('content')
<section class="breadcrumb_background">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="wrap_breadcrumb a-center">
          <h1 class="title-head-page margin-top-0">Đăng nhập tài khoản</h1>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="bread-crumb">
  <span class="crumb-border"></span>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 a-left">
        <ul class="breadcrumb" itemscope="" itemtype="">         
          <li class="home">
            <a itemprop="url" href="{{ url('/') }}"><span itemprop="title">Trang chủ</span></a>            
            <span class="mr_lr"> / </span>
          </li>
          <li><strong><span itemprop="title">Đăng nhập tài khoản</span></strong></li>
        </ul>
      </div>
    </div>
  </div>
</section>
<div class="container margin-top-30">
  <div class="title_head">
    <h4 class="title_center_page">
      <span>Đăng nhập tài khoản</span>
    </h4>
  </div>

  <div class="row">
    <div class="col-md-4"> </div>
    @if (Session::has('erro'))
    <div class="alert alert-danger col-md-4 text-center" style="margin-top: 1em;border-radius: 5px">
      <ul style="list-style: none">
        <li><i class="ace-icon fa fa-exclamation-triangle"></i> {{Session::get('erro')}} </li>
      </ul>
    </div>
    @endif
    @if (session('status'))
    <div class="alert alert-success col-md-4 text-center" style="margin-top: 1em;border-radius: 5px">
      <ul style="list-style: none">
        <li> {{ session('status') }} </li>
      </ul>
    </div>
    @endif
    <div class="col-md-4"> </div>
  </div>

  <div class="row">
    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 col-lg-offset-4 col-md-offset-3 col-sm-offset-3 ">
      <div class="page-login">
        <div id="login">
          <form accept-charset="UTF-8" action="{{ url('post-dang-nhap.html') }}" id="" method="post">
            {{ csrf_field() }}
            <div class="form-signup clearfix">
              <div class="row">
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                  <fieldset class="form-group">
                    <input type="text" class="form-control form-control-lg"  value="{{ old('name') }}" name="name" id="name" placeholder="Nhập tên tài khoản" required="">
                  </fieldset>
                  <fieldset class="form-group">
                    <input type="password" class="form-control form-control-lg" value="" name="password" id="password" placeholder="Mật khẩu" required="">
                  </fieldset>
                  @if (!Auth::check())
                    <span class="recv-text"><a id="rcv-pass">Quên mật khẩu?</a></span>
                  @endif
                </div>
              </div>
              <div class="col-xs-12 text-xs-left margin-bottom-15" style="margin-top:20px; padding: 0">
                <button type="submit" value="Đăng nhập" class="btn button-50 btn-50-blue width_100">Đăng nhập</button>
              </div>
              <div class="fot_sigup hidden">
                <span class="tit margin-top-15"><span>Hoặc đăng nhập</span></span>
              </div>
              <div class="social_login"></div>
              <span class="have_ac">Chưa có tài khoản đăng ký <a href="{{ url('dang-ky.html') }}">tại đây</a></span>
            </div>
          </form>
        </div>
        <div id="recover-password" class="rcv">
          <div class="form-signup clearfix" style="margin-top:0px;">
            <div class="recover">
              <form accept-charset="UTF-8" action="{{ route('password.email') }}" id="recover_customer_password" method="post">
                @csrf
                <div class="form-signup aaaaaaaa erorr_page a-center" style="margin-top:0px;"></div>
                <div class="form_recover_" style="display:none;">
                  <div class="form-signup clearfix">
                    <fieldset class="form-group">
                      <input type="email" class="form-control form-control-lg" value="" name="email" id="email" placeholder="Nhập Email để lấy lại mật khẩu" required="">
                    </fieldset>
                  </div>
                  <div class="action_bottom">
                    <button type="summit" value="Lấy lại mật khẩu" class="btn button-50 width_100 btn-50-blue" style="margin-top:20px;">Lấy lại mật khẩu</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection