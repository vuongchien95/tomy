@extends('frontend.layout.master')

@if (isset($cauhinh))
@section('title',$cauhinh->TieuDe)
@section('keywords',$cauhinh->TuKhoa)
@section('description',$cauhinh->MoTa)
@section('url',url(''))
@section('titleseo',$cauhinh->MoTa)
@section('type','product')
@section('descriptionseo')
@section('image')
@endif

@section('content')
<section class="awe-section-1">
  <section class="slide_index">
    <div class="slider-wrapper theme-default">
      <div id="slider" class="nivoSlider">
        @foreach ($banner as $items)
        <img src="{{ asset('uploads/images/banner/'.$items->Image) }}"   alt="Banner Tony4men">
        @endforeach
      </div>
    </div>
  </section>
</section>
<section class="awe-section-2">
  <section class="section_service">
    <div class="container">
      <div class="row">
        <div class="wrap_item_srv owl-carousel" data-lg-items="3" data-md-items="3" data-xs-items="1" data-sm-items="2" data-margin="15">
          @foreach ($chinhsach as $items)
          <div class="col-item-srv col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="service_item_ed a">
              <span class="iconx">
                <img alt="{!! $items->TenChinhSach !!}" title="{!! $items->TenChinhSach !!}" src="{{ asset('uploads/chinhsach/'.$items->Image) }}" style="width: 54px;height: 47px" />
              </span>
              <div class="content_srv">
                <span class="title_service">{!! $items->TenChinhSach !!}</span>
                <span class="content_service">{!! $items->MoTa !!}</span>
              </div>
            </div>
          </div>
          @endforeach
        </div>
      </div>
    </div>
  </section>
</section>
<section class="awe-section-3">
  <section class="section_product_sale_owl">
    <div class="container">
      @if (count($sanphamkhuyenmai) > 0)
      <div class="row">
        <div class="title_section_center ">
          <h2 class="title cooktail_">
            <a class="a-position" href="javascript:void(0)" title="Sản phẩm khuyến mãi"> Sản Phẩm Khuyến Mãi </a>
          </h2>
        </div>
        <div class="border_wrap col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="owl_product_comback ">
            <div class="product_comeback_wrap">
              <div class="owl_product_item_content owl-carousel" data-loop="true" data-dot="false" data-nav='true' data-lg-items='4' data-md-items='3' data-sm-items='3' data-xs-items="1" data-margin='0'>
                @foreach ($sanphamkhuyenmai as $items)
                <div class="item saler_item col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="owl_item_product product-col">
                    <div class="product-box">
                      <div class="product-thumbnail">
                        <a href="{{ url('san-pham/'.$items->Slug) }}" class="image_link display_flex" data-images=""  title="{{ $items->Name }}">
                          <img src="{{ asset('uploads/sanpham/'.$items->Image) }}" alt="{{ $items->Name }}" title="{{ $items->Name }}">
                        </a>
                        <div class="product-action-grid clearfix form-nut-grid">
                          <form action="" method="post" class="form-nut-grid" data-id="" enctype="multipart/form-data">
                            <div>
                              <a title="xem nhanh" href="javascript:;" id="data-id" data-id="{{ $items->id }}" class="button_wh_40 btn_view right-to quick-view">
                                <i class="fa fa-search"></i>
                              </a>
                              <a href="javascript:;" id="data-id" data-id="{{ $items->id }}" class="button_wh_40 btn-cart left-to muahang" title="Mua hàng"><span>Mua hàng</span>
                              </a>
                              @if (Auth::check())
                                <a title="Yêu thích" id="data-id" data-id="{{ $items->id }}" ok="{{ Auth::user()->id }}" class="button_wh_40 iWishAdd iwishAddWrapper" href="javascript:;" >
                                  <i class="fa fa-heart-o"></i>
                                </a>
                              @else
                                <a title="Yêu thích" class="button_wh_40 iWishAdda iwishAddWrapper" href="javascript:;" ><i class="fa fa-heart-o"></i>
                                </a>
                              @endif
                            </div>
                          </form>
                        </div>
                      </div>
                      <div class="product-info a-center">
                        <div class="reviews-product-grid">
                          <div class="bizweb-product-reviews-badge" data-id="8989905"></div>
                        </div>
                        <h3 class="product-name">
                          <a class="text2line" href="{{ url('san-pham/'.$items->Slug) }}" alt="{!! $items->Name !!}" title="{{ $items->Name }}">{{ $items->Name }}
                          </a>
                        </h3>
                        <div class="price-box clearfix">      
                          <span class="price product-price">{{ number_format($items->GiaSale,0,',','.') }}₫</span>
                          <span class="price product-price-old">
                            {{ number_format($items->Gia,0,',','.') }}₫     
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                @endforeach
              </div>
            </div>
          </div>
        </div>
      </div>
      @endif
    </div>
  </section>
</section>
<section class="awe-section-4">
  <section class="section_banner_index ">
    <div class="container">
      <div class="row">
        <div class="wrap_banner">
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <a class="img1" href="javascript:void(0)" title="tony4men">
              <img alt="Banner 1" src="{{ asset('img/banner-1.jpg') }}" style="width: 570px;height: 195px" />
            </a>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <a class="img1" href="javascript:void(0)" title="tony4men">
              <img alt="Banner 2" src="{{ asset('img/banner-2.jpg') }}" style="width: 570px;height: 195px" />
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>
</section>
<section class="awe-section-5">
  <section class="section_bestSale">
    <div class="container">
      <div class="title_section_center mr_0">
        <h2 class="title drink"><span> Sản Phẩm Bán Chạy Nhất Tuần </span></h2>
      </div>
      <div class="tab_link_module">
        <div class="tabs-container tab_border">
          <span class="hidden-md hidden-lg button_show_tab">
            <i class="fa fa-angle-down"></i>
          </span>
          <span class="hidden-md hidden-lg title_check_tabs"></span>
          <div class="clearfix">
            <ul class="ul_link link_tab_check_click container closeit">
              @php
                $i = 0;
              @endphp
              @foreach ($danhmuccha as $items)
              @php
                $i++;
              @endphp
                <li class="li_tab">
                  <a href="#content-tabb{{ $i }}" class="head-tabs head-tab{{ $i }}" data-src=".head-tab{{ $i }}" title="{!! $items->Name !!}">{!! $items->Name !!}</a>
                </li>
              @endforeach
            </ul>
          </div>
        </div>
        <div class="tabs-content">
          <div id="content-tabb1" class="content-tab content-tab-proindex" style="display:none">
            <div class="clearfix wrap_item_list row">
              <div class="owl_mobile owl-carousel not-dqowl">
                @if (count($aonam) > 0)
                @foreach ($aonam as $items)
                <div class="col-xxs col-xs-12 col-md-3 col-sm-12 col-lg-3">
                  <div class="wrp_item_small product-col">
                    <div class="product-box">
                      <div class="product-thumbnail">
                        <a href="{{ url('san-pham/'.$items->Slug) }}" class="image_link display_flex" data-images="" title="{!! $items->Name !!}">
                          <img src="{{ asset('uploads/sanpham/'.$items->Image) }}" title="{!! $items->Name !!}" alt="{!! $items->Name !!}">
                        </a>
                        <div class="product-action-grid clearfix">
                          <form action="" method="post" class="variants form-nut-grid" enctype="multipart/form-data">
                            <div>
                              <a title="xem nhanh" href="javascript:;" id="data-id" data-id="{{ $items->id }}" class="button_wh_40 btn_view right-to quick-view">
                                <i class="fa fa-search"></i>
                              </a>
                              <a href="javascript:;" id="data-id" data-id="{{ $items->id }}" class="button_wh_40 btn-cart left-to muahang" title="Mua hàng"><span>Mua hàng</span>
                              </a>
                              @if (Auth::check())
                                <a title="Yêu thích" id="data-id" data-id="{{ $items->id }}" ok="{{ Auth::user()->id }}" class="button_wh_40 iWishAdd iwishAddWrapper" href="javascript:;" >
                                  <i class="fa fa-heart-o"></i>
                                </a>
                              @else
                                <a title="Yêu thích" class="button_wh_40 iWishAdda iwishAddWrapper" href="javascript:;" ><i class="fa fa-heart-o"></i>
                                </a>
                              @endif
                            </div>
                          </form>
                        </div>
                      </div>
                      <div class="product-info a-center">
                        <div class="reviews-product-grid">
                          <div class="bizweb-product-reviews-badge" data-id="8988858"></div>
                        </div>
                        <h3 class="product-name">
                          <a class="text2line" href="{{ url('san-pham/'.$items->Slug) }}" alt="{!! $items->Name !!}" title="{!! $items->Name !!}">{!! $items->Name !!}
                          </a>
                        </h3>
                        <div class="price-box clearfix">
                          <span class="price product-price">{!! isset($items->GiaSale) ? number_format($items->GiaSale,0,',','.') : number_format($items->Gia,0,',','.') !!}₫</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                @endforeach
                @else
                <div class="col-lg-12 col-md-12 block_width"><span class="warning">Đang cập nhật sản phẩm</span></div>
                @endif
              </div>
            </div>
          </div>
          <div id="content-tabb2" class="content-tab content-tab-proindex" style="display:none">
            <div class="clearfix wrap_item_list row">
              <div class="owl_mobile owl-carousel not-dqowl">
                @if (count($quannam) > 0)
                @foreach ($quannam as $items)
                <div class="col-xxs col-xs-12 col-md-3 col-sm-12 col-lg-3">
                  <div class="wrp_item_small product-col">
                    <div class="product-box">
                      <div class="product-thumbnail">
                        <a href="{{ url('san-pham/'.$items->Slug) }}" class="image_link display_flex" data-images="" title="{!! $items->Name !!}">
                          <img src="{{ asset('uploads/sanpham/'.$items->Image) }}" title="{!! $items->Name !!}" alt="{!! $items->Name !!}">
                        </a>
                        <div class="product-action-grid clearfix">
                          <form action="" method="post" class="variants form-nut-grid" enctype="multipart/form-data">
                            <div>
                              <a title="xem nhanh" href="javascript:;" id="data-id" data-id="{{ $items->id }}" class="button_wh_40 btn_view right-to quick-view">
                                <i class="fa fa-search"></i>
                              </a>
                              <a href="javascript:;" id="data-id" data-id="{{ $items->id }}" class="button_wh_40 btn-cart left-to muahang" title="Mua hàng"><span>Mua hàng</span>
                              </a>
                              @if (Auth::check())
                                <a title="Yêu thích" id="data-id" data-id="{{ $items->id }}" ok="{{ Auth::user()->id }}" class="button_wh_40 iWishAdd iwishAddWrapper" href="javascript:;" >
                                  <i class="fa fa-heart-o"></i>
                                </a>
                              @else
                                <a title="Yêu thích" class="button_wh_40 iWishAdda iwishAddWrapper" href="javascript:;" ><i class="fa fa-heart-o"></i>
                                </a>
                              @endif
                            </div>
                          </form>
                        </div>
                      </div>
                      <div class="product-info a-center">
                        <div class="reviews-product-grid">
                          <div class="bizweb-product-reviews-badge" data-id="8988858"></div>
                        </div>
                        <h3 class="product-name">
                          <a class="text2line" href="{{ url('san-pham/'.$items->Slug) }}" alt="{!! $items->Name !!}" title="{!! $items->Name !!}">{!! $items->Name !!}
                          </a>
                        </h3>
                        <div class="price-box clearfix">
                          <span class="price product-price">{!! isset($items->GiaSale) ? number_format($items->GiaSale,0,',','.') : number_format($items->Gia,0,',','.') !!}₫</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                @endforeach
                @else
                <div class="col-lg-12 col-md-12 block_width"><span class="warning">Đang cập nhật sản phẩm</span></div>
                @endif
              </div>
            </div>
          </div>
          <div id="content-tabb3" class="content-tab content-tab-proindex" style="display:none">
            <div class="clearfix wrap_item_list row">
              <div class="owl_mobile owl-carousel not-dqowl">
                @if (count($giaydep) > 0)
                @foreach ($giaydep as $items)
                <div class="col-xxs col-xs-12 col-md-3 col-sm-12 col-lg-3">
                  <div class="wrp_item_small product-col">
                    <div class="product-box">
                      <div class="product-thumbnail">
                        <a href="{{ url('san-pham/'.$items->Slug) }}" class="image_link display_flex" data-images="" title="{!! $items->Name !!}">
                          <img src="{{ asset('uploads/sanpham/'.$items->Image) }}" title="{!! $items->Name !!}" alt="{!! $items->Name !!}">
                        </a>
                        <div class="product-action-grid clearfix">
                          <form action="" method="post" class="variants form-nut-grid" enctype="multipart/form-data">
                            <div>
                              <a title="xem nhanh" href="javascript:;" id="data-id" data-id="{{ $items->id }}" class="button_wh_40 btn_view right-to quick-view">
                                <i class="fa fa-search"></i>
                              </a>
                              <a href="javascript:;" id="data-id" data-id="{{ $items->id }}" class="button_wh_40 btn-cart left-to muahang" title="Mua hàng"><span>Mua hàng</span>
                              </a>
                              @if (Auth::check())
                                <a title="Yêu thích" id="data-id" data-id="{{ $items->id }}" ok="{{ Auth::user()->id }}" class="button_wh_40 iWishAdd iwishAddWrapper" href="javascript:;" >
                                  <i class="fa fa-heart-o"></i>
                                </a>
                              @else
                                <a title="Yêu thích" class="button_wh_40 iWishAdda iwishAddWrapper" href="javascript:;" ><i class="fa fa-heart-o"></i>
                                </a>
                              @endif
                            </div>
                          </form>
                        </div>
                      </div>
                      <div class="product-info a-center">
                        <div class="reviews-product-grid">
                          <div class="bizweb-product-reviews-badge" data-id="8988858"></div>
                        </div>
                        <h3 class="product-name">
                          <a class="text2line" href="{{ url('san-pham/'.$items->Slug) }}" alt="{!! $items->Name !!}" title="{!! $items->Name !!}">{!! $items->Name !!}
                          </a>
                        </h3>
                        <div class="price-box clearfix">
                          <span class="price product-price">{!! isset($items->GiaSale) ? number_format($items->GiaSale,0,',','.') : number_format($items->Gia,0,',','.') !!}₫</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                @endforeach
                @else
                <div class="col-lg-12 col-md-12 block_width"><span class="warning">Đang cập nhật sản phẩm</span></div>
                @endif
              </div>
            </div>
          </div>
          <div id="content-tabb4" class="content-tab content-tab-proindex" style="display:none">
            <div class="clearfix wrap_item_list row">
              <div class="owl_mobile owl-carousel not-dqowl">
                @if (count($baloda) > 0)
                @foreach ($baloda as $items)
                <div class="col-xxs col-xs-12 col-md-3 col-sm-12 col-lg-3">
                  <div class="wrp_item_small product-col">
                    <div class="product-box">
                      <div class="product-thumbnail">
                        <a href="{{ url('san-pham/'.$items->Slug) }}" class="image_link display_flex" data-images="" title="{!! $items->Name !!}">
                          <img src="{{ asset('uploads/sanpham/'.$items->Image) }}" title="{!! $items->Name !!}" alt="{!! $items->Name !!}">
                        </a>
                        <div class="product-action-grid clearfix">
                          <form action="" method="post" class="variants form-nut-grid" enctype="multipart/form-data">
                            <div>
                              <a title="xem nhanh" href="javascript:;" id="data-id" data-id="{{ $items->id }}" class="button_wh_40 btn_view right-to quick-view">
                                <i class="fa fa-search"></i>
                              </a>
                              <a href="javascript:;" id="data-id" data-id="{{ $items->id }}" class="button_wh_40 btn-cart left-to muahang" title="Mua hàng"><span>Mua hàng</span>
                              </a>
                              @if (Auth::check())
                                <a title="Yêu thích" id="data-id" data-id="{{ $items->id }}" ok="{{ Auth::user()->id }}" class="button_wh_40 iWishAdd iwishAddWrapper" href="javascript:;" >
                                  <i class="fa fa-heart-o"></i>
                                </a>
                              @else
                                <a title="Yêu thích" class="button_wh_40 iWishAdda iwishAddWrapper" href="javascript:;" ><i class="fa fa-heart-o"></i>
                                </a>
                              @endif
                            </div>
                          </form>
                        </div>
                      </div>
                      <div class="product-info a-center">
                        <div class="reviews-product-grid">
                          <div class="bizweb-product-reviews-badge" data-id="8988858"></div>
                        </div>
                        <h3 class="product-name">
                          <a class="text2line" href="{{ url('san-pham/'.$items->Slug) }}" alt="{!! $items->Name !!}" title="{!! $items->Name !!}">{!! $items->Name !!}
                          </a>
                        </h3>
                        <div class="price-box clearfix">
                          <span class="price product-price">{!! isset($items->GiaSale) ? number_format($items->GiaSale,0,',','.') : number_format($items->Gia,0,',','.') !!}₫</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                @endforeach
                @else
                <div class="col-lg-12 col-md-12 block_width"><span class="warning">Đang cập nhật sản phẩm</span></div>
                @endif
              </div>
            </div>
          </div>
          <div id="content-tabb5" class="content-tab content-tab-proindex" style="display:none">
            <div class="clearfix wrap_item_list row">
              <div class="owl_mobile owl-carousel not-dqowl">
                @if (count($phukien) > 0)
                @foreach ($phukien as $items)
                <div class="col-xxs col-xs-12 col-md-3 col-sm-12 col-lg-3">
                  <div class="wrp_item_small product-col">
                    <div class="product-box">
                      <div class="product-thumbnail">
                        <a href="{{ url('san-pham/'.$items->Slug) }}" class="image_link display_flex" data-images="" title="{!! $items->Name !!}">
                          <img src="{{ asset('uploads/sanpham/'.$items->Image) }}" title="{!! $items->Name !!}" alt="{!! $items->Name !!}">
                        </a>
                        <div class="product-action-grid clearfix">
                          <form action="" method="post" class="variants form-nut-grid" enctype="multipart/form-data">
                            <div>
                              <a title="xem nhanh" href="javascript:;" id="data-id" data-id="{{ $items->id }}" class="button_wh_40 btn_view right-to quick-view">
                                <i class="fa fa-search"></i>
                              </a>
                              <a href="javascript:;" id="data-id" data-id="{{ $items->id }}" class="button_wh_40 btn-cart left-to muahang" title="Mua hàng"><span>Mua hàng</span>
                              </a>
                              @if (Auth::check())
                                <a title="Yêu thích" id="data-id" data-id="{{ $items->id }}" ok="{{ Auth::user()->id }}" class="button_wh_40 iWishAdd iwishAddWrapper" href="javascript:;" >
                                  <i class="fa fa-heart-o"></i>
                                </a>
                              @else
                                <a title="Yêu thích" class="button_wh_40 iWishAdda iwishAddWrapper" href="javascript:;" ><i class="fa fa-heart-o"></i>
                                </a>
                              @endif
                            </div>
                          </form>
                        </div>
                      </div>
                      <div class="product-info a-center">
                        <div class="reviews-product-grid">
                          <div class="bizweb-product-reviews-badge" data-id="8988858"></div>
                        </div>
                        <h3 class="product-name">
                          <a class="text2line" href="{{ url('san-pham/'.$items->Slug) }}" alt="{!! $items->Name !!}" title="{!! $items->Name !!}">{!! $items->Name !!}
                          </a>
                        </h3>
                        <div class="price-box clearfix">
                          <span class="price product-price">{!! isset($items->GiaSale) ? number_format($items->GiaSale,0,',','.') : number_format($items->Gia,0,',','.') !!}₫</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                @endforeach
                @else
                <div class="col-lg-12 col-md-12 block_width"><span class="warning">Đang cập nhật sản phẩm</span></div>
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</section>
<section class="awe-section-7">
  <section class="section_blog_index">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="wrap_blog_index row relative">
            <div class="title_section_center mr_0">
              <h2 class="title cooktail_">
                <a href="{{ url('tin-tuc') }}.html" title="Tin tức thời trang" alt="Tin tức thời trang"> Tin Tức Thời Trang </a>
              </h2>
            </div>
            <div class="wrap_owl_blog owl-blog-index owl-carousel not-dqowl timeout" data-lg-items="3" data-md-items="3" data-sm-items="2" data-xs-items="1" data-height="true" data-dot="true" data-nav="false" data-margin="0">
              @foreach ($tintuc as $items)
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <article class="blog-item blog-item-grid">
                  <div class="wrap_blg">
                    <div class="blog-item-thumbnail img1">
                      <a href="{{ url('tin-tuc/'.$items->Slug) }}">
                        <picture>
                          <img src="{{ asset('uploads/tintuc/'.$items->Image) }}" style="max-width:100%;height: 247px" class="img-responsive" alt="{!! $items->TieuDe !!}" title="{!! $items->TieuDe !!}">
                        </picture>
                      </a>
                      <div class="content_article_">
                        <span class="time_post"><i class="fa fa-clock-o"></i>&nbsp; {!! $items->created_at !!}</span>
                        <h3 class="blog-item-name">
                          <a class="text2line" href="{{ url('tin-tuc/'.$items->Slug) }}" title="{!! $items->TieuDe !!}" alt="{!! $items->TieuDe !!}">{!! $items->TieuDe !!}
                          </a>
                        </h3>
                      </div>
                    </div>
                  </div>
                </article>
              </div>
              @endforeach
            </div>
            <div class="button_more">
              <a href="{{ url('tin-tuc') }}.html" title="Xem thêm" class="more_blog">Xem thêm</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</section>
@endsection
@section('script')
<script type="text/javascript">
</script>
@endsection