@extends('frontend.layout.master')

@section('title','Tony4men - Đăng Ký')
@section('keywords')
@section('description')
@section('url',url('/dang-ky.html'))
@section('titleseo','Tony4men - Đăng Ký')
@section('type','product')
@section('descriptionseo')
@section('image')

@section('content')
<section class="breadcrumb_background">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="wrap_breadcrumb a-center">
          <h1 class="title-head-page margin-top-0">Đăng ký tài khoản</h1>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="bread-crumb">
  <span class="crumb-border"></span>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 a-left">
        <ul class="breadcrumb" itemscope="" itemtype="">
          <li class="home">
            <a itemprop="url" href="{{ url('/') }}"><span itemprop="title">Trang chủ</span></a>            
            <span class="mr_lr"> / </span>
          </li>
          <li><strong><span itemprop="title">Đăng ký tài khoản</span></strong></li>
        </ul>
      </div>
    </div>
  </div>
</section>
<div class="container margin-top-30">
  <div class="title_head margin-bottom-20">
    <h4 class="title_center_page">
      <span>Đăng ký tài khoản</span>
    </h4>
  </div>
  <div class="row">
    <div class="col-md-4"> </div>
    @if(count($errors) > 0)
    <div class="alert alert-danger col-md-4" style="margin-top: 1em;border-radius: 5px">
      <ul style="list-style: none">
        @foreach($errors->all() as $err)
        <li><i class="ace-icon fa fa-exclamation-triangle"></i> {{ $err }} </li>
        @endforeach
      </ul>
    </div>
    @endif
    <div class="col-md-4"> </div>
  </div>
  <div class="row">
    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 col-lg-offset-4 col-md-offset-3 col-sm-offset-3">
      <div class="page-login">
        <div id="login">
          <link href="{{ asset('frontend/css/challenge.css') }}" rel="stylesheet">
          <div class="bizweb-challenge__container">
            <form accept-charset="UTF-8" action="{{ url('dang-ky.html') }}" id="customer_register" method="post">
              {{ csrf_field() }}
              <div class="form-signup clearfix">
                <div class="row">
                  <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                    <fieldset class="form-group">
                      <input type="text" class="form-control form-control-lg" placeholder="Họ &amp; tên" value="{{ old('name') }}" name="name" id="name" required="">
                    </fieldset>
                    <fieldset class="form-group" id="staticParent">
                      <input type="text" class="form-control form-control-lg" pattern="^[0-9]*$" placeholder="Số điện thoại" value="{{ old('phone') }}" name="phone" id="phone" required="">
                    </fieldset>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                    <fieldset class="form-group">
                      <input type="email" class="form-control form-control-lg" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" placeholder="Email" value="{{ old('email') }}" name="email" id="email" required="">
                    </fieldset>
                    <fieldset class="form-group">
                      <input type="password" class="form-control form-control-lg" placeholder="Mật khẩu" value="" name="password" required="">
                    </fieldset>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                    <fieldset class="form-group">
                      <select class="field__input field__input--select form-control" name="tinhthanh" id="tinhThanhChange" required style="font-weight: 600;padding-left: 20px;">
                        <option value=''>--- Chọn tỉnh thành ---</option>
                        @foreach ($tinhThanh as $items)
                          <option  value="{{ $items->id }}">{{ $items->Name }}</option>
                        @endforeach
                      </select>
                    </fieldset>
                    <fieldset class="form-group">
                      <select class="field__input field__input--select form-control" name="quanhuyen" id="quanHuyenChange" required style="font-weight: 600;padding-left: 20px;">
                        <option value="">--- Chọn quận huyện ---</option>
                      </select>
                    </fieldset>
                    <fieldset class="form-group">
                      <select class="field__input field__input--select form-control" name="xaphuong" id="xaPhuongChange" required style="font-weight: 600;padding-left: 20px;">
                        <option value="">--- Chọn xã phường ---</option>
                      </select>
                    </fieldset>
                    <fieldset class="form-group">
                      <input name="address" type="text" value="{{ old('address') }}" class="ip field__input form-control" id="" placeholder="Địa chỉ" required />
                    </fieldset>
                  </div>
                </div>
                <div class="col-xs-12 text-xs-left margin-bottom-15" style="margin-top:5px; padding: 0">
                  <button type="summit" value="Đăng ký" class="btn button-50 btn-50-blue width_100">Đăng ký </button>
                </div>
                <span class="have_ac margin-top-15" style="display:block;">Đã có tài khoản đăng nhập 
                  <a href="{{ url('dang-nhap.html') }}">tại đây</a>
                </span>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection