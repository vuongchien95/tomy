<script src="{{ asset('backend/assets/js/jquery-2.1.4.min.js') }}"></script>
<script src="{{ asset('backend/assets/js/bootstrap.min.js') }}"></script>


<!-- page specific plugin scripts -->

    <!--[if lte IE 8]>
      <script src="assets/js/excanvas.min.js"></script>
    <![endif]-->

    
    

    <!-- ace scripts -->
    
    
    <script src="{{ asset('backend/assets/js/jquery-ui.custom.min.js') }}"></script>
    <script src="{{ asset('backend/assets/js/jquery.ui.touch-punch.min.js') }}"></script>
    <script src="{{ asset('backend/assets/js/jquery.easypiechart.min.js') }}"></script>
    <script src="{{ asset('backend/assets/js/jquery.sparkline.index.min.js') }}"></script>
    <script src="{{ asset('backend/assets/js/jquery.flot.min.js') }}"></script>
    <script src="{{ asset('backend/assets/js/jquery.flot.pie.min.js') }}"></script>
    <script src="{{ asset('backend/assets/js/jquery.flot.resize.min.js') }}"></script>
    <script src="{{ asset('backend/assets/js/wizard.min.js') }}"></script>
    <script src="{{ asset('backend/assets/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('backend/assets/js/jquery-additional-methods.min.js') }}"></script>
    <script src="{{ asset('backend/assets/js/bootstrap-wysiwyg.min.js') }}"></script>
    <script src="{{ asset('backend/assets/js/bootbox.js') }}"></script>
    <script src="{{ asset('backend/assets/js/jquery.maskedinput.min.js') }}"></script>
    <script src="{{ asset('backend/assets/js/select2.min.js') }}"></script>
    <script src="{{ asset('backend/assets/js/dropzone.min.js') }}"></script>
    <script src="{{ asset('backend/assets/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('backend/assets/js/dataTables.select.min.js') }}"></script>
    <script src="{{ asset('backend/assets/js/jquery.dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('backend/assets/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('backend/assets/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('backend/assets/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('backend/assets/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('backend/assets/js/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('backend/assets/js/jquery.gritter.min.js') }}"></script>
    <script src="{{ asset('backend/assets/js/spin.js') }}"></script>
    <script src="{{ asset('backend/assets/js/ace-elements.min.js') }}"></script>
    <script src="{{ asset('backend/assets/js/ace.min.js') }}"></script>
    <script src="{{ asset('backend/assets/js/chosen.jquery.min.js') }}"></script>
    <script src="{{ asset('backend/assets/js/spinbox.min.js') }}"></script>
    <script src="{{ asset('backend/assets/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('backend/assets/js/bootstrap-timepicker.min.js') }}"></script>
    <script src="{{ asset('backend/assets/js/moment.min.js') }}"></script>
    <script src="{{ asset('backend/assets/js/daterangepicker.min.js') }}"></script>
    <script src="{{ asset('backend/assets/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('backend/assets/js/bootstrap-colorpicker.min.js') }}"></script>
    <script src="{{ asset('backend/assets/js/jquery.knob.min.js') }}"></script>
    <script src="{{ asset('backend/assets/js/autosize.min.js') }}"></script>
    <script src="{{ asset('backend/assets/js/jquery.inputlimiter.min.js') }}"></script>
    <script src="{{ asset('backend/assets/js/bootstrap-tag.min.js') }}"></script>
    <script src="{{ asset('backend/assets/js/fullcalendar.min.js') }}"></script>

   
    
    
    