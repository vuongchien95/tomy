<link rel="stylesheet" href="{{ asset('backend/assets/css/bootstrap.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('backend/assets/font-awesome/4.5.0/css/font-awesome.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('backend/assets/css/fonts.googleapis.com.css') }}" />
  <link rel="stylesheet" href="{{ asset('backend/assets/css/ace.min.css') }}" class="ace-main-stylesheet" id="main-ace-style" />
  <link rel="stylesheet" href="{{ asset('backend/assets/css/select2.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('backend/assets/css/style.css') }}" />
  <link rel="stylesheet" href="{{ asset('backend/assets/css/jquery-ui.custom.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('backend/assets/css/fullcalendar.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('backend/assets/css/dropzone.min.css') }}" />
  

    <!-- [if lte IE 9]>
      <link rel="stylesheet" href="assets/css/ace-part2.min.css" class="ace-main-stylesheet" />
      <![endif] -->
      <link rel="stylesheet" href="{{ asset('backend/assets/css/ace-skins.min.css') }}" />
      <link rel="stylesheet" href="{{ asset('backend/assets/css/ace-rtl.min.css') }}" />
      {{-- <link rel="stylesheet" href="assets/css/ace-ie.min.css" /> --}}