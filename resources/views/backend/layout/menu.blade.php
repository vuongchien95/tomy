<div id="sidebar" class="sidebar responsive ace-save-state">
	<script type="text/javascript">
		try{ace.settings.loadState('sidebar')}catch(e){}
	</script>

	<div class="sidebar-shortcuts" id="sidebar-shortcuts">
		<div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
			<button class="btn btn-success">
				<i class="ace-icon fa fa-signal"></i>
			</button>

			<button class="btn btn-info">
				<i class="ace-icon fa fa-leaf"></i>
			</button>

			<button class="btn btn-warning">
				<i class="ace-icon fa fa-users"></i>
			</button>

			<button class="btn btn-danger">
				<i class="ace-icon fa fa-cogs"></i>
			</button>
		</div>

		<div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
			<span class="btn btn-success"></span>

			<span class="btn btn-info"></span>

			<span class="btn btn-warning"></span>

			<span class="btn btn-danger"></span>
		</div>
	</div><!-- /.sidebar-shortcuts -->

	<ul class="nav nav-list">
		<li class="{{ Request::is('admin/pages') ? 'active' : '' }}">
			<a href="{{ url('admin/pages') }}">
				<i class="menu-icon fa fa-tachometer"></i>
				<span class="menu-text"> Quản Trị Hệ Thống </span>
			</a>

			<b class="arrow"></b>
		</li>

		<li class="{{ Request::is('admin/cauhinhweb*') ? 'open' : '' }}">
			<a href="javascript:voi" class="dropdown-toggle">
				<i class="menu-icon ace-icon fa fa-cogs red"></i>
				<span class="menu-text">
					Cấu Hình Web
				</span>

				<b class="arrow fa fa-angle-down"></b>
			</a>

			<b class="arrow"></b>

			<ul class="submenu">
				<li class="{{ Request::is('admin/cauhinhweb/cau-hinh') ? 'active' : '' }}">
					<a href="{{ url('admin/cauhinhweb/cau-hinh') }}">
						<i class="menu-icon fa fa-caret-right"></i>
						Cấu Hình Chung
					</a>

					<b class="arrow"></b>
				</li>

				<li class="{{ Request::is('admin/cauhinhweb/banner') ? 'active' : '' }}">
					<a href="{{ url('admin/cauhinhweb/banner') }}">
						<i class="menu-icon fa fa-caret-right"></i>
						Banner Web
					</a>

					<b class="arrow"></b>
				</li>

				<li class="{{ Request::is('admin/cauhinhweb/logo') ? 'active' : '' }}">
					<a href="{{ url('admin/cauhinhweb/logo') }}">
						<i class="menu-icon fa fa-caret-right"></i>
						Logo Web
					</a>

					<b class="arrow"></b>
				</li>

				<li class="{{ Request::is('admin/cauhinhweb/lienhe') ? 'active' : '' }}">
					<a href="{{ url('admin/cauhinhweb/lienhe') }}">
						<i class="menu-icon fa fa-caret-right"></i>
						Liên Hệ
					</a>

					<b class="arrow"></b>
				</li>

        <li class="{{ Request::is('admin/cauhinhweb/video-fb') ? 'active' : '' }}">
          <a href="{{ url('admin/cauhinhweb/video-fb') }}">
            <i class="menu-icon fa fa-caret-right"></i>
            Video - Facebook
          </a>

          <b class="arrow"></b>
        </li>

			</ul>
		</li>

    <li class="{{ Request::is('admin/danhmuc') ? 'active' : '' }}">
      <a href="{{ url('admin/danhmuc') }}">
        {{-- <i class="menu-icon ace-icon glyphicon glyphicon-list"></i> --}}
        <i class="menu-icon ace-icon fa fa-folder-o smaller-140 green"></i>
        <span class="menu-text"> Danh Mục </span>
      </a>

      <b class="arrow"></b>
    </li>

		<li class="{{ Request::is('admin/tintuc') ? 'open' : '' }}">
			<a href="javascript:voi" class="dropdown-toggle">
				<i class="menu-icon fa fa-list-alt orange"></i>
				<span class="menu-text">
					Tin Tức - Dịch Vụ
				</span>

				<b class="arrow fa fa-angle-down"></b>
			</a>

			<b class="arrow"></b>

			<ul class="submenu">
				<li class="{{ Request::is('admin/tintuc') ? 'active' : '' }}">
					<a href="{{ url('admin/tintuc') }}">
						<i class="menu-icon fa fa-caret-right"></i>
						Tin Tức 
					</a>

					<b class="arrow"></b>
				</li>

				<li class="{{ Request::is('admin/dichvu') ? 'active' : '' }}">
					<a href="{{ url('admin/dichvu') }}">
						<i class="menu-icon fa fa-caret-right"></i>
						Dịch Vụ
					</a>

					<b class="arrow"></b>
				</li>
			</ul>
		</li>

		<li class="{{ Request::is('admin/sanpham') ? 'active' : '' }}">
			<a href="{{ url('admin/sanpham') }}">
				<i class="menu-icon ace-icon fa fa-leaf light blue"></i>
				<span class="menu-text"> Sản Phẩm </span>
			</a>

			<b class="arrow"></b>
		</li>

		<li class="{{ Request::is('admin/donhang') ? 'active' : '' }}">
			<a href="{{ url('admin/donhang') }}">
				<i class="menu-icon ace-icon fa fa-credit-card Light brown"></i>
				<span class="menu-text"> Đơn Đặt Hàng </span>
        <span class="badge badge-info">+8</span>
			</a>

			<b class="arrow"></b>
		</li>

    <li class="{{ Request::is('admin/user') ? 'active' : '' }}">
      <a href="{{ url('admin/user') }}">
        <i class="menu-icon ace-icon glyphicon glyphicon-user red"></i>
        <span class="menu-text"> Thành Viên Web </span>
      </a>

      <b class="arrow"></b>
    </li>

    <li class="">
      <a href="javascript:voi">
        <i class="menu-icon ace-icon glyphicon glyphicon-book blue"></i>
        <span class="menu-text"> Kho Hàng </span>
      </a>

      <b class="arrow"></b>
    </li>

    <li class="{{ Request::is('admin/pages/lich') ? 'active' : '' }}">
      <a href="{{ url('admin/pages/lich') }}">
        <i class="menu-icon fa fa-calendar grey"></i>

        <span class="menu-text">
          Xem Lịch
          <span class="badge badge-transparent tooltip-error" title="" data-original-title="2 Important Events">
            <i class="ace-icon fa fa-exclamation-triangle red bigger-130"></i>
          </span>
        </span>
      </a>

      <b class="arrow"></b>
    </li>
		
    <li class="">
      <a href="javascript:voi(0)" class="dropdown-toggle">
        <i class="menu-icon fa fa-code Dark brown"></i>
        <span class="menu-text">
          Quản Lý Code
        </span>

        <b class="arrow fa fa-angle-down"></b>
      </a>

      <b class="arrow"></b>

      <ul class="submenu">
        <li class="">
          <a href="javascript:voi(0)">
            <i class="menu-icon fa fa-caret-right"></i>
            Quản Lý Menu 
          </a>

          <b class="arrow"></b>
        </li>

        <li class="">
          <a href="javascript:voi(0)">
            <i class="menu-icon fa fa-caret-right"></i>
            Chỉnh sửa code
          </a>

          <b class="arrow"></b>
        </li>
        <li class="">
          <a href="javascript:voi(0)">
            <i class="menu-icon fa fa-caret-right"></i>
            Thư mục ảnh
          </a>

          <b class="arrow"></b>
        </li>
        <li class="">
          <a href="javascript:voi(0)">
            <i class="menu-icon fa fa-caret-right"></i>
            Thay đổi màu
          </a>

          <b class="arrow"></b>
        </li>
        <li class="">
          <a href="javascript:voi(0)">
            <i class="menu-icon fa fa-caret-right"></i>
            Mã Nhúng
          </a>

          <b class="arrow"></b>
        </li>
      </ul>
    </li>

    <li class="{{ Request::is('admin/phanmorong*') ? 'open' : '' }}">
      <a href="javascript:void(0)" class="dropdown-toggle">
        <i class="menu-icon ace-icon fa fa-globe red"></i>
        <span class="menu-text">
          Phần Mở Rộng
        </span>

        <b class="arrow fa fa-angle-down"></b>
      </a>

      <b class="arrow"></b>

      <ul class="submenu">
        <li class="{{ Request::is('admin/phanmorong/gioithieu') ? 'active' : '' }}">
          <a href="{{ url('admin/phanmorong/gioithieu') }}">
            <i class="menu-icon fa fa-caret-right"></i>
            Giới Thiệu Cty
          </a>

          <b class="arrow"></b>
        </li>

        <li class="{{ Request::is('admin/phanmorong/chinhsach') ? 'active' : '' }}">
          <a href="{{ url('admin/phanmorong/chinhsach') }}">
            <i class="menu-icon fa fa-caret-right"></i>
            Chính Sách - Ưu Đãi
          </a>

          <b class="arrow"></b>
        </li>

        <li class="">
          <a href="javascript:voi">
            <i class="menu-icon fa fa-caret-right"></i>
            Dự Án Tiêu Biểu 
          </a>

          <b class="arrow"></b>
        </li>

        <li class="">
          <a href="javascript:voi">
            <i class="menu-icon fa fa-caret-right"></i>
            Các Cơ Sở Khác
          </a>

          <b class="arrow"></b>
        </li>

        <li class="">
          <a href="javascript:voi">
            <i class="menu-icon fa fa-caret-right"></i>
            Cảm Nhận Khách Hàng
          </a>

          <b class="arrow"></b>
        </li>

        <li class="">
          <a href="javascript:voi">
            <i class="menu-icon fa fa-caret-right"></i>
            Đăng Ký Nhận Mail
          </a>

          <b class="arrow"></b>
        </li>

        <li class="">
          <a href="javascript:voi">
            <i class="menu-icon fa fa-caret-right"></i>
            Yêu Cầu Gọi Lại
          </a>

          <b class="arrow"></b>
        </li>

        <li class="">
          <a href="javascript:voi">
            <i class="menu-icon fa fa-caret-right"></i>
            Quảng Cáo
          </a>

          <b class="arrow"></b>
        </li>

      </ul>
    </li>

		<li class="{{ Request::is('admin/pages/inbox-mail') ? 'active' : '' }}">
			<a href="{{ url('admin/pages/inbox-mail') }}">
				<i class="menu-icon ace-icon fa fa-comment green"></i>
				<span class="menu-text"> Inbox Mail </span>
        <span class="badge badge-warning">2</span>
			</a>

			<b class="arrow"></b>
		</li>
		
		<li class="">
			<a href="javascript:voi">
				<i class="menu-icon ace-icon glyphicon glyphicon-bookmark"></i>
				<span class="menu-text"> HD Sử Dụng </span>
			</a>

			<b class="arrow"></b>
		</li>
		
		
	</ul>

	<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
		<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
	</div>
</div>