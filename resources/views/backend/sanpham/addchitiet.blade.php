@extends('backend.layout.master')
@section('content')
<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="{{ url('admin/pages') }}">Home</a>
        </li>

        <li>
          <a href="{{ url('admin/sanpham') }}"> Sản Phẩm </a>
        </li>
        <li class="active">Thêm chi tiết sản phẩm</li>
      </ul><!-- /.breadcrumb -->
      <div class="nav-search" id="nav-search">
        <form class="form-search">
          <span class="input-icon">
            <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
            <i class="ace-icon fa fa-search nav-search-icon"></i>
          </span>
        </form>
      </div><!-- /.nav-search -->
    </div>
    <div class="page-content">
      <div class="row">
        <div class="col-xs-12">
          <div class="widget-box">
            <div class="widget-header widget-header-blue widget-header-flat">
              <h4 class="widget-title lighter">Thêm chi tiết sản phẩm: {{ $sanpham->Name }}</h4>
            </div>
            <div class="widget-body">
              @include('backend.block.err')
              <div class="widget-main">
                <div id="fuelux-wizard-container">
                  <div class="step-content pos-rel">
                    <div class="step-pane active" data-step="1">
                      <form action="{{ route('admin.sanpham.getDetail',$sanpham->id) }}" class="form-horizontal" method="POST" id="validation-form" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                          <div class="pull-text col-sm-3" ></div>
                          <div class="pull-text col-sm-7">
                            @include('backend.block.alert')
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="name">Thêm Ảnh Cho Sản Phẩm:</label>
                          <div class="col-xs-12 col-sm-6">
                            <div class="clearfix">
                              <input multiple="" name="avatar[]" type="file" id="id-input-file-3" />
                            </div>
                          </div>
                        </div>      
                        <div class="row form-group" style="margin-bottom: 2em">
                          @if(count($sp_img) > 0)
                          @foreach($sp_img as $items)
                          <div class="col-md-2" style="margin-bottom: 2em">
                            <img src="{{ asset('uploads/images/sanphamimg/'.$items->Image) }}" style="width: 100%;height: 100%;padding: 1em;border: 1px solid #c8bdbd; border-radius: 5px;">
                            &nbsp; &nbsp; &nbsp;
                            <a href="{{ url('admin/sanpham/delimg',$items->id) }}" class="btn btn-danger" style="margin:1em 0 0 2em;border-radius:5px;padding: 4px;">
                              <i class="ace-icon glyphicon glyphicon-remove"></i>
                              Xóa 
                            </a>
                          </div>
                          @endforeach
                          @endif
                        </div>
                        
                        @if ($sanpham->Kieu == 1) {{-- áo --}}
                        <h3 class="header smaller lighter blue">
                          Size - Màu - Số Lượng
                          <small>(Chọn màu và số lượng theo size) 
                            <span class="help-button" data-rel="popover" data-trigger="hover" data-placement="right" data-content="- Tích chọn Size - Màu - Số lượng (thông tin nào không có vui lòng để trống) " title="" data-original-title="Chú ý !">?</span>
                          </small>
                        </h3>
                        <div class="form-group row">
                          <div class="col-xs-12 col-sm-2">
                            <div class="control-group text-center">
                              <label>
                                @if ($query)
                                <input type="checkbox" name="sizeS" class="ace ace-checkbox-2" {{ $sp_chitiet[0]->size == "S" ? 'checked' : null }}  value="S">
                                @else
                                <input name="sizeS" class="ace ace-checkbox-2" type="checkbox"  value="S">
                                @endif
                                
                                <span class="lbl blue" style="font-weight: bold"> size S</span>
                              </label>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[0]->mau_sl[0]->mau == "Xanh" ? 'checked' :null }} name="mau1" value="Xanh" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau1" value="Xanh" class="ace input-lg">
                                    @endif
                                    <span class="lbl bigger-120" style="font-size: 11px !important;">Xanh</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" >
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[0]->mau_sl[0]->sl }}" class="input-sm" name="soluong1" id="spinner2" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong1" id="spinner2" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                    <input type="checkbox" {{ $sp_chitiet[0]->mau_sl[1]->mau == "Đỏ" ? 'checked' :null }} name="mau2" value="Đỏ" class="ace input-lg">
                                    @else
                                    <input type="checkbox" name="mau2" value="Đỏ" class="ace input-lg">
                                    @endif
                                    <span class="lbl bigger-120" style="font-size: 11px !important;">Đỏ</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner">
                                    @if ($query)
                                    <input type="text" value="{{ $sp_chitiet[0]->mau_sl[1]->sl }}"  class="input-sm" name="soluong2" id="spinner8" />
                                    @else
                                    <input type="text" class="input-sm" name="soluong2" id="spinner8" />
                                    @endif
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[0]->mau_sl[2]->mau == "Đen" ? 'checked' :null }} name="mau3" value="Đen" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau3" value="Đen" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Đen</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[0]->mau_sl[2]->sl }}" class="input-sm" name="soluong3" id="spinner9" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong3" id="spinner9" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[0]->mau_sl[3]->mau == "Vàng" ? 'checked' :null }} name="mau4" value="Vàng" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau4" value="Vàng" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Vàng</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[0]->mau_sl[3]->sl }}" class="input-sm" name="soluong4" id="spinner10" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong4" id="spinner10" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[0]->mau_sl[4]->mau == "Trắng" ? 'checked' :null }} name="mau5" value="Trắng" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau5" value="Trắng" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Trắng</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[0]->mau_sl[4]->sl }}" class="input-sm" name="soluong5" id="spinner11" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong5" id="spinner11" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                            </div>
                          </div>
                          <div class="col-xs-12 col-sm-2">
                            <div class="control-group text-center">
                              <label>
                                @if ($query)
                                <input type="checkbox" name="sizeM" class="ace ace-checkbox-2" {{ $sp_chitiet[1]->size == "M" ? 'checked' : null }}  value="M">
                                @else
                                <input name="sizeM" class="ace ace-checkbox-2" type="checkbox"  value="M">
                                @endif
                                
                                <span class="lbl blue" style="font-weight: bold"> size M</span>
                              </label>

                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[1]->mau_sl[0]->mau == "Xanh" ? 'checked' :null }} name="mau6" value="Xanh" class="ace input-lg" title="Xanh">
                                    @else
                                      <input type="checkbox" name="mau6" value="Xanh" class="ace input-lg" title="Xanh">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Xanh</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[1]->mau_sl[0]->sl }}" class="input-sm" name="soluong6" id="spinner3" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong6" id="spinner3" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>

                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[1]->mau_sl[1]->mau == "Đỏ" ? 'checked' :null }} name="mau7" value="Đỏ" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau7" value="Đỏ" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Đỏ</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[1]->mau_sl[1]->sl }}" class="input-sm" name="soluong7" id="spinner12" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong7" id="spinner12" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>

                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[1]->mau_sl[2]->mau == "Đen" ? 'checked' :null }}  name="mau8" value="Đen" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau8" value="Đen" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Đen</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[1]->mau_sl[2]->sl }}" class="input-sm" name="soluong8" id="spinner13" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong8" id="spinner13" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>

                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[1]->mau_sl[3]->mau == "Vàng" ? 'checked' :null }} name="mau9" value="Vàng" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau9" value="Vàng" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Vàng</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[1]->mau_sl[3]->sl }}" class="input-sm" name="soluong9" id="spinner14" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong9" id="spinner14" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>

                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[1]->mau_sl[4]->mau == "Trắng" ? 'checked' :null }} name="mau10" value="Trắng" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau10" value="Trắng" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Trắng</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[1]->mau_sl[4]->sl }}" class="input-sm" name="soluong10" id="spinner15" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong10" id="spinner15" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                            </div>
                          </div>
                          <div class="col-xs-12 col-sm-2">
                            <div class="control-group text-center">
                              <label>
                                @if ($query)
                                <input type="checkbox" name="sizeL" class="ace ace-checkbox-2" {{ $sp_chitiet[2]->size == "L" ? 'checked' : null }}  value="L">
                                @else
                                <input name="sizeL" class="ace ace-checkbox-2" type="checkbox"  value="L">
                                @endif
                                
                                <span class="lbl blue" style="font-weight: bold"> size L</span>
                              </label>

                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[2]->mau_sl[0]->mau == "Xanh" ? 'checked' :null }} name="mau11" value="Xanh" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau11" value="Xanh" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Xanh</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query) 
                                      <input type="text" value="{{ $sp_chitiet[2]->mau_sl[0]->sl }}" class="input-sm" name="soluong11" id="spinner4" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong11" id="spinner4" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>

                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[2]->mau_sl[1]->mau == "Đỏ" ? 'checked' :null }} name="mau12" value="Đỏ" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau12" value="Đỏ" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Đỏ</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[2]->mau_sl[1]->sl }}" class="input-sm" name="soluong12" id="spinner16" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong12" id="spinner16" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>

                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[2]->mau_sl[2]->mau == "Đen" ? 'checked' :null }} name="mau13" value="Đen" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau13" value="Đen" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Đen</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[2]->mau_sl[2]->sl }}" class="input-sm" name="soluong13" id="spinner17" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong13" id="spinner17" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>

                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[2]->mau_sl[3]->mau == "Vàng" ? 'checked' :null }} name="mau14" value="Vàng" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau14" value="Vàng" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Vàng</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[2]->mau_sl[3]->sl }}" class="input-sm" name="soluong14" id="spinner18" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong14" id="spinner18" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>

                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[2]->mau_sl[4]->mau == "Trắng" ? 'checked' : null }} name="mau15" value="Trắng" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau15" value="Trắng" class="ace input-lg">
                                    @endif
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Trắng</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[2]->mau_sl[4]->sl }}"  class="input-sm" name="soluong15" id="spinner19" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong15" id="spinner19" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>

                            </div>
                          </div>
                          <div class="col-xs-12 col-sm-2">
                            <div class="control-group text-center">
                              <label>
                                @if ($query)
                                <input type="checkbox" name="sizeXL" class="ace ace-checkbox-2" {{ $sp_chitiet[3]->size == "XL" ? 'checked' : null }}  value="XL">
                                @else
                                <input name="sizeXL" class="ace ace-checkbox-2" type="checkbox"  value="XL">
                                @endif
                                
                                <span class="lbl blue" style="font-weight: bold"> size XL</span>
                              </label>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[3]->mau_sl[0]->mau == "Xanh" ? 'checked' : null }} name="mau16" value="Xanh" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau16" value="Xanh" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Xanh</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[3]->mau_sl[0]->sl }}" class="input-sm" name="soluong16" id="spinner5" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong16" id="spinner5" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[3]->mau_sl[1]->mau == "Đỏ" ? 'checked' : null }} name="mau17" value="Đỏ" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau17" value="Đỏ" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Đỏ</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[3]->mau_sl[1]->sl }}" class="input-sm" name="soluong17" id="spinner20" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong17" id="spinner20" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[3]->mau_sl[2]->mau == "Đen" ? 'checked' : null }} name="mau18" value="Đen" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau18" value="Đen" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Đen</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[3]->mau_sl[2]->sl }}" class="input-sm" name="soluong18" id="spinner21" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong18" id="spinner21" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[3]->mau_sl[3]->mau == "Vàng" ? 'checked' : null }} name="mau19" value="Vàng" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau19" value="Vàng" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Vàng</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[3]->mau_sl[3]->sl }}" class="input-sm" name="soluong19" id="spinner22" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong19" id="spinner22" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[3]->mau_sl[4]->mau == "Trắng" ? 'checked' : null }} name="mau20" value="Trắng" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau20" value="Trắng" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Trắng</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[3]->mau_sl[4]->sl }}" class="input-sm" name="soluong20" id="spinner23" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong20" id="spinner23" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                            </div>
                          </div>
                          <div class="col-xs-12 col-sm-2">
                            <div class="control-group text-center">
                              <label>
                                @if ($query)
                                <input type="checkbox" name="sizeXXL" class="ace ace-checkbox-2" {{ $sp_chitiet[4]->size == "XXL" ? 'checked' : null }}  value="XXL">
                                @else
                                <input name="sizeXXL" class="ace ace-checkbox-2" type="checkbox"  value="XXL">
                                @endif
                                
                                <span class="lbl blue" style="font-weight: bold"> size XXL</span>
                              </label>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[4]->mau_sl[0]->mau == "Xanh" ? 'checked' : null }}  name="mau21" value="Xanh" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau21" value="Xanh" class="ace input-lg" >
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Xanh</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[4]->mau_sl[0]->sl }}" class="input-sm" name="soluong21" id="spinner6" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong21" id="spinner6" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[4]->mau_sl[1]->mau == "Đỏ" ? 'checked' : null }} name="mau22" value="Đỏ" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau22" value="Đỏ" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Đỏ</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[4]->mau_sl[1]->sl }}" class="input-sm" name="soluong22" id="spinner24" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong22" id="spinner24" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[4]->mau_sl[2]->mau == "Đen" ? 'checked' : null }} name="mau23" value="Đen" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau23" value="Đen" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Đen</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[4]->mau_sl[2]->sl }}" class="input-sm" name="soluong23" id="spinner25" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong23" id="spinner25" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[4]->mau_sl[3]->mau == "Vàng" ? 'checked' : null }} name="mau24" value="Vàng" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau24" value="Vàng" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Vàng</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[4]->mau_sl[3]->sl }}" class="input-sm" name="soluong24" id="spinner26" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong24" id="spinner26" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>

                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[4]->mau_sl[4]->mau == "Trắng" ? 'checked' : null }} name="mau25" value="Trắng" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau25" value="Trắng" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Trắng</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[4]->mau_sl[4]->sl }}" class="input-sm" name="soluong25" id="spinner27" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong25" id="spinner27" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>

                            </div>
                          </div>
                        </div>
                        @elseif($sanpham->Kieu == 2) {{-- quần --}}
                        <h3 class="header smaller lighter blue">
                          Size - Màu - Số Lượng
                          <small>(Chọn màu và số lượng theo size)
                            <span class="help-button" data-rel="popover" data-trigger="hover" data-placement="right" data-content="- Tích chọn Size - Màu - Số lượng (thông tin nào không có vui lòng để trống) " title="" data-original-title="Chú ý !">?</span>
                          </small>
                        </h3>
                        <div class="form-group row">
                          <div class="col-xs-12 col-sm-2">
                            <div class="control-group text-center">
                              <label>
                                @if ($query)
                                <input type="checkbox" name="size28" class="ace ace-checkbox-2" {{ $sp_chitiet[0]->size == "28" ? 'checked' : null }}  value="28">
                                @else
                                <input name="size28" class="ace ace-checkbox-2" type="checkbox"  value="28">
                                @endif
                                
                                <span class="lbl blue" style="font-weight: bold"> size 28</span>
                              </label>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[0]->mau_sl[0]->mau == "Xanh" ? 'checked' :null }} name="mau26" value="Xanh" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau26" value="Xanh" class="ace input-lg">
                                    @endif
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Xanh</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[0]->mau_sl[0]->sl }}" class="input-sm" name="soluong26" id="spinner2" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong26" id="spinner2" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[0]->mau_sl[1]->mau == "Xám" ? 'checked' :null }} name="mau27" value="Xám" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau27" value="Xám" class="ace input-lg">
                                    @endif
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Xám</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[0]->mau_sl[1]->sl }}" class="input-sm" name="soluong27" id="spinner28" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong27" id="spinner28" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[0]->mau_sl[2]->mau == "Đen" ? 'checked' :null }} name="mau28" value="Đen" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau28" value="Đen" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Đen</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[0]->mau_sl[2]->sl }}" class="input-sm" name="soluong28" id="spinner29" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong28" id="spinner29" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[0]->mau_sl[3]->mau == "Bạc" ? 'checked' :null }} name="mau29" value="Bạc" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau29" value="Bạc" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Bạc</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[0]->mau_sl[3]->sl }}" class="input-sm" name="soluong29" id="spinner30" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong29" id="spinner30" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[0]->mau_sl[4]->mau == "Trắng" ? 'checked' :null }} name="mau30" value="Trắng" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau30" value="Trắng" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Trắng</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[0]->mau_sl[4]->sl }}" class="input-sm" name="soluong30" id="spinner31" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong30" id="spinner31" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>

                            </div>
                          </div>
                          <div class="col-xs-12 col-sm-2">
                            <div class="control-group text-center">
                              <label>
                                @if ($query)
                                <input type="checkbox" name="size29" class="ace ace-checkbox-2" {{ $sp_chitiet[1]->size == "29" ? 'checked' : null }}  value="29">
                                @else
                                <input name="size29" class="ace ace-checkbox-2" type="checkbox"  value="29">
                                @endif
                                
                                <span class="lbl blue" style="font-weight: bold"> size 29</span>
                              </label>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[1]->mau_sl[0]->mau == "Xanh" ? 'checked' :null }} name="mau31" value="Xanh" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau31" value="Xanh" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Xanh</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[1]->mau_sl[0]->sl }}" class="input-sm" name="soluong31" id="spinner3" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong31" id="spinner3" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[1]->mau_sl[1]->mau == "Xám" ? 'checked' :null }} name="mau32" value="Xám" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau32" value="Xám" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Xám</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[1]->mau_sl[1]->sl }}" class="input-sm" name="soluong32" id="spinner32" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong32" id="spinner32" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[1]->mau_sl[2]->mau == "Đen" ? 'checked' :null }} name="mau33" value="Đen" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau33" value="Đen" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Đen</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[1]->mau_sl[2]->sl }}" class="input-sm" name="soluong33" id="spinner33" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong33" id="spinner33" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[1]->mau_sl[3]->mau == "Bạc" ? 'checked' :null }} name="mau34" value="Bạc" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau34" value="Bạc" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Bạc</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[1]->mau_sl[3]->sl }}" class="input-sm" name="soluong34" id="spinner34" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong34" id="spinner34" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[1]->mau_sl[4]->mau == "Trắng" ? 'checked' :null }} name="mau35" value="Trắng" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau35" value="Trắng" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Trắng</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[1]->mau_sl[4]->sl }}" class="input-sm" name="soluong35" id="spinner35" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong35" id="spinner35" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                            </div>
                          </div>
                          <div class="col-xs-12 col-sm-2">
                            <div class="control-group text-center">
                              <label>
                                @if ($query)
                                <input type="checkbox" name="size30" class="ace ace-checkbox-2" {{ $sp_chitiet[2]->size == "30" ? 'checked' : null }}  value="30">
                                @else
                                <input name="size30" class="ace ace-checkbox-2" type="checkbox"  value="30">
                                @endif
                                
                                <span class="lbl blue" style="font-weight: bold"> size 30</span>
                              </label>

                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[2]->mau_sl[0]->mau == "Xanh" ? 'checked' :null }} name="mau36" value="Xanh" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau36" value="Xanh" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Xanh</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[2]->mau_sl[0]->sl }}" class="input-sm" name="soluong36" id="spinner4" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong36" id="spinner4" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[2]->mau_sl[1]->mau == "Xám" ? 'checked' :null }} name="mau37" value="Xám" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau37" value="Xám" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Xám</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[2]->mau_sl[1]->sl }}" class="input-sm" name="soluong37" id="spinner36" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong37" id="spinner36" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[2]->mau_sl[2]->mau == "Đen" ? 'checked' :null }} name="mau38" value="Đen" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau38" value="Đen" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Đen</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[2]->mau_sl[2]->sl }}" class="input-sm" name="soluong38" id="spinner37" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong38" id="spinner37" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[2]->mau_sl[3]->mau == "Bạc" ? 'checked' :null }} name="mau39" value="Bạc" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau39" value="Bạc" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Bạc</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[2]->mau_sl[3]->sl }}" class="input-sm" name="soluong39" id="spinner38" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong39" id="spinner38" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[2]->mau_sl[4]->mau == "Trắng" ? 'checked' :null }} name="mau40" value="Trắng" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau40" value="Trắng" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Trắng</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[2]->mau_sl[4]->sl }}" class="input-sm" name="soluong40" id="spinner39" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong40" id="spinner39" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>

                            </div>
                          </div>
                          <div class="col-xs-12 col-sm-2">
                            <div class="control-group text-center">
                              <label>
                                @if ($query)
                                <input type="checkbox" name="size31" class="ace ace-checkbox-2" {{ $sp_chitiet[3]->size == "31" ? 'checked' : null }}  value="31">
                                @else
                                <input name="size31" class="ace ace-checkbox-2" type="checkbox"  value="31">
                                @endif
                                
                                <span class="lbl blue" style="font-weight: bold"> size 31</span>
                              </label>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[3]->mau_sl[0]->mau == "Xanh" ? 'checked' :null }} name="mau41" value="Xanh" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau41" value="Xanh" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Xanh</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[3]->mau_sl[0]->sl }}" class="input-sm" name="soluong41" id="spinner5" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong41" id="spinner5" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[3]->mau_sl[1]->mau == "Xám" ? 'checked' :null }} name="mau42" value="Xám" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau42" value="Xám" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Xám</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[3]->mau_sl[1]->sl }}" class="input-sm" name="soluong42" id="spinner40" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong42" id="spinner40" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[3]->mau_sl[2]->mau == "Đen" ? 'checked' :null }} name="mau43" value="Đen" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau43" value="Đen" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Đen</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[3]->mau_sl[2]->sl }}" class="input-sm" name="soluong43" id="spinner41" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong43" id="spinner41" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[3]->mau_sl[3]->mau == "Bạc" ? 'checked' :null }} name="mau44" value="Bạc" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau44" value="Bạc" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Bạc</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[3]->mau_sl[3]->sl }}" class="input-sm" name="soluong44" id="spinner42" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong44" id="spinner42" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[3]->mau_sl[4]->mau == "Trắng" ? 'checked' :null }} name="mau45" value="Trắng" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau45" value="Trắng" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Trắng</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[3]->mau_sl[4]->sl }}" class="input-sm" name="soluong45" id="spinner43" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong45" id="spinner43" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>

                            </div>
                          </div>
                          <div class="col-xs-12 col-sm-2">
                            <div class="control-group text-center">
                              <label>
                                @if ($query)
                                <input type="checkbox" name="size32" class="ace ace-checkbox-2" {{ $sp_chitiet[4]->size == "32" ? 'checked' : null }}  value="32">
                                @else
                                <input name="size32" class="ace ace-checkbox-2" type="checkbox"  value="32">
                                @endif
                                
                                <span class="lbl blue" style="font-weight: bold"> size 32</span>
                              </label>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[4]->mau_sl[0]->mau == "Xanh" ? 'checked' :null }} name="mau46" value="Xanh" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau46" value="Xanh" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Xanh</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[4]->mau_sl[0]->sl }}" class="input-sm" name="soluong46" id="spinner6" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong46" id="spinner6" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[4]->mau_sl[1]->mau == "Xám" ? 'checked' :null }} name="mau47" value="Xám" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau47" value="Xám" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Xám</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[4]->mau_sl[1]->sl }}" class="input-sm" name="soluong47" id="spinner44" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong47" id="spinner44" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[4]->mau_sl[2]->mau == "Đen" ? 'checked' :null }} name="mau48" value="Đen" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau48" value="Đen" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Đen</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[4]->mau_sl[2]->sl }}" class="input-sm" name="soluong48" id="spinner45" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong48" id="spinner45" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[4]->mau_sl[3]->mau == "Bạc" ? 'checked' :null }} name="mau49" value="Bạc" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau49" value="Bạc" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Bạc</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[4]->mau_sl[3]->sl }}" class="input-sm" name="soluong49" id="spinner46" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong49" id="spinner46" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[4]->mau_sl[4]->mau == "Trắng" ? 'checked' :null }} name="mau50" value="Trắng" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau50" value="Trắng" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Trắng</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[4]->mau_sl[4]->sl }}" class="input-sm" name="soluong50" id="spinner47" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong50" id="spinner47" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                            </div>
                          </div>
                          <div class="col-xs-12 col-sm-2">
                            <div class="control-group text-center">
                              <label>
                                @if ($query)
                                <input type="checkbox" name="size33" class="ace ace-checkbox-2" {{ $sp_chitiet[5]->size == "33" ? 'checked' : null }}  value="33">
                                @else
                                <input name="size33" class="ace ace-checkbox-2" type="checkbox"  value="33">
                                @endif
                                
                                <span class="lbl blue" style="font-weight: bold"> size 33</span>
                              </label>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[5]->mau_sl[0]->mau == "Xanh" ? 'checked' :null }} name="mau51" value="Xanh" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau51" value="Xanh" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Xanh</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[5]->mau_sl[0]->sl }}" class="input-sm" name="soluong51" id="spinner7" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong51" id="spinner7" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[5]->mau_sl[1]->mau == "Xám" ? 'checked' :null }} name="mau52" value="Xám" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau52" value="Xám" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Xám</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[5]->mau_sl[1]->sl }}" class="input-sm" name="soluong52" id="spinner48" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong52" id="spinner48" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[5]->mau_sl[2]->mau == "Đen" ? 'checked' :null }} name="mau53" value="Đen" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau53" value="Đen" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Đen</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[5]->mau_sl[2]->sl }}" class="input-sm" name="soluong53" id="spinner49" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong53" id="spinner49" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[5]->mau_sl[3]->mau == "Bạc" ? 'checked' :null }} name="mau54" value="Bạc" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau54" value="Bạc" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Bạc</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[5]->mau_sl[3]->sl }}" class="input-sm" name="soluong54" id="spinner50" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong54" id="spinner50" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[5]->mau_sl[4]->mau == "Trắng" ? 'checked' :null }} name="mau55" value="Trắng" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau55" value="Trắng" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Trắng</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[5]->mau_sl[4]->sl }}" class="input-sm" name="soluong55" id="spinner51" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong55" id="spinner51" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>

                            </div>
                          </div>
                        </div>
                        @elseif($sanpham->Kieu == 3) {{-- giày - dép --}}
                        <h3 class="header smaller lighter blue">
                          Size - Màu - Số Lượng
                          <small>(Chọn màu và Số lượng theo Size)
                            <span class="help-button" data-rel="popover" data-trigger="hover" data-placement="right" data-content="- Tích chọn Size - Màu - Số lượng (thông tin nào không có vui lòng để trống) " title="" data-original-title="Chú ý !">?</span>
                          </small>
                        </h3>
                        <div class="form-group row">
                          <div class="col-xs-12 col-sm-2">
                            <div class="control-group text-center">
                              <label>
                                @if ($query)
                                <input type="checkbox" name="sizegiay38" class="ace ace-checkbox-2" {{ $sp_chitiet[0]->size == "38" ? 'checked' : null }}  value="38">
                                @else
                                <input name="sizegiay38" class="ace ace-checkbox-2" type="checkbox"  value="38">
                                @endif
                                
                                <span class="lbl blue" style="font-weight: bold"> size 38</span>
                              </label>

                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[0]->mau_sl[0]->mau == "Xanh" ? 'checked' :null }} name="mau56" value="Xanh" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau56" value="Xanh" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Xanh</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[0]->mau_sl[0]->sl }}" class="input-sm" name="soluong56" id="spinner52" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong56" id="spinner52" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[0]->mau_sl[1]->mau == "Đỏ" ? 'checked' :null }} name="mau57" value="Đỏ" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau57" value="Đỏ" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Đỏ</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[0]->mau_sl[1]->sl }}" class="input-sm" name="soluong57" id="spinner53" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong57" id="spinner53" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[0]->mau_sl[2]->mau == "Đen" ? 'checked' :null }} name="mau58" value="Đen" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau58" value="Đen" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Đen</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[0]->mau_sl[2]->sl }}" class="input-sm" name="soluong58" id="spinner54" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong58" id="spinner54" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[0]->mau_sl[3]->mau == "Nâu" ? 'checked' :null }} name="mau59" value="Nâu" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau59" value="Nâu" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Nâu</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[0]->mau_sl[3]->sl }}" class="input-sm" name="soluong59" id="spinner55" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong59" id="spinner55" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[0]->mau_sl[4]->mau == "Trắng" ? 'checked' :null }} name="mau60" value="Trắng" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau60" value="Trắng" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Trắng</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[0]->mau_sl[4]->sl }}" class="input-sm" name="soluong60" id="spinner56" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong60" id="spinner56" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                            </div>
                          </div>
                          <div class="col-xs-12 col-sm-2">
                            <div class="control-group text-center">
                              <label>
                                @if ($query)
                                <input type="checkbox" name="sizegiay39" class="ace ace-checkbox-2" {{ $sp_chitiet[1]->size == "39" ? 'checked' : null }}  value="39">
                                @else
                                <input name="sizegiay39" class="ace ace-checkbox-2" type="checkbox"  value="39">
                                @endif
                                
                                <span class="lbl blue" style="font-weight: bold"> size 39</span>
                              </label>

                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[1]->mau_sl[0]->mau == "Xanh" ? 'checked' :null }} name="mau61" value="Xanh" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau61" value="Xanh" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Xanh</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[1]->mau_sl[0]->sl }}" class="input-sm" name="soluong61" id="spinner57" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong61" id="spinner57" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[1]->mau_sl[1]->mau == "Đỏ" ? 'checked' :null }} name="mau62" value="Đỏ" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau62" value="Đỏ" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Đỏ</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[1]->mau_sl[1]->sl }}" class="input-sm" name="soluong62" id="spinner58" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong62" id="spinner58" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[1]->mau_sl[2]->mau == "Đen" ? 'checked' :null }} name="mau63" value="Đen" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau63" value="Đen" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Đen</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[1]->mau_sl[2]->sl }}" class="input-sm" name="soluong63" id="spinner59" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong63" id="spinner59" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[1]->mau_sl[3]->mau == "Nâu" ? 'checked' :null }} name="mau64" value="Nâu" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau64" value="Nâu" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Nâu</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[1]->mau_sl[3]->sl }}" class="input-sm" name="soluong64" id="spinner60" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong64" id="spinner60" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[1]->mau_sl[4]->mau == "Trắng" ? 'checked' :null }} name="mau65" value="Trắng" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau65" value="Trắng" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Trắng</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[1]->mau_sl[4]->sl }}" class="input-sm" name="soluong65" id="spinner61" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong65" id="spinner61" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                            </div>
                          </div>
                          <div class="col-xs-12 col-sm-2">
                            <div class="control-group text-center">

                              <label>
                                @if ($query)
                                <input type="checkbox" name="sizegiay40" class="ace ace-checkbox-2" {{ $sp_chitiet[2]->size == "40" ? 'checked' : null }}  value="40">
                                @else
                                <input name="sizegiay40" class="ace ace-checkbox-2" type="checkbox"  value="40">
                                @endif
                                
                                <span class="lbl blue" style="font-weight: bold"> size 40</span>
                              </label>

                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[2]->mau_sl[0]->mau == "Xanh" ? 'checked' :null }} name="mau66" value="Xanh" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau66" value="Xanh" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Xanh</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[2]->mau_sl[0]->sl }}" class="input-sm" name="soluong66" id="spinner62" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong66" id="spinner62" />
                                    @endif
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[2]->mau_sl[1]->mau == "Đỏ" ? 'checked' :null }} name="mau67" value="Đỏ" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau67" value="Đỏ" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Đỏ</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[2]->mau_sl[1]->sl }}" class="input-sm" name="soluong67" id="spinner63" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong67" id="spinner63" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[2]->mau_sl[2]->mau == "Đen" ? 'checked' :null }} name="mau68" value="Đen" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau68" value="Đen" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Đen</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[2]->mau_sl[2]->sl }}" class="input-sm" name="soluong68" id="spinner64" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong68" id="spinner64" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[2]->mau_sl[3]->mau == "Nâu" ? 'checked' :null }} name="mau69" value="Nâu" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau69" value="Nâu" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Nâu</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[2]->mau_sl[3]->sl }}" class="input-sm" name="soluong69" id="spinner65" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong69" id="spinner65" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[2]->mau_sl[4]->mau == "Trắng" ? 'checked' :null }} name="mau70" value="Trắng" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau70" value="Trắng" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Trắng</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[2]->mau_sl[4]->sl }}" class="input-sm" name="soluong70" id="spinner66" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong70" id="spinner66" />
                                    @endif
                                  </div>
                                </div>
                              </div><br/>


                            </div>
                          </div>
                          <div class="col-xs-12 col-sm-2">
                            <div class="control-group text-center">
                              <label>
                                @if ($query)
                                <input type="checkbox" name="sizegiay41" class="ace ace-checkbox-2" {{ $sp_chitiet[3]->size == "41" ? 'checked' : null }}  value="41">
                                @else
                                <input name="sizegiay41" class="ace ace-checkbox-2" type="checkbox"  value="41">
                                @endif
                                
                                <span class="lbl blue" style="font-weight: bold"> size 41</span>
                              </label>

                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[3]->mau_sl[0]->mau == "Xanh" ? 'checked' :null }} name="mau71" value="Xanh" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau71" value="Xanh" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Xanh</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[3]->mau_sl[0]->sl }}" class="input-sm" name="soluong71" id="spinner67" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong71" id="spinner67" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[3]->mau_sl[1]->mau == "Đỏ" ? 'checked' :null }} name="mau72" value="Đỏ" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau72" value="Đỏ" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Đỏ</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[3]->mau_sl[1]->sl }}" class="input-sm" name="soluong72" id="spinner68" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong72" id="spinner68" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[3]->mau_sl[2]->mau == "Đen" ? 'checked' :null }} name="mau73" value="Đen" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau73" value="Đen" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Đen</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[3]->mau_sl[2]->sl }}" class="input-sm" name="soluong73" id="spinner69" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong73" id="spinner69" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query) 
                                      <input type="checkbox" {{ $sp_chitiet[3]->mau_sl[3]->mau == "Nâu" ? 'checked' :null }} name="mau74" value="Nâu" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau74" value="Nâu" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Nâu</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[3]->mau_sl[3]->sl }}" class="input-sm" name="soluong74" id="spinner70" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong74" id="spinner70" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[3]->mau_sl[4]->mau == "Trắng" ? 'checked' :null }} name="mau75" value="Trắng" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau75" value="Trắng" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Trắng</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[3]->mau_sl[4]->sl }}" class="input-sm" name="soluong75" id="spinner71" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong75" id="spinner71" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>

                            </div>
                          </div>
                          <div class="col-xs-12 col-sm-2">
                            <div class="control-group text-center">

                              <label>
                                @if ($query)
                                <input type="checkbox" name="sizegiay42" class="ace ace-checkbox-2" {{ $sp_chitiet[4]->size == "42" ? 'checked' : null }}  value="42">
                                @else
                                <input name="sizegiay42" class="ace ace-checkbox-2" type="checkbox"  value="42">
                                @endif
                                
                                <span class="lbl blue" style="font-weight: bold"> size 42</span>
                              </label>

                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[4]->mau_sl[0]->mau == "Xanh" ? 'checked' :null }} name="mau76" value="Xanh" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau76" value="Xanh" class="ace input-lg">
                                    @endif
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Xanh</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[4]->mau_sl[0]->sl }}" class="input-sm" name="soluong76" id="spinner72" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong76" id="spinner72" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[4]->mau_sl[1]->mau == "Đỏ" ? 'checked' :null }} name="mau77" value="Đỏ" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau77" value="Đỏ" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Đỏ</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[4]->mau_sl[1]->sl }}" class="input-sm" name="soluong77" id="spinner73" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong77" id="spinner73" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[4]->mau_sl[2]->mau == "Đen" ? 'checked' :null }} name="mau78" value="Đen" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau78" value="Đen" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Đen</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[4]->mau_sl[2]->sl }}" class="input-sm" name="soluong78" id="spinner74" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong78" id="spinner74" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[4]->mau_sl[3]->mau == "Nâu" ? 'checked' :null }} name="mau79" value="Nâu" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau79" value="Nâu" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Nâu</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[4]->mau_sl[3]->sl }}" class="input-sm" name="soluong79" id="spinner75" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong79" id="spinner75" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[4]->mau_sl[4]->mau == "Trắng" ? 'checked' :null }} name="mau80" value="Trắng" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau80" value="Trắng" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Trắng</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[4]->mau_sl[4]->sl }}" class="input-sm" name="soluong80" id="spinner76" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong80" id="spinner76" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                            </div>
                          </div>
                          <div class="col-xs-12 col-sm-2">
                            <div class="control-group text-center">
                              <label>
                                @if ($query)
                                <input type="checkbox" name="sizegiay43" class="ace ace-checkbox-2" {{ $sp_chitiet[5]->size == "43" ? 'checked' : null }}  value="43">
                                @else
                                <input name="sizegiay43" class="ace ace-checkbox-2" type="checkbox"  value="43">
                                @endif
                                
                                <span class="lbl blue" style="font-weight: bold"> size 43</span>
                              </label>

                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[5]->mau_sl[0]->mau == "Xanh" ? 'checked' :null }} name="mau81" value="Xanh" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau81" value="Xanh" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Xanh</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[5]->mau_sl[0]->sl }}" class="input-sm" name="soluong81" id="spinner77" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong81" id="spinner77" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[5]->mau_sl[1]->mau == "Đỏ" ? 'checked' :null }} name="mau82" value="Đỏ" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau82" value="Đỏ" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Đỏ</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[5]->mau_sl[1]->sl }}" class="input-sm" name="soluong82" id="spinner78" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong82" id="spinner78" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[5]->mau_sl[2]->mau == "Đen" ? 'checked' :null }} name="mau83" value="Đen" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau83" value="Đen" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Đen</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[5]->mau_sl[2]->sl }}" class="input-sm" name="soluong83" id="spinner79" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong83" id="spinner79" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[5]->mau_sl[3]->mau == "Nâu" ? 'checked' :null }} name="mau84" value="Nâu" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau84" value="Nâu" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Nâu</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[5]->mau_sl[3]->sl }}" class="input-sm" name="soluong84" id="spinner80" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong84" id="spinner80" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                              <div class="checkbox">
                                <div class="col-xs-12 col-sm-2">
                                  <label class="block">
                                    @if ($query)
                                      <input type="checkbox" {{ $sp_chitiet[5]->mau_sl[4]->mau == "Trắng" ? 'checked' :null }} name="mau85" value="Trắng" class="ace input-lg">
                                    @else
                                      <input type="checkbox" name="mau85" value="Trắng" class="ace input-lg">
                                    @endif
                                    
                                    <span class="lbl bigger-120" style="    font-size: 11px !important;">Trắng</span>
                                  </label>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                  <div class="ace-spinner middle touch-spinner" style="width: 140px;">
                                    @if ($query)
                                      <input type="text" value="{{ $sp_chitiet[5]->mau_sl[4]->sl }}" class="input-sm" name="soluong85" id="spinner81" />
                                    @else
                                      <input type="text" class="input-sm" name="soluong85" id="spinner81" />
                                    @endif
                                    
                                  </div>
                                </div>
                              </div><br/>
                            </div>
                          </div>
                        </div>
                        @endif

                        <hr/>
                        <div class="form-group">
                          <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="name">Mã Giảm Giá</label>
                          <div class="col-xs-12 col-sm-9">
                            <div class="clearfix">
                              <input type="text" id="name" name="magiamgia" class="col-xs-12 col-sm-5" value="{!! old('magiamgia',isset($query) ? $query->MaGiamGia : null) !!}"  placeholder="Nhập mã giảm giá nếu có..." />
                            </div>
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="name">Giảm Giá Còn:</label>
                          <div class="col-xs-12 col-sm-9">
                            <div class="clearfix">
                              <input type="text" id="name" name="gia" class="col-xs-12 col-sm-5" value="{!! old('gia',isset($query) ? $query->Gia : null) !!}" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')"/>
                            </div>
                          </div>
                        </div>
                        <hr />
                        <div class="wizard-actions">
                          <a href="{{ url('admin/sanpham') }}" class="btn btn-info"><i class="fa fa-reply"></i> Về Danh Sách</a>
                          <button class="btn" type="reset"><i class="ace-icon fa fa-undo bigger-110"></i> Nhập Lại </button>
                          <button type="submit" id="trigger-save" class="btn btn-success submit-form" name="add"><i class="fa fa-save"></i> Thêm Mới </button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div><!-- /.widget-main -->
            </div><!-- /.widget-body -->
          </div>
        </div>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.page-content -->
</div>
</div>
@endsection

@section('script')
<script type="text/javascript">
  jQuery(function($) {

    $('[data-rel=tooltip]').tooltip();

    $('.select2').css('width','200px').select2({allowClear:true})
    .on('change', function(){
      $(this).closest('form').validate().element($(this));
    }); 
    $(document).one('ajaxloadstart.page', function(e) {
      $('[class*=select2]').remove();
    });
    $('#id-disable-check').on('click', function() {
      var inp = $('#form-input-readonly').get(0);
      if(inp.hasAttribute('disabled')) {
        inp.setAttribute('readonly' , 'true');
        inp.removeAttribute('disabled');
        inp.value="This text field is readonly!";
      }
      else {
        inp.setAttribute('disabled' , 'disabled');
        inp.removeAttribute('readonly');
        inp.value="This text field is disabled!";
      }
    });
    if(!ace.vars['touch']) {
      $('.chosen-select').chosen({allow_single_deselect:true}); 

      $(window)
      .off('resize.chosen')
      .on('resize.chosen', function() {
        $('.chosen-select').each(function() {
          var $this = $(this);
          $this.next().css({'width': $this.parent().width()});
        })
      }).trigger('resize.chosen');
          //resize chosen on sidebar collapse/expand
          $(document).on('settings.ace.chosen', function(e, event_name, event_val) {
            if(event_name != 'sidebar_collapsed') return;
            $('.chosen-select').each(function() {
              var $this = $(this);
              $this.next().css({'width': $this.parent().width()});
            })
          });


          $('#chosen-multiple-style .btn').on('click', function(e){
            var target = $(this).find('input[type=radio]');
            var which = parseInt(target.val());
            if(which == 2) $('#form-field-select-4').addClass('tag-input-style');
            else $('#form-field-select-4').removeClass('tag-input-style');
          });
        }


        $('[data-rel=tooltip]').tooltip({container:'body'});
        $('[data-rel=popover]').popover({container:'body'});

        autosize($('textarea[class*=autosize]'));
        
        $('textarea.limited').inputlimiter({
          remText: '%n character%s remaining...',
          limitText: 'max allowed : %n.'
        });

        $.mask.definitions['~']='[+-]';
        $('.input-mask-date').mask('99/99/9999');
        $('.input-mask-phone').mask('(999) 999-9999');
        $('.input-mask-eyescript').mask('~9.99 ~9.99 999');
        $(".input-mask-product").mask("a*-999-a999",{placeholder:" ",completed:function(){alert("You typed the following: "+this.val());}});



        $( "#input-size-slider" ).css('width','200px').slider({
          value:1,
          range: "min",
          min: 1,
          max: 8,
          step: 1,
          slide: function( event, ui ) {
            var sizing = ['', 'input-sm', 'input-lg', 'input-mini', 'input-small', 'input-medium', 'input-large', 'input-xlarge', 'input-xxlarge'];
            var val = parseInt(ui.value);
            $('#form-field-4').attr('class', sizing[val]).attr('placeholder', '.'+sizing[val]);
          }
        });

        $( "#input-span-slider" ).slider({
          value:1,
          range: "min",
          min: 1,
          max: 12,
          step: 1,
          slide: function( event, ui ) {
            var val = parseInt(ui.value);
            $('#form-field-5').attr('class', 'col-xs-'+val).val('.col-xs-'+val);
          }
        });


        
        //"jQuery UI Slider"
        //range slider tooltip example
        $( "#slider-range" ).css('height','200px').slider({
          orientation: "vertical",
          range: true,
          min: 0,
          max: 100,
          values: [ 17, 67 ],
          slide: function( event, ui ) {
            var val = ui.values[$(ui.handle).index()-1] + "";

            if( !ui.handle.firstChild ) {
              $("<div class='tooltip right in' style='display:none;left:16px;top:-6px;'><div class='tooltip-arrow'></div><div class='tooltip-inner'></div></div>")
              .prependTo(ui.handle);
            }
            $(ui.handle.firstChild).show().children().eq(1).text(val);
          }
        }).find('span.ui-slider-handle').on('blur', function(){
          $(this.firstChild).hide();
        });
        
        
        $( "#slider-range-max" ).slider({
          range: "max",
          min: 1,
          max: 10,
          value: 100
        });
        
        $( "#slider-eq > span" ).css({width:'90%', 'float':'left', margin:'15px'}).each(function() {
          // read initial values from markup and remove that
          var value = parseInt( $( this ).text(), 10 );
          $( this ).empty().slider({
            value: value,
            range: "min",
            animate: true
            
          });
        });
        
        $("#slider-eq > span.ui-slider-purple").slider('disable');//disable third item

        
        $('#id-input-file-1 , #id-input-file-2').ace_file_input({
          no_file:'No File ...',
          btn_choose:'Choose',
          btn_change:'Change',
          droppable:false,
          onchange:null,
          thumbnail:false //| true | large
          //whitelist:'gif|png|jpg|jpeg'
          //blacklist:'exe|php'
          //onchange:''
          //
        });
        //pre-show a file name, for example a previously selected file
        //$('#id-input-file-1').ace_file_input('show_file_list', ['myfile.txt'])


        $('#id-input-file-3').ace_file_input({
          style: 'well',
          btn_choose: 'Bấm Ctrl Để Chọn Nhiều Ảnh',
          btn_change: null,
          no_icon: 'ace-icon fa fa-cloud-upload',
          droppable: true,
          thumbnail: 'small'//large | fit
          //,icon_remove:null//set null, to hide remove/reset button
          /**,before_change:function(files, dropped) {
            //Check an example below
            //or examples/file-upload.html
            return true;
          }*/
          /**,before_remove : function() {
            return true;
          }*/
          ,
          preview_error : function(filename, error_code) {
            //name of the file that failed
            //error_code values
            //1 = 'FILE_LOAD_FAILED',
            //2 = 'IMAGE_LOAD_FAILED',
            //3 = 'THUMBNAIL_FAILED'
            //alert(error_code);
          }

        }).on('change', function(){
          //console.log($(this).data('ace_input_files'));
          //console.log($(this).data('ace_input_method'));
        });
        
        
        //$('#id-input-file-3')
        //.ace_file_input('show_file_list', [
          //{type: 'image', name: 'name of image', path: 'http://path/to/image/for/preview'},
          //{type: 'file', name: 'hello.txt'}
        //]);


        //dynamically change allowed formats by changing allowExt && allowMime function
        $('#id-file-format').removeAttr('checked').on('change', function() {
          var whitelist_ext, whitelist_mime;
          var btn_choose
          var no_icon
          if(this.checked) {
            btn_choose = "Bấm Ctrl Để Chọn Nhiều Ảnh";
            no_icon = "ace-icon fa fa-picture-o";

            whitelist_ext = ["jpeg", "jpg", "png", "gif" , "bmp"];
            whitelist_mime = ["image/jpg", "image/jpeg", "image/png", "image/gif", "image/bmp"];
          }
          else {
            btn_choose = "Bấm Ctrl Để Chọn Nhiều Ảnh";
            no_icon = "ace-icon fa fa-cloud-upload";
            
            whitelist_ext = null;//all extensions are acceptable
            whitelist_mime = null;//all mimes are acceptable
          }
          var file_input = $('#id-input-file-3');
          file_input
          .ace_file_input('update_settings',
          {
            'btn_choose': btn_choose,
            'no_icon': no_icon,
            'allowExt': whitelist_ext,
            'allowMime': whitelist_mime
          })
          file_input.ace_file_input('reset_input');
          
          file_input
          .off('file.error.ace')
          .on('file.error.ace', function(e, info) {
            //console.log(info.file_count);//number of selected files
            //console.log(info.invalid_count);//number of invalid files
            //console.log(info.error_list);//a list of errors in the following format
            
            //info.error_count['ext']
            //info.error_count['mime']
            //info.error_count['size']
            
            //info.error_list['ext']  = [list of file names with invalid extension]
            //info.error_list['mime'] = [list of file names with invalid mimetype]
            //info.error_list['size'] = [list of file names with invalid size]
            
            
            /**
            if( !info.dropped ) {
              //perhapse reset file field if files have been selected, and there are invalid files among them
              //when files are dropped, only valid files will be added to our file array
              e.preventDefault();//it will rest input
            }
            */
            
            
            //if files have been selected (not dropped), you can choose to reset input
            //because browser keeps all selected files anyway and this cannot be changed
            //we can only reset file field to become empty again
            //on any case you still should check files with your server side script
            //because any arbitrary file can be uploaded by user and it's not safe to rely on browser-side measures
          });
          
          
          /**
          file_input
          .off('file.preview.ace')
          .on('file.preview.ace', function(e, info) {
            console.log(info.file.width);
            console.log(info.file.height);
            e.preventDefault();//to prevent preview
          });
          */

        });

        $('#spinner1').ace_spinner({value:0,min:0,max:200,step:10, btn_up_class:'btn-info' , btn_down_class:'btn-info'})
        .closest('.ace-spinner')
        .on('changed.fu.spinbox', function(){
          //console.log($('#spinner1').val())
        }); 
        $('#spinner2').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner3').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});


        $('#spinner4').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner5').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner6').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner7').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner8').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner9').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner10').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner11').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner12').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner13').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner14').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner15').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner16').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner17').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner18').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner19').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner20').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner21').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner22').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner23').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner24').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner25').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner26').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner27').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner28').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner29').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner30').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner31').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner32').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner33').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner34').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner35').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner36').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner37').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner38').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner39').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner40').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner41').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner42').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner43').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner44').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner45').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner46').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner47').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner48').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner49').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner50').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner51').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner52').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner53').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner54').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner55').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner56').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner57').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});

        $('#spinner58').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner59').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner60').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner61').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner62').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner63').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner64').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner65').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner66').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner67').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner68').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner69').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner70').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner71').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});

        $('#spinner72').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner73').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner74').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner75').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner76').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner77').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner78').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner79').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner80').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner81').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner82').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner83').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});


        //$('#spinner1').ace_spinner('disable').ace_spinner('value', 11);
        //or
        //$('#spinner1').closest('.ace-spinner').spinner('disable').spinner('enable').spinner('value', 11);//disable, enable or change value
        //$('#spinner1').closest('.ace-spinner').spinner('value', 0);//reset to 0


        //datepicker plugin
        //link
        $('.date-picker').datepicker({
          autoclose: true,
          todayHighlight: true
        })
        //show datepicker when clicking on the icon
        .next().on(ace.click_event, function(){
          $(this).prev().focus();
        });

        //or change it into a date range picker
        $('.input-daterange').datepicker({autoclose:true});


        //to translate the daterange picker, please copy the "examples/daterange-fr.js" contents here before initialization
        $('input[name=date-range-picker]').daterangepicker({
          'applyClass' : 'btn-sm btn-success',
          'cancelClass' : 'btn-sm btn-default',
          locale: {
            applyLabel: 'Apply',
            cancelLabel: 'Cancel',
          }
        })
        .prev().on(ace.click_event, function(){
          $(this).next().focus();
        });


        $('#timepicker1').timepicker({
          minuteStep: 1,
          showSeconds: true,
          showMeridian: false,
          disableFocus: true,
          icons: {
            up: 'fa fa-chevron-up',
            down: 'fa fa-chevron-down'
          }
        }).on('focus', function() {
          $('#timepicker1').timepicker('showWidget');
        }).next().on(ace.click_event, function(){
          $(this).prev().focus();
        });
        
        

        
        if(!ace.vars['old_ie']) $('#date-timepicker1').datetimepicker({
         //format: 'MM/DD/YYYY h:mm:ss A',//use this option to display seconds
         icons: {
          time: 'fa fa-clock-o',
          date: 'fa fa-calendar',
          up: 'fa fa-chevron-up',
          down: 'fa fa-chevron-down',
          previous: 'fa fa-chevron-left',
          next: 'fa fa-chevron-right',
          today: 'fa fa-arrows ',
          clear: 'fa fa-trash',
          close: 'fa fa-times'
        }
      }).next().on(ace.click_event, function(){
        $(this).prev().focus();
      });


      $('#colorpicker1').colorpicker();
        //$('.colorpicker').last().css('z-index', 2000);//if colorpicker is inside a modal, its z-index should be higher than modal'safe

        $('#simple-colorpicker-1').ace_colorpicker();
        $('#simple-colorpicker-2').ace_colorpicker();
        $('#simple-colorpicker-3').ace_colorpicker();
        $('#simple-colorpicker-4').ace_colorpicker();
        $('#simple-colorpicker-5').ace_colorpicker();
        $('#simple-colorpicker-6').ace_colorpicker();

        $('#simple-colorpicker-7').ace_colorpicker();
        $('#simple-colorpicker-8').ace_colorpicker();
        $('#simple-colorpicker-9').ace_colorpicker();
        $('#simple-colorpicker-10').ace_colorpicker();
        $('#simple-colorpicker-11').ace_colorpicker();
        $('#simple-colorpicker-12').ace_colorpicker();
        $('#simple-colorpicker-13').ace_colorpicker();
        $('#simple-colorpicker-14').ace_colorpicker();
        $('#simple-colorpicker-15').ace_colorpicker();
        $('#simple-colorpicker-16').ace_colorpicker();
        $('#simple-colorpicker-17').ace_colorpicker();
        $('#simple-colorpicker-18').ace_colorpicker();
        $('#simple-colorpicker-19').ace_colorpicker();
        $('#simple-colorpicker-20').ace_colorpicker();
        $('#simple-colorpicker-21').ace_colorpicker();
        $('#simple-colorpicker-22').ace_colorpicker();
        $('#simple-colorpicker-23').ace_colorpicker();
        $('#simple-colorpicker-24').ace_colorpicker();
        $('#simple-colorpicker-25').ace_colorpicker();
        $('#simple-colorpicker-26').ace_colorpicker();
        $('#simple-colorpicker-27').ace_colorpicker();
        $('#simple-colorpicker-28').ace_colorpicker();
        $('#simple-colorpicker-29').ace_colorpicker();
        $('#simple-colorpicker-30').ace_colorpicker();
        $('#simple-colorpicker-31').ace_colorpicker();
        $('#simple-colorpicker-32').ace_colorpicker();
        $('#simple-colorpicker-33').ace_colorpicker();
        $('#simple-colorpicker-34').ace_colorpicker();
        $('#simple-colorpicker-35').ace_colorpicker();
        $('#simple-colorpicker-36').ace_colorpicker();
        $('#simple-colorpicker-37').ace_colorpicker();
        $('#simple-colorpicker-38').ace_colorpicker();
        $('#simple-colorpicker-39').ace_colorpicker();
        $('#simple-colorpicker-40').ace_colorpicker();
        $('#simple-colorpicker-41').ace_colorpicker();
        $('#simple-colorpicker-42').ace_colorpicker();
        $('#simple-colorpicker-43').ace_colorpicker();
        $('#simple-colorpicker-44').ace_colorpicker();

        $('#simple-colorpicker-45').ace_colorpicker();
        $('#simple-colorpicker-46').ace_colorpicker();
        $('#simple-colorpicker-47').ace_colorpicker();
        $('#simple-colorpicker-48').ace_colorpicker();
        $('#simple-colorpicker-49').ace_colorpicker();
        $('#simple-colorpicker-50').ace_colorpicker();
        $('#simple-colorpicker-51').ace_colorpicker();
        $('#simple-colorpicker-52').ace_colorpicker();
        $('#simple-colorpicker-53').ace_colorpicker();
        $('#simple-colorpicker-54').ace_colorpicker();
        $('#simple-colorpicker-55').ace_colorpicker();
        $('#simple-colorpicker-56').ace_colorpicker();
        $('#simple-colorpicker-57').ace_colorpicker();
        $('#simple-colorpicker-58').ace_colorpicker();
        $('#simple-colorpicker-59').ace_colorpicker();
        $('#simple-colorpicker-60').ace_colorpicker();
        $('#simple-colorpicker-61').ace_colorpicker();
        $('#simple-colorpicker-62').ace_colorpicker();
        $('#simple-colorpicker-63').ace_colorpicker();
        $('#simple-colorpicker-64').ace_colorpicker();
        $('#simple-colorpicker-65').ace_colorpicker();

        $('#simple-colorpicker-66').ace_colorpicker();
        $('#simple-colorpicker-67').ace_colorpicker();
        $('#simple-colorpicker-68').ace_colorpicker();
        $('#simple-colorpicker-69').ace_colorpicker();
        $('#simple-colorpicker-70').ace_colorpicker();
        $('#simple-colorpicker-71').ace_colorpicker();
        $('#simple-colorpicker-72').ace_colorpicker();
        $('#simple-colorpicker-73').ace_colorpicker();
        $('#simple-colorpicker-74').ace_colorpicker();
        $('#simple-colorpicker-75').ace_colorpicker();
        $('#simple-colorpicker-76').ace_colorpicker();
        $('#simple-colorpicker-77').ace_colorpicker();
        $('#simple-colorpicker-78').ace_colorpicker();
        $('#simple-colorpicker-79').ace_colorpicker();
        $('#simple-colorpicker-80').ace_colorpicker();
        $('#simple-colorpicker-81').ace_colorpicker();
        $('#simple-colorpicker-82').ace_colorpicker();
        $('#simple-colorpicker-83').ace_colorpicker();
        $('#simple-colorpicker-84').ace_colorpicker();
        $('#simple-colorpicker-85').ace_colorpicker();
        $('#simple-colorpicker-86').ace_colorpicker();




        // $('#simple-colorpicker-1').ace_colorpicker();
        // $('#simple-colorpicker-1').ace_colorpicker();
        // $('#simple-colorpicker-1').ace_colorpicker();
        // $('#simple-colorpicker-1').ace_colorpicker();

        //$('#simple-colorpicker-1').ace_colorpicker('pick', 2);//select 2nd color
        //$('#simple-colorpicker-1').ace_colorpicker('pick', '#fbe983');//select #fbe983 color
        //var picker = $('#simple-colorpicker-1').data('ace_colorpicker')
        //picker.pick('red', true);//insert the color if it doesn't exist


        $(".knob").knob();
        
        
        var tag_input = $('#form-field-tags');
        try{
          tag_input.tag(
          {
            placeholder:tag_input.attr('placeholder'),
            //enable typeahead by specifying the source array
            source: ace.vars['US_STATES'],//defined in ace.js >> ace.enable_search_ahead
            /**
            //or fetch data from database, fetch those that match "query"
            source: function(query, process) {
              $.ajax({url: 'remote_source.php?q='+encodeURIComponent(query)})
              .done(function(result_items){
              process(result_items);
              });
            }
            */
          }
          )

          //programmatically add/remove a tag
          var $tag_obj = $('#form-field-tags').data('tag');
          $tag_obj.add('Programmatically Added');
          
          var index = $tag_obj.inValues('some tag');
          $tag_obj.remove(index);
        }
        catch(e) {
          //display a textarea for old IE, because it doesn't support this plugin or another one I tried!
          tag_input.after('<textarea id="'+tag_input.attr('id')+'" name="'+tag_input.attr('name')+'" rows="3">'+tag_input.val()+'</textarea>').remove();
          //autosize($('#form-field-tags'));
        }
        
        
        /////////
        $('#modal-form input[type=file]').ace_file_input({
          style:'well',
          btn_choose:'Drop files here or click to choose',
          btn_change:null,
          no_icon:'ace-icon fa fa-cloud-upload',
          droppable:true,
          thumbnail:'large'
        })
        
        //chosen plugin inside a modal will have a zero width because the select element is originally hidden
        //and its width cannot be determined.
        //so we set the width after modal is show
        $('#modal-form').on('shown.bs.modal', function () {
          if(!ace.vars['touch']) {
            $(this).find('.chosen-container').each(function(){
              $(this).find('a:first-child').css('width' , '210px');
              $(this).find('.chosen-drop').css('width' , '210px');
              $(this).find('.chosen-search input').css('width' , '200px');
            });
          }
        })
        /**
        //or you can activate the chosen plugin after modal is shown
        //this way select element becomes visible with dimensions and chosen works as expected
        $('#modal-form').on('shown', function () {
          $(this).find('.modal-chosen').chosen();
        })
        */

        
        
        $(document).one('ajaxloadstart.page', function(e) {
          autosize.destroy('textarea[class*=autosize]')
          
          $('.limiterBox,.autosizejs').remove();
          $('.daterangepicker.dropdown-menu,.colorpicker.dropdown-menu,.bootstrap-datetimepicker-widget.dropdown-menu').remove();
        });
      })
    </script>
    @endsection

