@extends('backend.layout.master')
@section('content')
<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="{{ url('admin/pages') }}">Home</a>
        </li>

        <li>
          <a href="javascript:voi"> Sản Phẩm </a>
        </li>
        <li class="active"> Chi Tiết Sản Phẩm</li>
      </ul><!-- /.breadcrumb -->

      <div class="nav-search" id="nav-search">
        <form class="form-search">
          <span class="input-icon">
            <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
            <i class="ace-icon fa fa-search nav-search-icon"></i>
          </span>
        </form>
      </div><!-- /.nav-search -->
    </div>

    <div class="page-content">
      <div class="row">
        <div class="col-xs-12">
          <div class="row">
            <div class="col-xs-12">
              <div class="clearfix">
                <a href="{{ url('admin/sanpham') }}">
                  <div class="pull-text col-xs-3" style="margin-bottom: 5px">
                    <button class="btn btn-success"><i class="ace-icon glyphicon glyphicon-list"></i> Danh Sách </button>
                  </div>
                </a>
                <div class="pull-text col-xs-6">
                  @include('backend.block.alert')
                </div>
                <div class="pull-right tableTools-container col-xs-2"></div>
              </div>
              <div class="table-header">
                Chi Tiết Sản Phẩm: {{ $chitiet->Name }}
              </div>

              <div>
                <table id="dynamic-table" class="table table-striped table-bordered table-hover">
                  <thead>
                    <tr>
                      <th>Mã Sản Phẩm</th>
                      <th>Ảnh Mô Tả</th>
                      <th>Tên Sản Phẩm</th>
                      <th>Giá</th>
                      <th>Giá Sale</th>
                      <th>Từ Khóa Seo</th>
                      <th>Tiêu Đề Seo</th>
                      <th>Mô Tả Seo</th>
                      <th>Thương Hiệu</th>
                      <th>Mô Tả</th>
                    </tr>
                  </thead>

                  <tbody>
                    <tr>
                      <td>{{ $chitiet->MaSp }}</td>
                      <td><img style="max-width: 300px;" src="{{ asset('uploads/sanpham/'.$chitiet->Image) }}"></td>
                      <td>{!! $chitiet->Name !!}</td>
                      <td>{!! $chitiet->Gia !!}</td>
                      <td>{!! $chitiet->GiaSale !!}</td>
                      <td>{!! $chitiet->TuKhoaSeo !!}</td>
                      <td>{!! $chitiet->TieuDeSeo !!}</td>
                      <td>{!! $chitiet->MoTaSeo !!}</td>
                      <td>{!! $chitiet->ThuongHieu !!}</td>
                      <td>{!! $chitiet->MoTa !!}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.page-content -->
  </div>
</div><!-- /.main-content -->
@endsection

