<div class="row">
    <div class="col-md-3"> </div>
    @if(count($errors) > 0)
    <div class="alert alert-danger col-md-5" style="margin-top: 1em;border-radius: 5px">
        <ul style="list-style: none">
            @foreach($errors->all() as $err)
            <li><i class="ace-icon fa fa-exclamation-triangle"></i> {{ $err }} </li>
            @endforeach
        </ul>
    </div>
    @endif
    <div class="col-md-3"> </div>
</div>