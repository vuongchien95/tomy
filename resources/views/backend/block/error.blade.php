<div class="row">
  <div class="col-md-3"> </div>
  @if (Session::has('thog_bao_loi'))
  <div class="alert alert-danger col-md-10 text-center" style="border-radius: 5px">
    <i class="ace-icon fa fa-exclamation-triangle"></i> {{ Session::get('thog_bao_loi') }}
  </div>
  @endif
  <div class="col-md-3"> </div>
</div>