  @extends('backend.layout.master')
  @section('content')
  <div class="main-content">
  	<div class="main-content-inner">
  		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
  			<ul class="breadcrumb">
  				<li>
  					<i class="ace-icon fa fa-home home-icon"></i>
  					<a href="{{ url('admin/pages') }}">Home</a>
  				</li>
  				<li class="active">Cấu hình Banner cho website</li>
  			</ul><!-- /.breadcrumb -->

  			<div class="nav-search" id="nav-search">
  				<form class="form-search">
  					<span class="input-icon">
  						<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
  						<i class="ace-icon fa fa-search nav-search-icon"></i>
  					</span>
  				</form>
  			</div><!-- /.nav-search -->
  		</div>

  		<div class="page-content">

  			<div class="row">
  				<div class="col-xs-12">
  					<!-- PAGE CONTENT BEGINS -->
  					<form action="" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
  						{{ csrf_field() }}
  						<div class="form-group">
  							<input type="hidden" class="inp-id" name="id[]" value="">
  							<div class="row">
  								<div class="col-sm-3"> </div>
  								<div class="col-sm-6">
  									<div class="widget-box">
  										<div class="widget-header">
  											<h4 class="widget-title">Banner Website</h4>
  											<div class="widget-toolbar">
  												<a href="#" data-action="collapse">
  													<i class="ace-icon fa fa-chevron-up"></i>
  												</a>
  											</div>
  										</div>

  										<div class="widget-body">
  											<div class="widget-main">
  												<div class="form-group">
  													<div class="col-xs-12">
  														<input multiple="" name="avatar[]" type="file" id="id-input-file-3" />
  													</div>
  												</div>
  											</div>
  										</div>
  									</div>
  								</div>
  								<div class="col-sm-3">
  									<div class="clearfix form-actions" style="background-color: #fff; border-top:none">
  										<div class="col-md-offset-3">
  											<button class="btn btn-info" type="submit" style="margin-bottom: 2em">
  												<i class="ace-icon fa fa-check bigger-110"></i>
  												Lưu Thay Đổi
  											</button>

  											&nbsp; &nbsp; &nbsp;
  											<a href="{{ url('admin/pages') }}" class="btn btn-danger">
  												<i class="fa fa-reply"></i>
  												Hủy Bỏ
  											</a>
  										</div>
  									</div>
  								</div>
  							</div>

  							
  							<div class="row">
  								@if(count($banner) > 0)
  								@foreach($banner as $items)
  								<div class="col-md-4">
  									<img src="{{ asset('uploads/images/banner/'.$items->Image) }}" style="width: 356px;height: 191px;padding: 1em;border: 1px solid #c8bdbd; border-radius: 5px;margin: 1em 0 0 2em;">
  									&nbsp; &nbsp; &nbsp;
  									<a href="{{ route('admin.cauhinhweb.delBanner',$items->id) }}" class="btn btn-danger" style="margin:1em 0 0 2em;border-radius:5px;padding: 4px;">
  										<i class="ace-icon glyphicon glyphicon-remove"></i>
  										Xóa 
  									</a>

                    <button type="button" class="btn btn-light" style="margin-top: 14px;padding: 4px;">Trạng Thái
                      <input name="switch-field-1" class="ace ace-switch ace-switch-6" {{ $items->AnHien == 1 ? 'checked' : null }} onchange="AnHien({{  $items->id}})" type="checkbox">
                      <span class="lbl" style="margin: -8px 4px;"></span>
                    </button>

                    
                  </div>
                  @endforeach
                  @endif
                </div>
              </div>
            </form>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.page-content -->
    </div>
  </div>
  @endsection

  @section('script')
  <script type="text/javascript">
    function AnHien(id){
      $.get('admin/cauhinhweb/bannerAjax/'+id,function(data){
        // location.reload(true);
        alert('Thay đổi trạng thái thành công !!');
      });
    }

  </script>
  <script type="text/javascript">
  	jQuery(function($) {
  		$('#id-disable-check').on('click', function() {
  			var inp = $('#form-input-readonly').get(0);
  			if(inp.hasAttribute('disabled')) {
  				inp.setAttribute('readonly' , 'true');
  				inp.removeAttribute('disabled');
  				inp.value="This text field is readonly!";
  			}
  			else {
  				inp.setAttribute('disabled' , 'disabled');
  				inp.removeAttribute('readonly');
  				inp.value="This text field is disabled!";
  			}
  		});


  		if(!ace.vars['touch']) {
  			$('.chosen-select').chosen({allow_single_deselect:true}); 
  					//resize the chosen on window resize

  					$(window)
  					.off('resize.chosen')
  					.on('resize.chosen', function() {
  						$('.chosen-select').each(function() {
  							var $this = $(this);
  							$this.next().css({'width': $this.parent().width()});
  						})
  					}).trigger('resize.chosen');
  					//resize chosen on sidebar collapse/expand
  					$(document).on('settings.ace.chosen', function(e, event_name, event_val) {
  						if(event_name != 'sidebar_collapsed') return;
  						$('.chosen-select').each(function() {
  							var $this = $(this);
  							$this.next().css({'width': $this.parent().width()});
  						})
  					});


  					$('#chosen-multiple-style .btn').on('click', function(e){
  						var target = $(this).find('input[type=radio]');
  						var which = parseInt(target.val());
  						if(which == 2) $('#form-field-select-4').addClass('tag-input-style');
  						else $('#form-field-select-4').removeClass('tag-input-style');
  					});
  				}


  				$('[data-rel=tooltip]').tooltip({container:'body'});
  				$('[data-rel=popover]').popover({container:'body'});

  				autosize($('textarea[class*=autosize]'));
  				
  				$('textarea.limited').inputlimiter({
  					remText: '%n character%s remaining...',
  					limitText: 'max allowed : %n.'
  				});

  				$.mask.definitions['~']='[+-]';
  				$('.input-mask-date').mask('99/99/9999');
  				$('.input-mask-phone').mask('(999) 999-9999');
  				$('.input-mask-eyescript').mask('~9.99 ~9.99 999');
  				$(".input-mask-product").mask("a*-999-a999",{placeholder:" ",completed:function(){alert("You typed the following: "+this.val());}});



  				$( "#input-size-slider" ).css('width','200px').slider({
  					value:1,
  					range: "min",
  					min: 1,
  					max: 20,
  					step: 1,
  					slide: function( event, ui ) {
  						var sizing = ['', 'input-sm', 'input-lg', 'input-mini', 'input-small', 'input-medium', 'input-large', 'input-xlarge', 'input-xxlarge'];
  						var val = parseInt(ui.value);
  						$('#form-field-4').attr('class', sizing[val]).attr('placeholder', '.'+sizing[val]);
  					}
  				});

  				$( "#input-span-slider" ).slider({
  					value:1,
  					range: "min",
  					min: 1,
  					max: 20,
  					step: 1,
  					slide: function( event, ui ) {
  						var val = parseInt(ui.value);
  						$('#form-field-5').attr('class', 'col-xs-'+val).val('.col-xs-'+val);
  					}
  				});


  				
  				//"jQuery UI Slider"
  				//range slider tooltip example
  				$( "#slider-range" ).css('height','200px').slider({
  					orientation: "vertical",
  					range: true,
  					min: 0,
  					max: 100,
  					values: [ 17, 67 ],
  					slide: function( event, ui ) {
  						var val = ui.values[$(ui.handle).index()-1] + "";

  						if( !ui.handle.firstChild ) {
  							$("<div class='tooltip right in' style='display:none;left:16px;top:-6px;'><div class='tooltip-arrow'></div><div class='tooltip-inner'></div></div>")
  							.prependTo(ui.handle);
  						}
  						$(ui.handle.firstChild).show().children().eq(1).text(val);
  					}
  				}).find('span.ui-slider-handle').on('blur', function(){
  					$(this.firstChild).hide();
  				});
  				
  				
  				$( "#slider-range-max" ).slider({
  					range: "max",
  					min: 1,
  					max: 20,
  					value: 2
  				});
  				
  				$( "#slider-eq > span" ).css({width:'90%', 'float':'left', margin:'15px'}).each(function() {
  					// read initial values from markup and remove that
  					var value = parseInt( $( this ).text(), 10 );
  					$( this ).empty().slider({
  						value: value,
  						range: "min",
  						animate: true
  						
  					});
  				});
  				
  				$("#slider-eq > span.ui-slider-purple").slider('disable');//disable third item

  				
  				$('#id-input-file-1 , #id-input-file-2').ace_file_input({
  					no_file:'No File ...',
  					btn_choose:'Choose',
  					btn_change:'Change',
  					droppable:false,
  					onchange:null,
  					thumbnail:false //| true | large
  					//whitelist:'gif|png|jpg|jpeg'
  					//blacklist:'exe|php'
  					//onchange:''
  					//
  				});
  				//pre-show a file name, for example a previously selected file
  				//$('#id-input-file-1').ace_file_input('show_file_list', ['myfile.txt'])


  				$('#id-input-file-3').ace_file_input({
  					style: 'well',
  					btn_choose: 'Bấm Ctrl Để Chọn Nhiều Ảnh',
  					btn_change: null,
  					no_icon: 'ace-icon fa fa-cloud-upload',
  					droppable: true,
  					thumbnail: 'small'//large | fit
  					//,icon_remove:null//set null, to hide remove/reset button
  					/**,before_change:function(files, dropped) {
  						//Check an example below
  						//or examples/file-upload.html
  						return true;
  					}*/
  					/**,before_remove : function() {
  						return true;
  					}*/
  					,
  					preview_error : function(filename, error_code) {
  						//name of the file that failed
  						//error_code values
  						//1 = 'FILE_LOAD_FAILED',
  						//2 = 'IMAGE_LOAD_FAILED',
  						//3 = 'THUMBNAIL_FAILED'
  						//alert(error_code);
  					}

  				}).on('change', function(){
  					// console.log($(this).data('ace_input_files'));
  					// console.log($(this).data('ace_input_method'));
  				});
  				
  				
  				// $('#id-input-file-3')
  				// .ace_file_input('show_file_list', [
  				// 	{type: 'image', name: 'name of image', path: 'http://path/to/image/for/preview'},
  				// 	{type: 'file', name: 'hello.txt'}
  				// ]);

  				
  				

  				//dynamically change allowed formats by changing allowExt && allowMime function
  				$('#id-file-format').removeAttr('checked').on('change', function() {
  					var whitelist_ext, whitelist_mime;
  					var btn_choose
  					var no_icon
  					if(this.checked) {
  						btn_choose = "Bấm Ctrl Để Chọn Nhiều Ảnh";
  						no_icon = "ace-icon fa fa-picture-o";

  						whitelist_ext = ["jpeg", "jpg", "png", "gif" , "bmp"];
  						whitelist_mime = ["image/jpg", "image/jpeg", "image/png", "image/gif", "image/bmp"];
  					}
  					else {
  						btn_choose = "Bấm Ctrl Để Chọn Nhiều Ảnh";
  						no_icon = "ace-icon fa fa-cloud-upload";
  						
  						whitelist_ext = null;//all extensions are acceptable
  						whitelist_mime = null;//all mimes are acceptable
  					}
  					var file_input = $('#id-input-file-3');
  					file_input
  					.ace_file_input('update_settings',
  					{
  						'btn_choose': btn_choose,
  						'no_icon': no_icon,
  						'allowExt': whitelist_ext,
  						'allowMime': whitelist_mime
  					})
  					file_input.ace_file_input('reset_input');
  					
  					file_input
  					.off('file.error.ace')
  					.on('file.error.ace', function(e, info) {
  						//console.log(info.file_count);//number of selected files
  						//console.log(info.invalid_count);//number of invalid files
  						//console.log(info.error_list);//a list of errors in the following format
  						
  						//info.error_count['ext']
  						//info.error_count['mime']
  						//info.error_count['size']
  						
  						//info.error_list['ext']  = [list of file names with invalid extension]
  						//info.error_list['mime'] = [list of file names with invalid mimetype]
  						//info.error_list['size'] = [list of file names with invalid size]
  						
  						
  						/**
  						if( !info.dropped ) {
  							//perhapse reset file field if files have been selected, and there are invalid files among them
  							//when files are dropped, only valid files will be added to our file array
  							e.preventDefault();//it will rest input
  						}
  						*/
  						
  						
  						//if files have been selected (not dropped), you can choose to reset input
  						//because browser keeps all selected files anyway and this cannot be changed
  						//we can only reset file field to become empty again
  						//on any case you still should check files with your server side script
  						//because any arbitrary file can be uploaded by user and it's not safe to rely on browser-side measures
  					});
  					
  					
  					/**
  					file_input
  					.off('file.preview.ace')
  					.on('file.preview.ace', function(e, info) {
  						console.log(info.file.width);
  						console.log(info.file.height);
  						e.preventDefault();//to prevent preview
  					});
  					*/

  				});

  				
  				$(document).one('ajaxloadstart.page', function(e) {
  					autosize.destroy('textarea[class*=autosize]')
  					
  					$('.limiterBox,.autosizejs').remove();
  					$('.daterangepicker.dropdown-menu,.colorpicker.dropdown-menu,.bootstrap-datetimepicker-widget.dropdown-menu').remove();
  				});

        });
      </script>
      @endsection

