@extends('backend.layout.master')
@section('content')
<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="{{ url('admin/pages') }}">Home</a>
        </li>

        <li>
          <a href="javascript:voi"> Chi Tiết Đơn Hàng </a>
        </li>
      </ul><!-- /.breadcrumb -->

      <div class="nav-search" id="nav-search">
        <form class="form-search">
          <span class="input-icon">
            <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
            <i class="ace-icon fa fa-search nav-search-icon"></i>
          </span>
        </form>
      </div><!-- /.nav-search -->
    </div>

    <div class="page-content">
      <div class="row">
        <div class="col-xs-12">
          <div class="row">
            <div class="col-xs-12">
              <div class="clearfix">
                <a href="{{ url('admin/donhang') }}">
                  <div class="pull-text col-xs-3" style="margin-bottom: 5px">
                    <button class="btn btn-success"><i class="ace-icon glyphicon glyphicon-list"></i> Danh Sách Đơn Hàng </button>
                  </div>
                </a>
                <div class="pull-text col-xs-6">
                  @include('backend.block.alert')
                </div>
                <div class="pull-right tableTools-container col-xs-2"></div>
              </div>
              

              <div class="row">
                <div class="col-xs-12">
                  <table id="simple-table" class="table  table-bordered table-hover">
                    <div class="table-header">
                      Thông Tin Khách Hàng
                    </div>
                    <tbody>
                      <tr>
                        <td><div>Tên Khách Hàng</div></td>
                        <td class="hidden-480">{!! $user->name !!}</td>
                      </tr>
                      <tr>
                        <td><div>Ngày Đặt Hàng</div></td>
                        <td class="hidden-480">{!! $donhang->NgayMua !!}</td>
                      </tr>
                      <tr>
                        <td><div>Số Điện Thoại </div></td>
                        <td class="hidden-480">{!! $user->phone !!}</td>
                      </tr>
                      <tr>
                        <td><div>Email</div></td>
                        <td class="hidden-480">{!! $user->email !!}</td>
                      </tr>
                      <tr>
                        <td><div>Địa Chỉ</div></td>
                        <td class="hidden-480">{!! $user->address !!}</td>
                      </tr>
                      <tr>
                        <td><div>Tổng Tiền</div></td>
                        <td class="hidden-480">{!! $donhang->TongTien !!} vnđ</td>
                      </tr>
                      <tr>
                        <td><div>Phương Thức Thanh Toán</div></td>
                        <td class="hidden-480">{!! $donhang->ThanhToan !!}</td>
                      </tr>
                      <tr>
                        <td><div>Chi Chú</div></td>
                        <td class="hidden-480">{!! $donhang->TinNhan !!}</td>
                      </tr>

                    </tbody>
                  </table>

                  <table id="simple-table" class="table  table-bordered table-hover">
                    <div class="table-header">
                      Chi Tiết Đơn Hàng
                    </div>
                    <thead>
                      <tr>
                        <th>Tên Sản Phẩm</th>
                        <th>Ảnh Sản Phẩm</th>
                        <th>Số Lượng - Size - Màu</th>
                        <th class="hidden-480">Giá Tiền</th>
                        <th class="hidden-480">Thành Tiền</th>
                        <th>Xóa</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($chitiet as $items)
                      <tr>
                        <td>{!! $items->SanPham->Name !!}</td>
                        <td>
                          <div style="text-align: center;">
                            <img src="{{ asset('uploads/sanpham/'.$items->SanPham->Image) }}" style="width: 50px;"/>
                          </div>
                        </td>
                        <td>{!! $items->SoLuong !!} Chiếc - {!! $items->SizeMau !!}</td>
                        <td class="hidden-480">{!! number_format($items->Gia) !!} vnđ</td>
                        <td class="hidden-480">{!! number_format(($items->SoLuong)*($items->Gia)) !!} vnđ</td>
                        <td>
                          <a class="red" href="{{ url('admin/donhang/delsp/'.$donhang->id.'/'.$items->idSanPham) }}" onclick="return confirm('Bạn có chắc là xóa Sản Phẩm này không ?')">
                            <i class="ace-icon fa fa-trash-o bigger-130"></i>
                            Xóa
                          </a>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>

                  <div class="panel-footer text-right">
                    @if ($donhang->TrangThai == 1)
                    <a href="{{ url('admin/donhang') }}" class="btn btn-default"><i class="fa fa-reply"></i> Quay Lại</a>
                    @else
                    <a href="{{ route('admin.donhang.xacnhan',$donhang->id) }}" class="btn btn-success" onclick="return xacnhan('Xác nhận đơn hàng này ?')">
                      <i class="ace-icon glyphicon glyphicon-edit"></i> Xác Nhận Đơn Hàng</a>
                      @endif
                      
                    </div>
                  </div><!-- /.span -->
                </div>
              </div>
            </div>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.page-content -->
    </div>
  </div><!-- /.main-content -->
  @endsection

