@extends('backend.layout.master')
@section('content')
<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="{{ url('admin/pages') }}">Home</a>
        </li>

        <li>
          <a href="javascript:voi"> Đơn Hàng </a>
        </li>
        <li class="active"> Danh Sách Đơn Hàng</li>
      </ul><!-- /.breadcrumb -->

      <div class="nav-search" id="nav-search">
        <form class="form-search">
          <span class="input-icon">
            <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
            <i class="ace-icon fa fa-search nav-search-icon"></i>
          </span>
        </form>
      </div><!-- /.nav-search -->
    </div>

    <div class="page-content">
      <div class="row">
        <div class="col-xs-12">
          <div class="row">
            <div class="col-xs-12">
              <div class="clearfix">
                <a href="javascript:voi" onclick="myFunction()">
                  <div class="pull-text col-xs-3" style="margin-bottom: 5px">
                    <button class="btn btn-success" ><i class="ace-icon fa fa-refresh white"></i> Làm Mới </button>
                  </div>
                </a>
                <script>
                  function myFunction() {
                    location.reload();
                  }
                </script>
                <div class="pull-text col-xs-6">
                  @include('backend.block.alert')
                </div>
                <div class="pull-right tableTools-container col-xs-2"></div>
              </div>
              <div class="table-header">
                Danh Sách Đơn Hàng
              </div>

              <div>
                <table id="dynamic-table" class="table table-striped table-bordered table-hover">
                  <thead>
                    <tr>
                      <th>STT</th>
                      <th>Tên Khách Hàng</th>
                      <th>Điện Thoại</th>
                      <th>Ngày Đặt Hàng</th>
                      <th>Tổng Tiền</th>
                      <th class="hidden-480">Trạng Thái</th>
                      <th>Chức Năng</th>
                    </tr>
                  </thead>

                  <tbody>
                    @php
                    $i = 0;
                    @endphp
                    @foreach ($list as $items)
                    @php
                    $i++;
                    @endphp
                    <tr>
                      <td>{{ $i }}</td>
                      <td>{!! $items->User->name !!}</td>
                      <td>{!! $items->User->phone !!}</td>
                      <td>{!! $items->NgayMua !!}</td>
                      <td>{!! $items->TongTien !!} vnđ</td>
                      <td class="hidden-480">
                        @if ( $items->TrangThai == 1)
                        <span class="label label-success arrowed-in arrowed-in-right"> Đã Xác Nhận </span>
                        
                        @elseif($items->TrangThai == 2)
                        <span class="label label-warning arrowed arrowed-right "> Chờ Xác Nhận </span>
                        @else
                        <span class="label label-danger arrowed arrowed-right " > Đã Hủy </span>
                        @endif
                      </td>

                      <td>
                        <div class="hidden-sm hidden-xs action-buttons">
                          <a class="blue" href="{{ route('admin.donhang.detail',$items->id) }}">
                            <i class="ace-icon fa fa-search-plus bigger-130"></i>
                            Chi Tiết
                          </a>
                          
                           <a class="pink" onclick="return confirm('Hủy bỏ đơn hàng ?')" href="{{ route('admin.donhang.huybo',$items->id) }}" style="margin-right: 5px">
                            <i class="ace-icon glyphicon glyphicon-remove"></i>
                            Hủy Bỏ 
                          </a>

                          <a class="red" href="{{ route('admin.donhang.del',$items->id) }}" onclick="return confirm('Bạn có chắc là xóa Đơn Hàng này không ?')">
                            <i class="ace-icon fa fa-trash-o bigger-130"></i>
                            Xóa
                          </a>


                        </div>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.page-content -->
  </div>
</div><!-- /.main-content -->
@endsection

@section('script')
<script type="text/javascript">
  jQuery(function($) {
    var myTable = $('#dynamic-table').DataTable( {
      "lengthMenu": [
      [10, 25, 50, -1],
      [10, 25, 50, "Tất cả"]
      ],
      language : {
        "searchPlaceholder": "Nhập dữ liệu ...",
        "emptyTable":     "Không có dữ liệu trong bảng",
        "info":           "Hiển thị _START_ đến _END_ của _TOTAL_ mục",
        "infoEmpty":      "Hiển thị 0 đến 0 của 0 mục",
        "infoFiltered":   "(Được lọc tử _MAX_ tất cả các mục)",
        "thousands":      ",",
        "lengthMenu":     "Hiển thị _MENU_ Mục",
        "loadingRecords": "Chờ xíu nhé...",
        "processing":     "Đang Xử Lí...",
        "search":         "Tìm Kiếm:",
        "zeroRecords":    "Không tìm thấy kết quả",
        "paginate": {
          "first":      "Đầu",
          "last":       "Cuối",
          "next":       "Tiếp",
          "previous":   "Trước"
        },
      }
    });




    $.fn.dataTable.Buttons.defaults.dom.container.className = 'dt-buttons btn-overlap btn-group btn-overlap';

    new $.fn.dataTable.Buttons( myTable, {
      buttons: [
      {
        "extend": "colvis",
        "text": "<i class='ace-icon fa fa-eye'></i> <span class='hidden'>Hiện/Ẩn Cột</span>",
        "className": "btn btn-white btn-primary btn-bold",
        columns: ':not(:first):not(:last)'
      },
      {
        "extend": "copy",
        "text": "<i class='fa fa-copy bigger-110 pink'></i> <span class='hidden'>Coppy</span>",
        "className": "btn btn-white btn-primary btn-bold"
      },
      {
        "extend": "csv",
        "text": "<i class='fa fa-database bigger-110 orange'></i> <span class='hidden'>Export to CSV</span>",
        "className": "btn btn-white btn-primary btn-bold"
      },
      {
        "extend": "excel",
        "text": "<i class='fa fa-file-excel-o bigger-110 green'></i> <span class='hidden'>Export to Excel</span>",
        "className": "btn btn-white btn-primary btn-bold"
      },
      {
        "extend": "pdf",
        "text": "<i class='fa fa-file-pdf-o bigger-110 red'></i> <span class='hidden'>Export to PDF</span>",
        "className": "btn btn-white btn-primary btn-bold"
      },
      {
        "extend": "print",
        "text": "<i class='fa fa-print bigger-110 grey'></i> <span class='hidden'>Print</span>",
        "className": "btn btn-white btn-primary btn-bold",
        autoPrint: false,
        message: 'This print was produced using the Print button for DataTables'
      }     
      ]
    } );
    myTable.buttons().container().appendTo( $('.tableTools-container') );
    var defaultCopyAction = myTable.button(1).action();
    myTable.button(1).action(function (e, dt, button, config) {
      defaultCopyAction(e, dt, button, config);
      $('.dt-button-info').addClass('gritter-item-wrapper gritter-info gritter-center white');
    });


    var defaultColvisAction = myTable.button(0).action();
    myTable.button(0).action(function (e, dt, button, config) {

      defaultColvisAction(e, dt, button, config);


      if($('.dt-button-collection > .dropdown-menu').length == 0) {
        $('.dt-button-collection')
        .wrapInner('<ul class="dropdown-menu dropdown-light dropdown-caret dropdown-caret" />')
        .find('a').attr('href', '#').wrap("<li />")
      }
      $('.dt-button-collection').appendTo('.tableTools-container .dt-buttons')
    });

    setTimeout(function() {
      $($('.tableTools-container')).find('a.dt-button').each(function() {
        var div = $(this).find(' > div').first();
        if(div.length == 1) div.tooltip({container: 'body', title: div.parent().text()});
        else $(this).tooltip({container: 'body', title: $(this).text()});
      });
    }, 500);





    myTable.on( 'select', function ( e, dt, type, index ) {
      if ( type === 'row' ) {
        $( myTable.row( index ).node() ).find('input:checkbox').prop('checked', true);
      }
    } );
    myTable.on( 'deselect', function ( e, dt, type, index ) {
      if ( type === 'row' ) {
        $( myTable.row( index ).node() ).find('input:checkbox').prop('checked', false);
      }
    } );


    $('th input[type=checkbox], td input[type=checkbox]').prop('checked', false);

    $('#dynamic-table > thead > tr > th input[type=checkbox], #dynamic-table_wrapper input[type=checkbox]').eq(0).on('click', function(){
          var th_checked = this.checked;//checkbox inside "TH" table header
          
          $('#dynamic-table').find('tbody > tr').each(function(){
            var row = this;
            if(th_checked) myTable.row(row).select();
            else  myTable.row(row).deselect();
          });
        });

    $('#dynamic-table').on('click', 'td input[type=checkbox]' , function(){
      var row = $(this).closest('tr').get(0);
      if(this.checked) myTable.row(row).deselect();
      else myTable.row(row).select();
    });



    $(document).on('click', '#dynamic-table .dropdown-toggle', function(e) {
      e.stopImmediatePropagation();
      e.stopPropagation();
      e.preventDefault();
    });



    var active_class = 'active';
    $('#simple-table > thead > tr > th input[type=checkbox]').eq(0).on('click', function(){
      var th_checked = this.checked;

      $(this).closest('table').find('tbody > tr').each(function(){
        var row = this;
        if(th_checked) $(row).addClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', true);
        else $(row).removeClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', false);
      });
    });

    $('#simple-table').on('click', 'td input[type=checkbox]' , function(){
      var $row = $(this).closest('tr');
      if($row.is('.detail-row ')) return;
      if(this.checked) $row.addClass(active_class);
      else $row.removeClass(active_class);
    });



    $('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});

    function tooltip_placement(context, source) {
      var $source = $(source);
      var $parent = $source.closest('table')
      var off1 = $parent.offset();
      var w1 = $parent.width();

      var off2 = $source.offset();

      if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
      return 'left';
    }




    $('.show-details-btn').on('click', function(e) {
      e.preventDefault();
      $(this).closest('tr').next().toggleClass('open');
      $(this).find(ace.vars['.icon']).toggleClass('fa-angle-double-down').toggleClass('fa-angle-double-up');
    });


  });
</script>
@endsection