@extends('backend.layout.master')
@section('content')
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="{{ url('admin/pages') }}">Home</a>
				</li>

				<li>
					<a href="{{ url('admin/danhmuc') }}">Danh Mục</a>
				</li>
				<li class="active">Chỉnh Sửa Danh Mục</li>
			</ul><!-- /.breadcrumb -->
			<div class="nav-search" id="nav-search">
				<form class="form-search">
					<span class="input-icon">
						<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
						<i class="ace-icon fa fa-search nav-search-icon"></i>
					</span>
				</form>
			</div><!-- /.nav-search -->
		</div>
		<div class="page-content">
			<div class="row">
				<div class="col-xs-12">
					<div class="widget-box">
						<div class="widget-header widget-header-blue widget-header-flat">
							<h4 class="widget-title lighter">Chỉnh Sửa Danh Mục</h4>
						</div>
						<div class="widget-body">
							@include('backend.block.err')
							<div class="widget-main">
								<div id="fuelux-wizard-container">
									<div class="step-content pos-rel">
										<div class="step-pane active" data-step="1">
											<form action="" class="form-horizontal" method="post" id="validation-form">
												<input type="hidden" name="_token" value="{{ csrf_token() }}">
												<div class="form-group">
													<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="name">Danh Mục:</label>

													<div class="col-xs-12 col-sm-9">
														<div class="clearfix">
															<input type="text" id="name" name="name" class="col-xs-12 col-sm-5" value="{{ old('name', isset($edit) ? $edit->Name : null) }}" required="required"/>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="state">Chọn Danh Mục Cha</label>

													<div class="col-xs-12 col-sm-9">
														<select id="state" name="danhmuc" class="select2" data-placeholder="Chọn danh mục...">
															<option value="0">&nbsp;Danh Mục Cha</option>
															@php
                                cat_parent($data,0,"--",$edit->DanhMuc_Cha);
                              @endphp
														</select>
													</div>
												</div>

												{{-- <div class="form-group">
													<label class="control-label col-xs-12 col-sm-3 no-padding-right">Trạng Thái</label>

													<div class="col-xs-12 col-sm-9">
														<div>
															<label class="line-height-1 blue">
																<input name="status" value="1" type="radio" class="ace" {{ $edit->AnHien == 1 ? 'checked' : null }}/>
																<span class="lbl"> Hiện Thị </span>
															</label>
														</div>

														<div>
															<label class="line-height-1 blue">
																<input name="status" value="2" type="radio" class="ace" {{ $edit->AnHien == 2 ? 'checked' : null }}/>
																<span class="lbl"> Tạm Ẩn </span>
															</label>
														</div>
													</div>
												</div> --}}
												<hr />
												<div class="wizard-actions">
													<a href="{{ url('admin/danhmuc') }}" class="btn btn-info"><i class="fa fa-reply"></i> Hủy Bỏ</a>
													<button class="btn" type="reset"><i class="ace-icon fa fa-undo bigger-110"></i> Nhập Lại </button>
													<button type="submit" id="trigger-save" class="btn btn-success submit-form" name="addUser"><i class="fa fa-save"></i> Cập Nhật </button>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div><!-- /.widget-main -->
						</div><!-- /.widget-body -->
					</div>
				</div>
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.page-content -->
</div>
</div>
@endsection

@section('script')
<script type="text/javascript">
	jQuery(function($) {

		$('[data-rel=tooltip]').tooltip();

		$('.select2').css('width','200px').select2({allowClear:true})
		.on('change', function(){
			$(this).closest('form').validate().element($(this));
		}); 


		
		$(document).one('ajaxloadstart.page', function(e) {
			$('[class*=select2]').remove();
		});
	})
</script>
@endsection

